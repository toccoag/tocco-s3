#![allow(clippy::unwrap_used)]

use fallible_iterator::FallibleIterator;
use std::fs::{self, File};
use std::io;
use std::os::unix::fs::symlink;
use std::path::{Path, PathBuf};
use tocco_s3::fs::object::ArchiveEntry;
use tocco_s3::fs::Archive;
use tocco_s3::hash::Hash;

#[test]
fn find_object() {
    let hash = "e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855";
    let test_data = [("bucket1", hash), ("bucket2", hash)];
    let dir = tempfile::tempdir().unwrap().path().join("archive");
    let archive = Archive::init(&dir).unwrap();
    for (bucket, hash) in test_data {
        let hash = Hash::new(hash).unwrap();
        archive
            .create_object(bucket, &hash, &mut io::empty())
            .unwrap();
    }

    let hash = Hash::new(hash).unwrap();
    let mut objects: Vec<_> = archive
        .object_finder()
        .unwrap()
        .all_with_hash(&hash)
        .map(|obj| {
            let bucket = obj.bucket().to_string();
            let hash = obj.hash().hex_digest().to_string();
            Ok((bucket, hash))
        })
        .collect()
        .unwrap();
    objects.sort();
    assert_eq!(test_data.len(), objects.len());
    for ((bucket_a, hash_a), (bucket_b, hash_b)) in test_data.iter().zip(objects.iter()) {
        assert_eq!(bucket_a, bucket_b);
        assert_eq!(hash_a, hash_b);
    }
}

#[test]
fn write_and_reread_object() {
    let dir = tempfile::tempdir().unwrap().path().join("archive");
    let archive = Archive::init(&dir).unwrap();
    let data = b"some data to write";
    let hash =
        Hash::new("608b4fa8e24e9e05507afb602a1f7f84a94a0d216d3db98713bb865bed784cb6").unwrap();
    let mut reader = &data[..];
    archive
        .create_object("bucket1", &hash, &mut reader)
        .unwrap();
    let mut writer = Vec::new();
    archive
        .get_object("bucket1", &hash)
        .unwrap()
        .unwrap()
        .copy_content(&mut writer)
        .unwrap();
    assert_eq!(data, &writer[..]);
}

#[test]
fn list_all_objects() {
    let dir = tempfile::tempdir().unwrap().path().join("archive");
    let archive = Archive::init(&dir).unwrap();
    let test_data = [
        (
            "bucket1",
            "0000000000000000000000000000000000000000000000000000000000001000",
        ),
        (
            "bucket1",
            "0000000000000000000000000000000000000000000000000000000000001001",
        ),
        (
            "bucket2",
            "0000000000000000000000000000000000000000000000000000000000002000",
        ),
        (
            "bucket2",
            "0000000000000000000000000000000000000000000000000000000000002001",
        ),
        (
            "bucket3",
            "0000000000000000000000000000000000000000000000000000000000003000",
        ),
    ];
    for (bucket, hash) in test_data {
        create_empty_object(&dir, bucket, hash);
    }
    std::fs::create_dir(dir.join("empty_bucket")).unwrap();
    File::create(dir.join("ignored_file_in_root")).unwrap();
    create_empty_object(&dir, "bucket1", "ignored_file_in_bucket");

    let mut observed_paths: Vec<_> = archive
        .all_objects()
        .unwrap()
        .filter_map(|obj| Ok(obj.into_hash_object()))
        .map(|obj| {
            let bucket = obj.bucket().to_string();
            let hash = obj.hash().hex_digest().to_string();
            Ok((bucket, hash))
        })
        .collect()
        .unwrap();
    observed_paths.sort();
    assert_eq!(test_data.len(), observed_paths.len());
    for ((bucket_a, hash_a), (bucket_b, hash_b)) in test_data.iter().zip(observed_paths.iter()) {
        assert_eq!(bucket_a, bucket_b);
        assert_eq!(hash_a, hash_b);
    }
}

#[test]
fn list_objects_in_bucket() {
    fn create_object<P: AsRef<Path>>(archive: &Archive, bucket: &str, path: P) -> PathBuf {
        let path: PathBuf = [archive.path(), Path::new(bucket), path.as_ref()]
            .iter()
            .collect();
        fs::create_dir_all(path.parent().unwrap()).unwrap();
        File::create(&path).unwrap();
        path
    }

    let dir = tempfile::tempdir().unwrap().path().join("archive");
    let archive = Archive::init(&dir).unwrap();
    let test_data = [
        ("searched_bucket", "a/path/to/file"),
        ("searched_bucket", "a_file"),
        (
            "searched_bucket",
            "b000000000000000000000000000000000000000000000000000000000001000",
        ),
        (
            "searched_bucket",
            "c000000000000000000000000000000000000000000000000000000000002000",
        ),
        (
            "searched_bucket",
            "non_hash/c000000000000000000000000000000000000000000000000000000000002000",
        ),
        ("searched_bucket", "path/to/file"),
        (
            "ignored_bucket",
            "aaaaaaaaaaaa0000000000000000000000000000000000000000000000003000",
        ),
        ("ignored_bucket", "path/to/ignored/file"),
    ];

    let expected: Vec<_> = test_data
        .iter()
        .filter_map(|(bucket, path)| {
            let abs_path = create_object(&archive, bucket, path);
            (bucket == &"searched_bucket").then_some(abs_path)
        })
        .collect();
    let mut found: Vec<_> = archive
        .objects_for_buckets(&["searched_bucket"])
        .unwrap()
        .unwrap()
        .map(|obj| match obj {
            ArchiveEntry::NonHash(obj) => obj.absolute_path(),
            ArchiveEntry::Hash(object) => [
                archive.path(),
                Path::new(object.bucket()),
                Path::new(&object.hash().hex_digest()),
            ]
            .iter()
            .collect(),
        })
        .collect();
    found.sort();
    assert_eq!(expected, found);
}

#[test]
fn list_objects_with_invalid_object() {
    let dir = tempfile::tempdir().unwrap().path().join("archive");
    let archive = Archive::init(&dir).unwrap();
    let bucket_path = dir.join("bucket1");
    fs::create_dir(&bucket_path).unwrap();
    let symlink_path = bucket_path.join("a_symlink");
    symlink("/etc/hosts", symlink_path).unwrap();

    let err = archive.all_objects().unwrap().next().unwrap_err();
    assert_eq!(err.kind(), io::ErrorKind::InvalidData);
    assert!(format!("{err}").starts_with("unsupported file type for "));
}

#[test]
fn integrity_check() {
    let dir = tempfile::tempdir().unwrap().path().join("archive");
    let archive = Archive::init(&dir).unwrap();
    let content = b"content matching hash";
    let hash =
        Hash::new("010784ed58a30394161abdabd8a3af90d88a4338def29feba18ab5d5c587dd40").unwrap();
    let mut reader = &content[..];
    let obj = archive
        .create_object("bucket1", &hash, &mut reader)
        .unwrap();

    // Checksum matches
    assert!(&obj.check_integrity().is_ok());

    // Corrupt file
    {
        let mut file = File::create(dir.join("bucket1").join(hash.hex_digest())).unwrap();
        let content = b"corrupted content";
        let mut reader = &content[..];
        io::copy(&mut reader, &mut file).unwrap();
    }

    // Checksum does NOT match
    assert!(&obj.check_integrity().is_err());
}

#[test]
fn look_up_by_hash() {
    let dir = tempfile::tempdir().unwrap().path().join("archive");
    let archive = Archive::init(&dir).unwrap();
    let hash1 = "0000000000000000000000000000000000000000000000000000000000000001";
    let hash2 = "0000000000000000000000000000000000000000000000000000000000000002";
    let hash3 = "0000000000000000000000000000000000000000000000000000000000000003";

    create_empty_object(&dir, "bucket1", hash1);
    create_empty_object(&dir, "bucket2", hash1);
    create_empty_object(&dir, "bucket3", hash1);
    create_empty_object(&dir, "bucket4", hash2);
    create_empty_object(&dir, "bucket5", hash1);
    create_empty_object(&dir, "bucket6", hash1);
    create_empty_object(&dir, "bucket7", hash1);

    let hash1 = Hash::new(hash1).unwrap();
    let hash2 = Hash::new(hash2).unwrap();
    let hash3 = Hash::new(hash3).unwrap();

    {
        let mut finder = archive.object_finder().unwrap();
        assert!(finder.exists_with_hash(&hash1).unwrap());
        assert!(finder.exists_with_hash(&hash2).unwrap());
        assert!(!finder.exists_with_hash(&hash3).unwrap());
        assert_eq!(finder.all_with_hash(&hash1).count().unwrap(), 6);
        assert_eq!(finder.all_with_hash(&hash2).count().unwrap(), 1);
        assert_eq!(finder.all_with_hash(&hash3).count().unwrap(), 0);
    }

    {
        let mut finder = archive
            .object_finder_for_buckets(["bucket1", "bucket7"])
            .unwrap();
        assert!(finder.exists_with_hash(&hash1).unwrap());
        assert!(!finder.exists_with_hash(&hash2).unwrap()); // not in searched buckets
        assert!(!finder.exists_with_hash(&hash3).unwrap());
        assert_eq!(finder.all_with_hash(&hash1).count().unwrap(), 2);
        assert_eq!(finder.all_with_hash(&hash2).count().unwrap(), 0);
        assert_eq!(finder.all_with_hash(&hash3).count().unwrap(), 0);
    }
}

fn create_empty_object(dir: &Path, bucket: &str, hash: &str) {
    let mut path = dir.to_path_buf();
    path.push(bucket);
    std::fs::create_dir_all(&path).unwrap();
    path.push(hash);
    File::create(&path).unwrap();
}
