#![allow(clippy::unwrap_used)]
#![allow(clippy::redundant_closure_for_method_calls)]

use std::fs::{self, File};
use std::io::Write;
use tocco_s3::fs::Archive;

#[test]
fn config() {
    let config = include_str!("sample_config1.toml");
    let dir = tempfile::tempdir().unwrap();
    {
        let mut file = File::create(dir.path().join("config.toml")).unwrap();
        file.write_all(config.as_bytes()).unwrap();
    }
    let archive = Archive::open(&dir).unwrap();

    assert_eq!(
        archive
            .db_servers()
            .iter()
            .map(|i| i.name())
            .collect::<Vec<_>>(),
        &[
            "db1.example.net",
            "db2.example.net",
            "db3.example.net",
            "db4.example.net"
        ]
    );
    assert!(!archive.removal_dry_run_enabled());
    assert_eq!(archive.retention_time().num_days(), 270);
}

#[tokio::test]
async fn config_with_s3_keys() {
    let dir = tempfile::tempdir().unwrap().path().join("archive");
    let _ = Archive::init(&dir).unwrap();
    let config = r#"
            [archive]

            [s3]
            endpoint = "https://objects.rma.cloudscale.ch"

            # setting keys instead
            # cloudscale_api_key = "unused"

            [s3.keys.tocco-nice-abc]
            access_key = "access_key_id_of_abc"
            secret_key = "secret_of_abc"

            [s3.keys.tocco-nice-xyz]
            access_key = "access_key_id_of_xyz"
            secret_key = "secret_of_xyz"

            [removal]
            retention_time_days = 180

            [[db_servers]]
            host_name = "localhost"
        "#;
    fs::write(dir.as_path().join("config.toml"), config).unwrap();
    let s3_keys = Archive::open(&dir).unwrap().s3_keys().await.unwrap();
    assert_eq!(s3_keys.len(), 2);
    {
        let key = &s3_keys["tocco-nice-abc"];
        assert_eq!(key.access_key, "access_key_id_of_abc");
        assert_eq!(key.secret_key, "secret_of_abc");
    }
    {
        let key = &s3_keys["tocco-nice-xyz"];
        assert_eq!(key.access_key, "access_key_id_of_xyz");
        assert_eq!(key.secret_key, "secret_of_xyz");
    }
}
