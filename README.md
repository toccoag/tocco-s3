# Tocco S3

Tool to manage S3 and S3 buckups.

Documentation at https://toccoag.gitlab.io/tocco-s3.

## Testing

It's easiest to run the tests against Minio, a simple S3 implentation,
running locally. A helper script is available which runs Minio in Docker
and allows running tests against it.

Run tests:

```
./minio-wrapper cargo test
```

Run tests, including those needing a Cloudscale API key:

```
export TOCCO_S3_TEST_CLOUDSCALE_API_TOKEN=...
./minio-wrapper cargo test --features tests_with_secrets
```

Requires a read/write key available [here](https://control.cloudscale.ch/service/tocco-71d37db3-2b69-4e15-9738-acfd59d9664e/api-token).

Run tests against Cloudscale's S3:

1. Create user

   ```
   tco s3 create-user --force s3-test
   ```

2. Run tests using printed key:

   ```
   export S3_ENDPOINT=https://objects.rma.cloudscale.ch
   export S3_KEY_ID=...
   export S3_SECRET_KEY=...
   cargo test
   ```

3. Update access_key / secret_key in `~/.s3cfg`.

4. List buckets

   ```
   s3cmd ls
   ```

4. Remove any remaining buckets

   ```
   s3cmd rb --recursive s3://...
   ```

5. Remove user

   ```
   tco s3 remove-user --force s3-test
   ```

Run benchmarks:

```
./minio-wrapper cargo +nightly bench --features nightly
```

Benchmarks create archives in /tmp with large numbers of objects. Depending
on your setup, you may not have enough space there or wish to test against
another file system. Use `TMPDIR` environment variable to change directory.

## Debugging the State DB

The DB is a simple SQLite DB that can be opened with `sqlite3`:

```
$ sqlite3 state.sqlite
SQLite version 3.40.1 2022-12-28 14:03:47
Enter ".help" for usage hints.
sqlite>
```

Use `.schema` or `.fullschema` to see DB schema:

```
sqlite> .schema
CREATE TABLE key_value (
    ...
) STRICT;
...
```

Table `object` stores the hash and last-seen timestamp of every object. For
reasons of efficiency, they are stored in binary and seconds since Unix Epoch,
respectively.  Use `hex()` and `datetime()` to get readable output:

```
sqlite> SELECT lower(hex(hash)), datetime(last_seen, 'unixepoch') FROM object LIMIT 3;
8f5ba9389cef8dc174e97c4e3a2ddc758a4bf8d66f26334dcc148d7f654940b3|2025-01-15 19:04:23
459d0ac2cba72d24f85bf9638539cb26ffe4e014db833d62a38e6796bcfc1113|2025-01-15 19:04:23
2b16aeb5a2866e7e8807ba1de0f5c5cced3044c7f598f8976a15324bcd2515b2|2025-01-15 19:04:23
```
