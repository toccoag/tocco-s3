#![allow(clippy::missing_panics_doc)]

use aws_sdk_s3 as s3;
use rand::random;
use s3::error::SdkError;
use s3::primitives::ByteStream;
use sha2::Digest;
use std::env;
use std::fs;
use std::io::Write as _;
use std::path::{Path, PathBuf};
use tocco_s3::hash::Hash;
use tocco_s3::Archive;
use tokio::sync::Semaphore;

// Cloudscale allows at most 1000 concurrent requests.
static SEMAPHORE: Semaphore = Semaphore::const_new(800);

pub fn create_empty_object_on_disk(archive: &Archive, bucket: &str, key: &str) {
    create_object_on_disk(archive, bucket, key, "");
}

pub fn create_object_on_disk(archive: &Archive, bucket: &str, key: &str, content: &str) {
    let path: PathBuf = [archive.path(), &Path::new(bucket), &Path::new(key)]
        .iter()
        .collect();
    fs::create_dir_all(&path.parent().unwrap()).unwrap();
    let mut file = fs::File::create(&path).unwrap();
    file.write(content.as_bytes()).unwrap();
}

pub async fn create_empty_object_on_s3(bucket: &str, hash: &str) {
    let client = s3_client();
    let _permit = SEMAPHORE.acquire().await.unwrap();
    let result = client
        .create_bucket()
        .set_bucket(Some(bucket.to_string()))
        .send()
        .await;
    match result {
        Ok(_) => (),
        Err(SdkError::ServiceError(se)) if se.err().is_bucket_already_owned_by_you() => (),
        Err(e) => panic!("{e:?}"),
    }
    client
        .put_object()
        .set_key(Some(hash.to_string()))
        .set_bucket(Some(bucket.to_string()))
        .set_body(Some(ByteStream::from_static(b"")))
        .send()
        .await
        .unwrap();
}

#[must_use]
pub async fn s3_object_exists(bucket: &str, key: &str) -> bool {
    let client = s3_client();
    let _permit = SEMAPHORE.acquire().await.unwrap();
    let resp = client
        .head_object()
        .set_bucket(Some(bucket.to_string()))
        .set_key(Some(key.to_string()))
        .send()
        .await;
    match resp {
        Ok(_) => true,
        Err(SdkError::ServiceError(se)) if se.err().is_not_found() => false,
        Err(e) => panic!("{}", e),
    }
}

pub async fn assert_s3_object_content(bucket: &str, key: &str, content: &str) {
    let client = s3_client();
    let _permit = SEMAPHORE.acquire().await.unwrap();
    let actual_content = client
        .get_object()
        .set_bucket(Some(bucket.to_string()))
        .set_key(Some(key.to_string()))
        .send()
        .await
        .unwrap()
        .body
        .collect()
        .await
        .unwrap()
        .to_vec();
    let actual_content = String::from_utf8(actual_content).unwrap();
    assert_eq!(actual_content, content);
}

#[must_use]
pub fn s3_client() -> aws_sdk_s3::Client {
    let (key_id, secret_key) = s3_key();
    tocco_s3::s3::client(&s3_endpoint(), &key_id, &secret_key)
}

#[must_use]
pub fn s3_endpoint() -> String {
    env::var("S3_ENDPOINT").unwrap_or_else(|_| "http://localhost:9000".to_string())
}

#[must_use]
pub fn s3_key() -> (String, String) {
    let key_id = env::var("S3_KEY_ID").unwrap_or_else(|_| "ANTN35UAENTS5UIAEATD".to_string());
    let secret_key = env::var("S3_SECRET_KEY")
        .unwrap_or_else(|_| "TtnuieannGt2rGuie2t8Tt7urarg5nauedRndrur".to_string());
    (key_id, secret_key)
}

#[must_use]
pub async fn create_bucket() -> (s3::Client, String) {
    let name = format!("tocco-s3-test-{}", random::<u32>());
    let endpoint = s3_endpoint();
    let (access_key, secret_key) = s3_key();
    let _permit = SEMAPHORE.acquire().await.unwrap();
    let client = tocco_s3::s3::client(&endpoint, &access_key, &secret_key);
    client.create_bucket().bucket(&name).send().await.unwrap();
    (client, name)
}

pub async fn create_bucket_with_name(name: &str) {
    let endpoint = s3_endpoint();
    let (access_key, secret_key) = s3_key();
    let _permit = SEMAPHORE.acquire().await.unwrap();
    let client = tocco_s3::s3::client(&endpoint, &access_key, &secret_key);
    client.create_bucket().bucket(name).send().await.unwrap();
}

#[must_use]
pub async fn create_s3_object(client: &aws_sdk_s3::Client, bucket: &str, content: &str) -> String {
    let mut hash = sha2::Sha256::new();
    hash.update(content.as_bytes());
    let hash = Hash::from_bytes(&hash.finalize()[..]).unwrap();
    let key = hash.hex_digest();
    create_s3_object_with_key(client, bucket, &key, content).await;
    key
}

pub async fn create_s3_object_with_key(
    client: &aws_sdk_s3::Client,
    bucket: &str,
    key: &str,
    content: &str,
) {
    let _permit = SEMAPHORE.acquire().await.unwrap();
    client
        .put_object()
        .bucket(bucket)
        .key(key)
        .body(ByteStream::from(content.as_bytes().to_owned()))
        .send()
        .await
        .unwrap();
}
