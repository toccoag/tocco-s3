[package]
name = "tocco-s3"
homepage = "https://gitlab.com/toccoag/tocco-s3"
license = "LGPL-3.0+"
version = "1.1.2"
edition = "2021"

[package.metadata.deb]
name = "tocco-s3"
maintainer = "pege <pgerberg@tocco.ch>"
copyright = "2024, Tocco AG <admin@tocco.ch>"
extended-description = """\
Manage S3 backups of Tocco.

See also:
• https://devops-docs.tocco.ch/services/s3.html
• https://docs.tocco.ch/framework/architecture/s3/index.html"""

[badges]
gitlab = { repository = "toccoag/tocco-s3" }

[dependencies]
async-stream = "0.3.5"
aws-config = { version = "1.2.0", features = ["behavior-version-latest"] }
aws-credential-types = "1.2.0"
aws-sdk-s3 = { version = "1.23.0", features = ["behavior-version-latest"] }
aws-smithy-async = "1.2.1"
aws-smithy-types = "1.1.8"
aws-types = "1.2.0"
base64 = "0.22"
chrono = { version = "0.4.23", default-features = false, features = ["clock"] }
clap = { version = "4.0", features=["derive"] }
env_logger = "0.11"
fallible-iterator = "0.3"
futures = "0.3.28"
lazy_static = "1.4.0"
log = { version = "0.4", features = ["release_max_level_debug"] }
nix = { version = "0.29", features=["fs", "user"] }
rand = "0.9.0"
reqwest = { version = "0.12.0", default-features = false, features = ["rustls-tls-native-roots", "json"] }
rusqlite = { version = "0.33.0", features = ["bundled", "chrono", "functions"] }
serde = { version = "1.0.152", features=["derive"] }
sha2 = "0.10"
thiserror = "2.0"
tokio = { version = "1.27.0", features = ["rt", "macros", "rt-multi-thread"] }
tokio-stream = "0.1.14"
tokio-util = { version = "0.7.9", features = ["io-util"] }
toml = "0.8.0"

[dev-dependencies]
rand = { version = "0.9.0", features=["small_rng"] }
tempfile = "3.8.0"
test_utils = { path = "test_utils" }

[features]
nightly = []
slow_benches = ["nightly"]
tests_with_secrets = []
