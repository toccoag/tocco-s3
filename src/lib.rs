//! # Manage S3 and S3 backups
//!
//! Used to manage objects created by [Tocco](https://www.tocco.ch).
//!
//! See also [S3 Storage Design Overview](https://docs.tocco.ch/framework/architecture/s3/s3.html).
//!
//! ## Code Overview
//!
//! | Where                    | What                                                     |
//! |--------------------------|----------------------------------------------------------|
//! | [`fs::Archive`]          | Representationof a backup archive and its configuration. |
//! | [`state::Db`]            | State DB for tracking live objects.                      |
//! | [`db`]                   | Obtaining list of objects from DB servers.               |
//!
//! ## Configuration
//!
//! Archives are configured via `config.toml` in the archive's root directory. Run
//! `tocco-s3 init` to initialize a repository with a default config, then modify
//! it as needed:
//!
//! ```toml
#![doc = include_str!("fs/default_config.toml")]
//! ```

#![allow(clippy::map_unwrap_or)]
#![allow(clippy::match_wildcard_for_single_variants)]
#![allow(clippy::missing_errors_doc)]
#![allow(clippy::needless_raw_string_hashes)]
#![allow(clippy::redundant_closure)]
#![allow(clippy::redundant_closure_for_method_calls)]
#![allow(clippy::result_unit_err)]
#![allow(clippy::single_match_else)]
#![allow(clippy::manual_let_else)]
#![deny(clippy::print_stderr)]
#![deny(clippy::print_stdout)]
// warn for development
//
// This are set to deny during CI but allowed locally
// to ease development.
//
// Sync with .gitlab-ci.yml
#![warn(clippy::dbg_macro)]
#![warn(clippy::unwrap_used)]
#![cfg_attr(all(test, feature = "nightly"), feature(test))]

#[cfg(all(test, feature = "nightly"))]
extern crate test;

#[macro_use]
extern crate log;

pub mod cloudscale;
pub mod common;
pub mod db;
pub mod error;
pub mod fs;
pub mod hash;
pub mod s3;
pub mod state;

pub use error::{Error, Result};
pub use fs::Archive;
