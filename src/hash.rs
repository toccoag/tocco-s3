//! Hash used to identify object and its content

use crate::Error;
use rusqlite::types::{FromSql, FromSqlError, FromSqlResult, ToSql, ToSqlOutput, ValueRef};
use sha2::Digest;
use std::fmt;
use std::io::{self, Read};

type Result<T> = std::result::Result<T, ()>;

/// Representation of a sha2 hash and object identifier
#[derive(Clone, Default, PartialEq, Eq, PartialOrd, Ord)]
pub struct Hash([u8; 32]);

impl Hash {
    /// Create new hash from its lower-case hexadecimal representation.
    ///
    /// ```
    /// use tocco_s3::hash::Hash;
    ///
    /// assert!(Hash::new("0123456789abcdef9afbf4c8996fb92427ae41e4649b934ca495991b7852b855").is_ok());
    /// ```
    ///
    /// An error is returned if the hash isn't valid.
    ///
    /// ```
    /// use tocco_s3::hash::Hash;
    ///
    /// // one character short
    /// assert!(Hash::new("e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca95991b7852b855").is_err());
    ///
    /// // invalid character
    /// assert!(Hash::new("g3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca95991b7852b855").is_err());
    /// ```
    pub fn new(hash: &str) -> Result<Self> {
        Ok(Hash(from_hex(hash)?))
    }

    /// Create `Hash` from bytes.
    ///
    /// ```
    /// use tocco_s3::hash::Hash;
    ///
    /// assert_eq!(
    ///     Hash::from_bytes(&[0xa6; 32]).unwrap().hex_digest(),
    ///     "a6a6a6a6a6a6a6a6a6a6a6a6a6a6a6a6a6a6a6a6a6a6a6a6a6a6a6a6a6a6a6a6"
    /// );
    /// ```
    ///
    /// Returns an error if `hash` isn't exactly 32 bytes long.
    ///
    /// ```
    /// use tocco_s3::hash::Hash;
    ///
    /// assert!(Hash::from_bytes(&[0; 33]).is_err());
    /// ```
    pub fn from_bytes(hash: &[u8]) -> Result<Self> {
        let hash: [u8; 32] = hash.try_into().map_err(|_| ())?;
        Ok(Hash(hash))
    }

    // Return hash as bytes
    #[must_use]
    pub fn as_bytes(&self) -> &[u8; 32] {
        &self.0
    }

    // Return hash as lower-case hex
    #[must_use]
    pub fn hex_digest(&self) -> String {
        to_hex(&self.0)
    }

    // Return hash as base64
    #[must_use]
    pub fn base64_digest(&self) -> String {
        use base64::{engine::general_purpose, Engine as _};

        general_purpose::STANDARD.encode(self.0)
    }

    /// Hash as bytes
    ///
    /// ```
    /// use tocco_s3::hash::Hash;
    ///
    /// assert_eq!(
    ///     Hash::from_bytes(&[0xa6; 32]).unwrap().hex_digest(),
    ///     "a6a6a6a6a6a6a6a6a6a6a6a6a6a6a6a6a6a6a6a6a6a6a6a6a6a6a6a6a6a6a6a6"
    /// );
    /// assert_eq!(
    ///     Hash::new("a6a6a6a6a6a6a6a6a6a6a6a6a6a6a6a6a6a6a6a6a6a6a6a6a6a6a6a6a6a6a6a6")
    ///         .unwrap()
    ///         .digest(),
    ///     &[0xa6; 32]
    /// )
    /// ```
    #[must_use]
    pub fn digest(&self) -> &[u8; 32] {
        &self.0
    }
}

impl fmt::Display for Hash {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        fmt::Display::fmt(&to_hex(&self.0), f)
    }
}

impl fmt::Debug for Hash {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        fmt::Debug::fmt(&self.hex_digest(), f)
    }
}

impl FromSql for Hash {
    fn column_result(value: ValueRef<'_>) -> FromSqlResult<Self> {
        value
            .as_blob()
            .and_then(|bytes| match Hash::from_bytes(bytes) {
                Ok(hash) => Ok(hash),
                Err(_err) => Err(FromSqlError::Other(Box::new(Error::InvalidHash))),
            })
    }
}

impl ToSql for Hash {
    fn to_sql(&self) -> rusqlite::Result<ToSqlOutput> {
        Ok(ToSqlOutput::from(&self.digest()[..]))
    }
}

/// Wheter a hash is a valid lower-case-encoded hex sha2 hash
pub(crate) fn is_hash_valid(hash: &str) -> bool {
    hash.len() == 64 && hash.chars().all(|c| matches!(c, '0'..='9'|'a'..='f'))
}

/// Reader that returns content from inner reader and verifies
/// the content's hash on the fly.
///
/// # Errors
///
/// Returns [`io::ErrorKind::InvalidData`] when hash doesn't match.
pub struct HashReader<'a> {
    inner: Box<dyn Read + 'a>,
    hasher: Option<sha2::Sha256>,
    hash: [u8; 32],
}

impl<'a> HashReader<'a> {
    pub(crate) fn new(reader: impl Read + 'a, hash: &Hash) -> Self {
        HashReader {
            inner: Box::new(reader),
            hasher: Some(sha2::Sha256::new()),
            hash: *hash.digest(),
        }
    }
}

impl Read for HashReader<'_> {
    fn read(&mut self, buf: &mut [u8]) -> io::Result<usize> {
        match self.inner.read(buf)? {
            0 => {
                if let Some(hasher) = self.hasher.take() {
                    let observed_hash = hasher.finalize();
                    if observed_hash[..] == self.hash {
                        Ok(0)
                    } else {
                        let hash_observed_hex = to_hex(&observed_hash.into());
                        let hash_hex = to_hex(&self.hash);
                        debug!(
                            "Encountered hash {:?} but expected {:?}. Incorrect data or interrupted read.",
                            &hash_observed_hex, &hash_hex
                        );
                        Err(io::Error::new(
                            io::ErrorKind::InvalidData,
                            format!(
                                "checksum mismatch, expected {hash_hex:?} but found \
                                 {hash_observed_hex:?}"
                            ),
                        ))
                    }
                } else {
                    // read on exhausted reader
                    Ok(0)
                }
            }
            len => {
                self.hasher
                    .as_mut()
                    .expect("read() called after reaching EOF")
                    .update(&buf[..len]);
                Ok(len)
            }
        }
    }
}

/// Convert bytes to lower-case hex
#[must_use]
pub fn to_hex(bytes: &[u8; 32]) -> String {
    static HEX_DIGITS: [char; 16] = [
        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f',
    ];
    let mut buf = String::with_capacity(bytes.len() * 2);
    for byte in bytes {
        buf.push(HEX_DIGITS[(byte >> 4) as usize]);
        buf.push(HEX_DIGITS[(byte & 0xf) as usize]);
    }
    buf
}

/// Convert lower-case hex to bytes
pub(crate) fn from_hex(hex: &str) -> Result<[u8; 32]> {
    if hex.len() != 64 {
        return Err(());
    }
    let mut buf = [0_u8; 32];
    for (byte, hex_byte) in buf.iter_mut().zip(hex.as_bytes().chunks_exact(2)) {
        *byte = (hex_to_quadbit(hex_byte[0])? << 4) | hex_to_quadbit(hex_byte[1])?;
    }
    Ok(buf)
}

#[allow(clippy::char_lit_as_u8)]
#[doc(hidden)]
fn hex_to_quadbit(c: u8) -> Result<u8> {
    match c as char {
        '0'..='9' => Ok(c - '0' as u8),
        'a'..='f' => Ok(c - 'a' as u8 + 10),
        _ => Err(()),
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn hex_de_and_encode() {
        let test_data = &[
            (
                "ab84a8bccfda6508f52c3eb9f15494d86fe7f928364994d88a63d59f13fe0798",
                [
                    0xab, 0x84, 0xa8, 0xbc, 0xcf, 0xda, 0x65, 0x08, 0xf5, 0x2c, 0x3e, 0xb9, 0xf1,
                    0x54, 0x94, 0xd8, 0x6f, 0xe7, 0xf9, 0x28, 0x36, 0x49, 0x94, 0xd8, 0x8a, 0x63,
                    0xd5, 0x9f, 0x13, 0xfe, 0x07, 0x98,
                ],
            ),
            (
                "34b1b3ea120f6b193e03168c840fdd56dc79657bf9f79d2ed6f17e8cd642d86a",
                [
                    0x34, 0xb1, 0xb3, 0xea, 0x12, 0x0f, 0x6b, 0x19, 0x3e, 0x03, 0x16, 0x8c, 0x84,
                    0x0f, 0xdd, 0x56, 0xdc, 0x79, 0x65, 0x7b, 0xf9, 0xf7, 0x9d, 0x2e, 0xd6, 0xf1,
                    0x7e, 0x8c, 0xd6, 0x42, 0xd8, 0x6a,
                ],
            ),
            (
                "0000d859ce787bc7c8c5d249d8d93348148d6f7dfe3480b5c87033b85f0dc0a1",
                [
                    0x00, 0x00, 0xd8, 0x59, 0xce, 0x78, 0x7b, 0xc7, 0xc8, 0xc5, 0xd2, 0x49, 0xd8,
                    0xd9, 0x33, 0x48, 0x14, 0x8d, 0x6f, 0x7d, 0xfe, 0x34, 0x80, 0xb5, 0xc8, 0x70,
                    0x33, 0xb8, 0x5f, 0x0d, 0xc0, 0xa1,
                ],
            ),
        ];
        for (hex, binary) in test_data {
            let hash = Hash::new(hex).unwrap();
            assert_eq!(hex, &hash.hex_digest());
            assert_eq!(&binary[..], &hash.digest()[..]);
        }
    }
}
