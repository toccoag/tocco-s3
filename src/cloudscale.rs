//! Cloudscale object user
//!
//! Handling of object users via [Cloudscale's API].
//!
//! [Cloudscale's API]: https://www.cloudscale.ch/en/api

use crate::s3;
use core::time::Duration;
use serde::Deserialize;
use std::collections::HashMap;

/// An object user
///
/// Obtain via [`users()`].
#[derive(Debug, Deserialize)]
pub struct User {
    pub id: String,
    pub display_name: String,
    pub keys: Vec<s3::Key>,
}

impl User {
    /// Get user's key
    ///
    /// If user has multiple keys, an arbitrary key is returned.
    ///
    /// # Panics
    ///
    /// Panics if users does not have a key.
    #[must_use]
    pub fn key(&self) -> &s3::Key {
        &self.keys[0]
    }

    /// Get user's key consuming `self`
    ///
    /// If user has multiple keys, an arbitrary key is returned.
    ///
    /// # Panics
    ///
    /// Panics if users does not have a key.
    #[must_use]
    pub fn into_key(mut self) -> s3::Key {
        self.keys.pop().expect("no key found")
    }
}

/// Get list of all users
///
/// Returns a `HashMap` where the key corresponds to the
/// bucket name.
///
/// It is assumed that all users have a display name set prefixed with "nice-"
/// and that bucket *tocco-nice-{some_name}* belongs to user *nice-{some_name}*.
///
/// Fetched via [objects-users][] API.
///
/// [objects-users]: https://www.cloudscale.ch/en/api/v1#objects-users-list
pub async fn users(api_key: &str) -> Result<HashMap<String, User>, reqwest::Error> {
    let client = reqwest::ClientBuilder::new()
        .user_agent(concat!("Tocco S3/", env!("CARGO_PKG_VERSION")))
        .connect_timeout(Duration::from_secs(30))
        .https_only(true)
        .build()?;
    let response = client
        .get("https://api.cloudscale.ch/v1/objects-users")
        .bearer_auth(api_key)
        .send()
        .await?
        .error_for_status()?;
    let users: Vec<User> = response.json().await?;
    let users = users
        .into_iter()
        .filter(|user| user.display_name.starts_with("nice-"))
        .map(|user| (format!("tocco-{}", user.display_name), user))
        .collect();
    Ok(users)
}

#[cfg(test)]
mod tests {
    use super::*;
    use reqwest::StatusCode;

    #[tokio::test]
    #[cfg(feature = "tests_with_secrets")]
    async fn test_users() {
        let env_var_name = "TOCCO_S3_TEST_CLOUDSCALE_API_TOKEN";
        let key = match std::env::var(env_var_name) {
            Ok(value) => value,
            Err(e) => panic!("{env_var_name}: {e}."),
        };
        let users = users(&key).await.unwrap();
        let user = users.get("tocco-nice-test").unwrap();
        user.id.starts_with("e6c15ec2e9dad");
        assert!(user.key().access_key.starts_with("D51XFP5S"));
    }

    #[tokio::test]
    async fn test_invalid_key() {
        let err = users("an invalid key").await.unwrap_err();
        match err {
            err if err.status() == Some(StatusCode::UNAUTHORIZED) => (),
            _ => panic!("{}", err),
        }
    }
}
