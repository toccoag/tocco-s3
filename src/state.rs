//! # State Database
//!
//! A state database is stored in `state.sqlite` within
//! the archive. The state DB contains information about
//! when indivitual objects were seen last.

use crate::error::{Error, Result};
use crate::hash::{self, Hash};
use chrono::{DateTime, Duration, Local};
use rusqlite::functions::FunctionFlags;
use rusqlite::types::{FromSql, ToSql};
use rusqlite::{Connection, OpenFlags, OptionalExtension};
use std::borrow::Borrow;
use std::path::Path;

pub(crate) fn open_or_init(path: &Path) -> Result<Db> {
    let flags = OpenFlags::SQLITE_OPEN_READ_WRITE
        | OpenFlags::SQLITE_OPEN_CREATE
        | OpenFlags::SQLITE_OPEN_NO_MUTEX;
    let conn = Connection::open_with_flags(path, flags)?;
    let current_version = || conn.query_row("PRAGMA user_version", [], |row| row.get::<_, u32>(0));
    let initial_version = current_version()?;
    conn.execute_batch(
        "PRAGMA journal_mode = WAL;
         PRAGMA foreign_keys = true;",
    )?;
    debug!("opened state DB of version {initial_version}");
    let upgrades = [
        upgrade_to_v1 as fn(_) -> _,
        upgrade_to_v2 as _,
        upgrade_to_v3 as _,
        upgrade_to_v4 as _,
        upgrade_to_v5 as _,
    ];
    let newest_version = upgrades.len() as u32; // `upgrades[0]` is version 1
    if initial_version > newest_version {
        return Err(Error::Custom(format!(
            "unsupported schema version {initial_version} for state DB, newest supported is {newest_version}"
        )));
    }
    for (index, upgrader) in upgrades.iter().enumerate() {
        let version = index as u32 + 1;
        if version > initial_version {
            debug!("upgrading state DB to version {version}");
            upgrader(&conn)?;
            debug_assert_eq!(current_version()?, version);
        }
    }
    Ok(Db { inner: conn })
}

/// SQLite DB state database
///
/// Obtain via [`crate::fs::Archive::state_db()`].
pub struct Db {
    inner: Connection,
}

impl Db {
    /// Insert last seen timestamp for given objects.
    ///
    /// Unlike [`Self::update_last_seen_times()`], times are only inserted
    /// for objects not in the state DB yet. Objects already in the DB are ignored.
    pub fn insert_last_seen_times<I, H>(&mut self, hashes: I) -> Result<()>
    where
        I: IntoIterator<Item = H>,
        H: Borrow<Hash>,
    {
        let hashes = hashes.into_iter();
        let tx = self.inner.transaction()?;
        let mut stmt = tx.prepare_cached(
            r#"
                INSERT INTO object (hash, last_seen)
                  VALUES(?, unixepoch('now'))
                  ON CONFLICT(hash)
                  DO NOTHING
            "#,
        )?;
        for hash in hashes {
            stmt.execute([&hash.borrow()])?;
        }
        drop(stmt);
        tx.commit()?;
        Ok(())
    }

    /// Update last seen timestamp for given objects.
    ///
    /// If an object is missing in state DB, it is inserted.
    pub fn update_last_seen_times<I, H>(&mut self, hashes: I) -> Result<()>
    where
        I: IntoIterator<Item = H>,
        H: Borrow<Hash>,
    {
        let hashes = hashes.into_iter();
        let tx = self.inner.transaction()?;
        let mut stmt = tx.prepare_cached(
            r#"
                INSERT INTO object (hash, last_seen)
                  VALUES(?, unixepoch('now'))
                  ON CONFLICT(hash)
                  DO UPDATE SET last_seen=unixepoch('now')
            "#,
        )?;
        for hash in hashes {
            stmt.execute([&hash.borrow()])?;
        }
        drop(stmt);
        tx.commit()?;
        Ok(())
    }

    /// Set last seen time
    ///
    /// This is mostly useful for testing. Consider using
    /// [`Self::update_last_seen_times()`] instead.
    pub fn set_last_seen_time(&mut self, hash: &Hash, ts: DateTime<Local>) -> Result<()> {
        self.inner.execute(
            r#"
                INSERT INTO object (hash, last_seen)
                  VALUES($1, unixepoch($2))
                  ON CONFLICT(hash)
                  DO UPDATE SET last_seen = unixepoch($2)
            "#,
            [&hash as &dyn ToSql, &ts],
        )?;
        Ok(())
    }

    /// Get last seen time
    pub fn last_seen_time(&mut self, hash: &Hash) -> Result<Option<DateTime<Local>>> {
        Ok(self
            .inner
            .query_row(
                "SELECT datetime(last_seen, 'unixepoch') FROM object where hash = ?",
                [hash],
                |row| Ok(row.get_unwrap(0)),
            )
            .optional()?)
    }

    /// Obtain all objects older than `duration`
    ///
    /// Create an [`ObjectsOlderThanPager`][`ObjectsOlderThanPager::new()`] first
    /// and then pass it to this function.
    ///
    /// Returns the next page (`Some(&[Hash])`) or `None` if there is no more
    /// matching objects.
    ///
    /// ```
    /// use chrono::Duration;
    /// use tocco_s3::state::ObjectsOlderThanPager;
    /// use tocco_s3::Archive;
    ///
    /// let archive_path = tempfile::tempdir().unwrap();
    /// let archive_path = archive_path.path().join("archive");
    /// let archive = Archive::init(&archive_path).unwrap();
    /// let mut state_db = archive.state_db().unwrap();
    /// let objects_returned_per_page = 1024;
    /// let older_than = Duration::days(90);
    /// let mut pager = ObjectsOlderThanPager::new(older_than, objects_returned_per_page);
    /// assert!(state_db
    ///     .next_objects_older_than(&mut pager)
    ///     .unwrap()
    ///     .is_none());
    /// ```
    pub fn next_objects_older_than<'pager>(
        &mut self,
        pager: &'pager mut ObjectsOlderThanPager,
    ) -> Result<Option<&'pager [Hash]>> {
        if pager.exhausted {
            return Ok(None);
        }
        // Marking exhausted in case we abort early with an `Err`.
        pager.exhausted = true;

        let last_hash = pager.objects.pop().unwrap_or_default();
        pager.objects.truncate(0);
        let tx = self.inner.transaction()?;
        let mut stmt = tx.prepare(
            r#"
                SELECT hash
                FROM object
                WHERE last_seen <= unixepoch($1) AND hash > $2
                ORDER BY hash
                LIMIT $3
            "#,
        )?;
        let rows = stmt.query_map(
            [
                &pager.before_this_date as &dyn ToSql,
                &last_hash,
                &(pager.page_size as u64),
            ],
            |row| row.get(0),
        )?;
        for row in rows {
            pager.objects.push(row?);
        }
        if pager.objects.is_empty() {
            Ok(None)
        } else {
            if pager.objects.len() == pager.page_size {
                pager.exhausted = false;
            }
            Ok(Some(&pager.objects))
        }
    }

    /// Delete object matching given hashes.
    pub fn delete_objects<I, H>(&mut self, hashes: I) -> Result<()>
    where
        I: IntoIterator<Item = H>,
        H: Borrow<Hash>,
    {
        let tx = self.inner.transaction()?;
        let mut stmt = tx.prepare("DELETE FROM object WHERE hash = $1")?;
        for hash in hashes {
            stmt.execute([hash.borrow()])?;
        }
        drop(stmt);
        tx.commit()?;
        Ok(())
    }

    /// Update time when DB was last scanned for live objects.
    ///
    /// This time indicates when last a list of all objects was fetched
    /// from the DB server and the `last_seen` times updated.
    pub fn update_last_db_scan_time(&self) -> Result<()> {
        self.set_value("last_db_scan", chrono::Local::now())
    }

    /// Get time when DB was last scanned for live objects.
    ///
    /// See [`Self::update_last_db_scan_time`]
    pub fn last_db_scan_time(&self) -> Result<DateTime<Local>> {
        self.value("last_db_scan")
    }

    /// Update time when archive was last scanned for live objects.
    ///
    /// This time indicates when last a list of all objects in the archive
    /// (on disk) was fetched and records created for objects that
    /// were missing in the DB.
    pub fn update_last_archive_scan_time(&self) -> Result<()> {
        self.set_value("last_archive_scan", chrono::Local::now())
    }

    /// Get time when archive was last scanned for live objects.
    ///
    /// See [`Self::update_last_db_scan_time`]
    pub fn last_archive_scan_time(&self) -> Result<DateTime<Local>> {
        self.value("last_archive_scan")
    }

    /// Update time when S3 objects were last backed up to the archive.
    ///
    /// This is the last time a backup was created successfully.
    pub fn update_last_s3_backup_time(&self) -> Result<()> {
        self.set_value("last_s3_sync", chrono::Local::now())
    }

    /// Get time when S3 objects were last backed up to the archive.
    ///
    /// See [`Self::update_last_s3_time`]
    pub fn last_s3_backup_time(&self) -> Result<DateTime<Local>> {
        self.value("last_s3_sync")
    }

    /// Vacuum database
    ///
    /// <https://www.sqlite.org/lang_vacuum.html>
    pub fn vacuum(&self) -> Result<()> {
        Ok(self.inner.execute_batch("VACUUM")?)
    }

    /// Optimize database
    ///
    /// Executed automatically when DB is `Drop`ped as recommended
    /// by upstream.
    ///
    /// <https://www.sqlite.org/pragma.html#pragma_optimize>
    pub fn optimize(&self) -> Result<()> {
        Ok(self
            .inner
            .execute_batch("PRAGMA analysis_limit=10000; PRAGMA optimize;")?)
    }

    /// Get value from `key_value` table
    ///
    /// # Panics
    ///
    /// Panics if …
    ///
    /// * … no row with key `key` is found.
    /// * … value cannot be represented as `T`.
    fn value<T>(&self, key: &str) -> Result<T>
    where
        T: FromSql,
    {
        Ok(self
            .inner
            .query_row("SELECT value FROM key_value WHERE key = ?", [&key], |row| {
                row.get(0)
            })?)
    }

    /// set value in `key_value` table
    ///
    /// # Panics
    ///
    /// Panics if no row with key `key` is found. (Row is UPDATEd not INSERTed.)
    fn set_value<T>(&self, key: &str, value: T) -> Result<()>
    where
        T: ToSql,
    {
        self.inner.execute(
            "UPDATE key_value SET value = ? WHERE key = ?",
            [&value as &dyn ToSql, &key],
        )?;
        Ok(())
    }
}

impl Drop for Db {
    fn drop(&mut self) {
        if let Err(e) = self.optimize() {
            info!("State DB optimization failed: {e}.");
        }
    }
}

fn upgrade_to_v1(conn: &Connection) -> Result<()> {
    let sql = r#"
        BEGIN;
        CREATE TABLE object (
            hash       HASH      NOT NULL PRIMARY KEY, -- Lower-case hex representation
                                                       -- of a SHA2 hash.

            last_seen  DATETIME  NOT NULL              -- RFC 3339 date/time (in UTC)
        );

        CREATE TABLE key_value (
            key       TEXT  NOT NULL PRIMARY KEY,
            value     ANY   NOT NULL
        );

        INSERT INTO key_value (key, value)
        VALUES
          ('last_archive_scan', '0000-01-01 00:00:00'),
          ('last_db_scan', '0000-01-01 00:00:00');

        PRAGMA user_version = 1;
        COMMIT;
    "#;
    conn.execute_batch(sql)?;
    Ok(())
}

fn upgrade_to_v2(conn: &Connection) -> Result<()> {
    let sql = r#"
        BEGIN;
        ALTER TABLE object RENAME TO object_old;

        -- Converting columns to type TEXT to prevent string like '0000015' from being
        -- converted to an integer.
        --
        -- See https://www.sqlite.org/datatype3.html#determination_of_column_affinity
        --
        -- Also, make table STRICT to prevent column affinity from tacking effect.
        CREATE TABLE object (
            hash       TEXT      NOT NULL PRIMARY KEY, -- Lower-case hex representation
                                                       -- of a SHA2 hash.

            last_seen  TEXT      NOT NULL              -- RFC 3339 date/time (in UTC)
        ) STRICT;
        INSERT INTO object (hash, last_seen) SELECT hash, last_seen from object_old;

        DROP TABLE object_old;

        PRAGMA user_version = 2;
        COMMIT;
    "#;
    conn.execute_batch(sql)?;
    Ok(())
}

fn upgrade_to_v3(conn: &Connection) -> Result<()> {
    let sql = r#"
        BEGIN;

        -- Make table STRICT and allow NULL value
        ALTER TABLE key_value RENAME TO key_value_old;
        CREATE TABLE key_value (
            key       TEXT  NOT NULL PRIMARY KEY CHECK (key <> ''),
            value     ANY
        ) STRICT;
        INSERT INTO key_value (key, value) SELECT key, value from key_value_old;

        DROP TABLE key_value_old;

        -- Convert hash to BLOB
        --
        -- This is stored more densely reducing DB size and, more importantly,
        -- allowing to cache more rows with the same amount of memory.
        ALTER TABLE object RENAME TO object_old;
        CREATE TABLE object (

            -- SHA2 hash
            hash       BLOB      NOT NULL PRIMARY KEY CHECK (length(hash) = 32),

            -- RFC 3339 date/time in UTC
            last_seen  TEXT      NOT NULL CHECK (last_seen GLOB '[0-9][0-9][0-9][0-9]-[0-3][0-9]-[0-3][0-9] [0-2][0-9]:[0-5][0-9]:[0-6][0-9]')
        ) STRICT;
        INSERT INTO object (hash, last_seen) SELECT unhex(hash), last_seen from object_old;
        DROP TABLE object_old;

        PRAGMA user_version = 3;
        COMMIT;
    "#;
    conn.create_scalar_function(
        "unhex",
        1,
        FunctionFlags::SQLITE_UTF8 | FunctionFlags::SQLITE_DETERMINISTIC,
        |ctx| {
            let value: String = ctx.get(0)?;
            hash::from_hex(&value)
                .map_err(|()| rusqlite::Error::ToSqlConversionFailure(Box::new(Error::InvalidHash)))
        },
    )?;
    conn.execute_batch(sql)?;
    Ok(())
}

fn upgrade_to_v4(conn: &Connection) -> Result<()> {
    let sql = r#"
        BEGIN;
        INSERT INTO key_value (key, value)
            VALUES ('last_s3_sync', '0000-01-01 00:00:00');

        PRAGMA user_version = 4;
        COMMIT;
    "#;
    conn.execute_batch(sql)?;
    Ok(())
}

fn upgrade_to_v5(conn: &Connection) -> Result<()> {
    let sql = r#"
        BEGIN;
            -- Convert timestamp to seconds since Unix Epoch
            ALTER TABLE object RENAME TO object_old;
            CREATE TABLE object (
                -- SHA2 hash
                hash       BLOB      NOT NULL PRIMARY KEY CHECK (length(hash) = 32),

                -- Seconds since Unix Epoch
                last_seen  INTEGER   NOT NULL
            ) STRICT;
            INSERT INTO object (hash, last_seen) SELECT hash, unixepoch(last_seen) FROM object_old;
            DROP TABLE object_old;

            PRAGMA user_version = 5;
        COMMIT;

        VACUUM;
    "#;
    conn.execute_batch(sql)?;
    Ok(())
}

/// See [`Db::next_objects_older_than()`]
pub struct ObjectsOlderThanPager {
    before_this_date: DateTime<Local>,
    page_size: usize,
    objects: Vec<Hash>,
    exhausted: bool,
}

impl ObjectsOlderThanPager {
    #[must_use]
    pub fn new(age: Duration, page_size: usize) -> ObjectsOlderThanPager {
        ObjectsOlderThanPager {
            before_this_date: chrono::Local::now() - age,
            page_size,
            exhausted: false,
            objects: Vec::with_capacity(page_size),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use chrono::{DurationRound, FixedOffset};
    use std::thread;

    #[test]
    fn batch_commit() {
        let db = tempfile::NamedTempFile::new().unwrap();
        let mut db = open_or_init(db.path()).unwrap();
        let hashes = [
            "039cf4480f0ded958a4a0b3c06f6cffa085f62af60fdce61d4124be279b06183",
            "440fb8e68877106c0d00476cd2cbf2150ef1f4daf1f82f61696972fd931daef2",
            "748e3b388e385c3ae6aa21df9a8a8bc6de74a3a21186e093b2a58cb60b0b544a",
        ];
        db.update_last_seen_times(hashes.iter().map(|h| Hash::new(h).unwrap()))
            .unwrap();
        let tx = db.inner.transaction().unwrap();
        let mut stmt = tx.prepare("SELECT hash FROM object ORDER BY 1").unwrap();
        let rows = stmt.query_map([], |row| row.get(0)).unwrap();
        let remaining: Vec<_> = rows
            .map(|r| r.unwrap())
            .map(|h: Hash| h.hex_digest())
            .collect();
        assert_eq!(hashes[..], remaining);
    }

    #[test]
    fn timestamp_insert() {
        let db = tempfile::NamedTempFile::new().unwrap();
        let mut db = open_or_init(db.path()).unwrap();
        let hash =
            Hash::new("039cf4480f0ded958a4a0b3c06f6cffa085f62af60fdce61d4124be279b06183").unwrap();
        db.insert_last_seen_times([&hash]).unwrap();
        let last_update1 = db.last_seen_time(&hash).unwrap().unwrap();
        let diff = chrono::Local::now() - last_update1;
        assert!(diff < Duration::seconds(10));
        assert!(diff >= Duration::seconds(-1));

        // second-precision timestamp
        thread::sleep(std::time::Duration::from_secs(1));

        // Update on existing object should have no effect.
        db.insert_last_seen_times([&hash]).unwrap();
        let last_update2 = db.last_seen_time(&hash).unwrap().unwrap();
        assert!(last_update1 == last_update2);
    }

    #[test]
    fn timestamp_update() {
        let db = tempfile::NamedTempFile::new().unwrap();
        let mut db = open_or_init(db.path()).unwrap();
        let hash =
            Hash::new("039cf4480f0ded958a4a0b3c06f6cffa085f62af60fdce61d4124be279b06183").unwrap();
        db.update_last_seen_times([&hash]).unwrap();
        let last_update1 = db.last_seen_time(&hash).unwrap().unwrap();
        let diff = chrono::Local::now() - last_update1;
        assert!(diff < Duration::seconds(10));
        assert!(diff >= Duration::seconds(-1));

        // second-precision timestamp
        thread::sleep(std::time::Duration::from_secs(1));

        db.update_last_seen_times([&hash]).unwrap();
        let last_update2 = db.last_seen_time(&hash).unwrap().unwrap();
        let diff = chrono::Local::now() - last_update2;
        assert!(diff < Duration::seconds(10));
        assert!(diff >= Duration::seconds(-1));
        assert!(last_update1 < last_update2);
    }

    #[test]
    fn remove_older_than() {
        let db = tempfile::NamedTempFile::new().unwrap();
        let mut db = open_or_init(db.path()).unwrap();
        db.inner
            .execute(
                r#"
            INSERT INTO object (hash, last_seen)
            VALUES
                -- keep
                (
                    x'3fd6b03c22abf1a9f06ca073e2349fdc65456ba3ad0944fe20bbf5f6e882ae4d',
                    unixepoch('now')
                ), (
                    x'9deb7768ca41250eb94f1699d93c5b17b04f1903855c60c31d63db53b8c84df5',
                    unixepoch('now')
                ), (
                    x'a7dc532d84d6b951c071dd9b71f5663a42f5e85ff864c42db33555eb02370258',
                    unixepoch('now')
                ),

                -- remove
                (
                    x'91de416cc4b936aae59ad1df04178dc6569c26cf01f2bc52d522506bd6694441',
                    unixepoch('now', '-7 months')
                ), (
                    x'bc37ef4b0f4c2332ae267c8257e571972db0716d8d97eb81ae388b75ecbfded6',
                    unixepoch('now', '-7 months')
                ), (
                    x'fde87d27d421485be43c3529b86315ef42cc0ec136dfcb797b0b8a3bf1f4873b',
                    unixepoch('now', '-7 months')
                )
        "#,
                [],
            )
            .unwrap();

        let mut pager = ObjectsOlderThanPager::new(Duration::days(180), 2);

        let hashes1 = db.next_objects_older_than(&mut pager).unwrap().unwrap();
        let hashes1 = hashes1.to_owned();
        assert_eq!(hashes1.len(), 2);

        let hashes2 = db.next_objects_older_than(&mut pager).unwrap().unwrap();
        let hashes2 = hashes2.to_owned();
        assert_eq!(hashes2.len(), 1);

        assert!(db.next_objects_older_than(&mut pager).unwrap().is_none());

        db.delete_objects(hashes1).unwrap();
        db.delete_objects(hashes2).unwrap();

        let tx = db.inner.transaction().unwrap();
        let mut stmt = tx.prepare("SELECT hash FROM object ORDER BY 1").unwrap();
        let rows = stmt.query_map([], |row| row.get(0)).unwrap();
        let remaining: Vec<_> = rows
            .map(|r| r.unwrap())
            .map(|h: Hash| h.hex_digest())
            .collect();
        assert_eq!(
            remaining,
            &[
                "3fd6b03c22abf1a9f06ca073e2349fdc65456ba3ad0944fe20bbf5f6e882ae4d",
                "9deb7768ca41250eb94f1699d93c5b17b04f1903855c60c31d63db53b8c84df5",
                "a7dc532d84d6b951c071dd9b71f5663a42f5e85ff864c42db33555eb02370258",
            ]
        );
    }

    #[test]
    fn last_update_last_seen_times() {
        let db = tempfile::NamedTempFile::new().unwrap();
        let db = open_or_init(db.path()).unwrap();

        #[allow(clippy::type_complexity)]
        let funcs: [(fn(_) -> _, fn(_) -> _); 3] = [
            (Db::update_last_db_scan_time, Db::last_db_scan_time),
            (
                Db::update_last_archive_scan_time,
                Db::last_archive_scan_time,
            ),
            (Db::update_last_s3_backup_time, Db::last_s3_backup_time),
        ];
        for (update_func, get_func) in funcs {
            assert_eq!(
                get_func(&db).unwrap(),
                chrono::DateTime::<FixedOffset>::parse_from_rfc3339("0000-01-01T00:00:00Z")
                    .unwrap()
            );

            let start = Local::now().duration_trunc(Duration::seconds(1)).unwrap();
            update_func(&db).unwrap();
            let finish = Local::now();
            let ts = get_func(&db).unwrap();
            assert!(ts >= start);
            assert!(ts <= finish);
        }
    }

    // If the column type isn't TEXT, the hash below would be convertet to the number `15`.
    //
    // See https://www.sqlite.org/datatype3.html#determination_of_column_affinity
    #[test]
    fn no_column_affinity() {
        let db = tempfile::NamedTempFile::new().unwrap();
        let mut db = open_or_init(db.path()).unwrap();

        let hash =
            Hash::new("0000000000000000000000000000000000000000000000000000000000000015").unwrap();
        db.update_last_seen_times([hash.clone()]).unwrap();

        let mut pager = ObjectsOlderThanPager::new(Duration::zero(), 2);
        let objects = db.next_objects_older_than(&mut pager).unwrap().unwrap();
        assert_eq!(objects, &[hash]);
    }

    #[test]
    fn last_seen_time() {
        let db = tempfile::NamedTempFile::new().unwrap();
        let mut db = open_or_init(db.path()).unwrap();

        let ts: DateTime<Local> =
            DateTime::<FixedOffset>::parse_from_rfc3339("2033-11-22T12:34:56-02:45")
                .unwrap()
                .into();
        let hash =
            Hash::new("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa").unwrap();
        db.set_last_seen_time(&hash, ts).unwrap();
        assert_eq!(ts, db.last_seen_time(&hash).unwrap().unwrap());
    }

    #[test]
    fn future_schema_version() {
        let db_file = tempfile::NamedTempFile::new().unwrap();
        let db = open_or_init(db_file.path()).unwrap();
        let current_version: u32 = db
            .inner
            .query_row("PRAGMA user_version", [], |row| row.get(0))
            .unwrap();

        // set to schema to version not yet supported
        let next_version = current_version + 1;
        db.inner
            // Parameters (like $1) appear to be unsupported for PRAGMA.
            //
            // safety: safe because next_version is guaranteed to be a number (type u32)
            .execute_batch(&format!("PRAGMA user_version = {next_version}"))
            .unwrap();

        match open_or_init(db_file.path()) {
            Ok(_) => panic!(),
            Err(e) => assert_eq!(format!("{}", e), format!("unsupported schema version {next_version} for state DB, newest supported is {current_version}")),
        }
    }
}
