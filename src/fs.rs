//! Object backups stored in file system
//!
//! ## File Hierarchy
//!
//! Objects are stored at `{bucket}/{hash}`:
//!
//! ```text
//! $ find -type f
//! …
//! tocco-nice-bis/002abc96c73d4a0b3bc4527f48e41c27be6f3aaa2259d98384d2c6a997312f72
//! tocco-nice-bis/00013d87b90fc73982d2e059db76f51eab39a4f938dd8a88f37b1f83a5404f16
//! tocco-nice-bis/000074d7521de059ed8df165a32dcdf43c250d4290b8eb32983427113ad45c55
//! tocco-nice-bis/00c928da8139c4d7a6bbdb0313c55069839f31bf12b19d2c296c86b09a4b8080
//! …
//! tocco-nice-ssb/ca930e167ad01e08ef08505bf9f431a2168a4d7cf939b1d744e1b0321fd7b28b
//! tocco-nice-ssb/de24e07233aa999a3d1e1e8309c0295e95d6f21035580c904c189d7653d4858c
//! tocco-nice-ssb/47c3284d768c499634427cffa88857737d1afb32be53871ecfd3e375d56b6c17
//! …
//! ```
//!
//! This code assumes all objects adhere to this scheme. Other directories and
//! files may exist and are ignored.
//!
//! ## Usage Example
//!
//! ```
//! use tocco_s3::fs::Archive;
//! use tocco_s3::hash::Hash;
//! use fallible_iterator::FallibleIterator;
//!
//! // First create an archive. Target directory must not exist.
//! let dir = tempfile::tempdir().unwrap().path().join("archive");
//! let archive = Archive::init(&dir).unwrap();
//!
//! // For the time being the archive is empty.
//! assert_eq!(archive.all_objects().unwrap().count().unwrap(), 0);
//!
//! // Once initialized, an archive can be opened …
//! let archive = Archive::open(&dir).unwrap();
//!
//! // … objects created …
//! let hash = Hash::new("ed7002b439e9ac845f22357d822bac1444730fbdb6016d3ec9432297b9ec9f73").unwrap();
//! archive.create_object(&"bucket1", &hash, &mut &b"content"[..]).unwrap();
//!
//! // … or fetched
//! let _object = archive.get_object(&"bucket1", &hash).unwrap();
//! ```
//!
//! ## Data Integrity
//!
//! [`Archive::create_object`] has to be used to create an object and
//! [`object::Object::copy_content`] to read an object. Both validate
//! the content against the objects checksum. An `Object`'s path is not
//! exposed to avoid bypassing this checks.

pub mod archive;
pub mod object;

pub use archive::Archive;
pub use object::Object;
