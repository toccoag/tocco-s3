/// Common utilities
use crate::hash;
use sha2::Digest;
use std::path::{Path, PathBuf};

/// Folder used, within the archive, to store temporary files.
pub const TEMP_DIR_NAME: &str = "_tmp";

/// Generate a reproducible, collision-free file name for temporary file
/// used during download.
#[must_use]
pub fn temp_file_path(archive: &Path, bucket: &str, object_path: &str) -> PathBuf {
    let mut path = archive.to_path_buf();
    path.push(TEMP_DIR_NAME);
    let hash = sha2::Sha256::new()
        .chain_update(bucket)
        .chain_update("/")
        .chain_update(object_path)
        .finalize();
    let file_name = hash::to_hex(hash[..].try_into().expect("hash of unexpected length"));
    path.push(file_name);
    path.set_extension("partial");
    path
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn temp_file_path() {
        let bucket = "bucket1";
        let object_path = "67279871e46f90ea0ab8043d49db8f37e719315bd9add3a2057b846db38be475";

        let path = super::temp_file_path(Path::new("/path/to/archive"), bucket, object_path);
        assert_eq!(
            path,
            Path::new("/path/to/archive/_tmp/b88fed9e087ecf204e92858f59f0e58e61aa0792cd32e12ef43e196e4f0fecc4.partial"),
        );

        let path = super::temp_file_path(Path::new(""), bucket, object_path);
        assert_eq!(
            path,
            Path::new(
                "_tmp/b88fed9e087ecf204e92858f59f0e58e61aa0792cd32e12ef43e196e4f0fecc4.partial"
            ),
        );
    }
}
