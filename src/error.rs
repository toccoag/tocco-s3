use aws_sdk_s3::operation::delete_objects::DeleteObjectsError;
use aws_sdk_s3::operation::get_object::GetObjectError;
use aws_sdk_s3::operation::head_object::HeadObjectError;
use aws_sdk_s3::operation::list_objects_v2::ListObjectsV2Error;
use aws_sdk_s3::operation::put_object::PutObjectError;
use std::io;

pub type Result<T> = std::result::Result<T, Error>;

#[derive(Debug, thiserror::Error)]
pub enum Error {
    #[error("{0}")]
    Custom(String),

    /// Invalid hash
    #[error("invalid hash")]
    InvalidHash,

    /// HTTP error
    #[error("HTTP error: {0}")]
    Http(#[from] reqwest::Error),

    /// I/O error
    #[error("i/o error: {0}")]
    Io(#[from] io::Error),

    /// SQLite error
    #[error("SQLite error: {0}")]
    SqLite(#[from] rusqlite::Error),

    /// Config parsing error
    #[error("archive config: {0}")]
    Toml(#[from] toml::de::Error),

    /// Error processing S3 object body
    // Use Debug because Display does not provide a meaningful error
    #[error("S3 body: {0:?}")]
    S3Stream(#[from] aws_sdk_s3::primitives::ByteStreamError),

    /// S3 delete error
    // Use Debug because Display does not provide a meaningful error
    #[error("S3 delete: {0:?}")]
    S3Delete(#[from] aws_sdk_s3::error::SdkError<DeleteObjectsError>),

    /// S3 get error
    // Use Debug because Display does not provide a meaningful error
    #[error("S3 get: {0:?}")]
    S3Get(#[from] aws_sdk_s3::error::SdkError<GetObjectError>),

    /// S3 head error
    // Use Debug because Display does not provide a meaningful error
    #[error("S3 head: {0:?}")]
    S3Head(#[from] aws_sdk_s3::error::SdkError<HeadObjectError>),

    /// S3 put error
    // Use Debug because Display does not provide a meaningful error
    #[error("S3 put: {0:?}")]
    S3Put(#[from] aws_sdk_s3::error::SdkError<PutObjectError>),

    /// S3 list error
    // Use Debug because Display does not provide a meaningful error
    #[error("s3 list: {0:?}")]
    S3List(#[from] aws_sdk_s3::error::SdkError<ListObjectsV2Error>),
}

impl Error {
    /// Retrieve underlying `io::Error` if any.
    #[must_use]
    pub fn io_error(&self) -> Option<&io::Error> {
        match *self {
            Error::Io(ref err) => Some(err),
            _ => None,
        }
    }
}
