//! Backup archive on disk
//!
//! See also documentation on [`crate::fs`].

use crate::cloudscale;
use crate::common::TEMP_DIR_NAME;
use crate::fs::object::{self, ListObjects, Object, ObjectFinder};
use crate::hash::Hash;
use crate::s3;
use crate::state;
use crate::Error;
use chrono::Duration;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::fs::File;
use std::io::{self, Read, Write};
use std::path::{Path, PathBuf};

const DEFAULT_CONFIG: &str = include_str!("default_config.toml");
const CONFIG_NAME: &str = "config.toml";

/// A backup archive on disk
#[derive(Clone, Debug)]
pub struct Archive {
    path: PathBuf,
    config: Config,
}

impl Archive {
    /// Create new archive
    ///
    /// See also [module documentation](`self`).
    ///
    /// # Errors
    ///
    /// * [`io::ErrorKind::AlreadyExists`] is returned when target
    ///   directory already exists:
    ///
    ///   ```
    ///   use std::io::ErrorKind;
    ///   use tocco_s3::fs::Archive;
    ///
    ///   assert_eq!(
    ///       Archive::init(&".").unwrap_err().io_error().unwrap().kind(),
    ///       ErrorKind::AlreadyExists,
    ///   );
    ///   ```
    pub fn init<P>(path: &P) -> Result<Archive, Error>
    where
        P: AsRef<Path>,
    {
        let path = path.as_ref();
        init_archive(path)?;
        Archive::open(&path)
    }

    /// Open existing archive
    ///
    /// See also [module documentation](`self`).
    ///
    /// # Errors
    ///
    /// * [`io::ErrorKind::NotFound`] is returned when the target
    ///   directory or config.toml therein does not exist:
    ///
    ///   ```
    ///   use std::io::ErrorKind;
    ///   use tocco_s3::fs::Archive;
    ///
    ///   // no config.toml
    ///   assert_eq!(
    ///       Archive::open(&".").unwrap_err().io_error().unwrap().kind(),
    ///       ErrorKind::NotFound
    ///   );
    ///
    ///   // no such file or directory
    ///   assert_eq!(
    ///       Archive::open(&"no/such/file").unwrap_err().io_error().unwrap().kind(),
    ///       ErrorKind::NotFound
    ///   );
    ///   ```
    pub fn open<P>(path: &P) -> Result<Archive, Error>
    where
        P: AsRef<Path>,
    {
        let path = path.as_ref();
        let config = parse_config(&config_path(path))?;
        if config.removal.retention_time_days < 90 {
            // Really low values are an issue because the last seen timestamps can
            // be a few days old and objects could be considered for removal incorrectly.
            // Safeguard by requiring this to be a sane value.
            return Err(Error::Custom(
                "Refusing to accept retention_time_days config lower than 90 days.".to_string(),
            ));
        }
        Ok(Archive {
            path: path.to_owned(),
            config,
        })
    }

    /// Path to this archive
    #[must_use]
    pub fn path(&self) -> &Path {
        &self.path
    }

    /// Return path to config.toml
    ///
    /// ```
    /// use tocco_s3::fs::Archive;
    /// use tocco_s3::hash::Hash;
    ///
    /// let dir = tempfile::tempdir().unwrap().path().join("archive");
    /// let archive = Archive::init(&dir).unwrap();
    /// assert_eq!(archive.config_path().file_name().unwrap(), "config.toml");
    /// ```
    #[must_use]
    pub fn config_path(&self) -> PathBuf {
        config_path(&self.path)
    }

    /// Get list of buckets in archive
    pub fn buckets(&self) -> io::Result<Vec<String>> {
        let reader = self.path.read_dir()?;
        let mut buckets = Vec::new();
        for entry in reader {
            let entry = entry?;
            if entry.file_type()?.is_dir() && entry.file_name() != TEMP_DIR_NAME {
                buckets.push(
                    entry
                        .file_name()
                        .into_string()
                        .expect("non-UTF8 bucket name"),
                );
            }
        }
        Ok(buckets)
    }

    /// Obtain iterator over all objects in all buckets
    pub fn all_objects(&self) -> io::Result<ListObjects> {
        ListObjects::new(self)
    }

    /// Obtain iterator over all objects in given `buckets`
    ///
    /// Buckets which don't exist are ignored silently.
    pub fn objects_for_buckets(&self, buckets: &[&str]) -> io::Result<ListObjects> {
        ListObjects::new_for_buckets(self, buckets)
    }

    /// Find objects by hash
    ///
    /// Reuse [`ObjectFinder`] to improve performance when
    /// looking up large number of objects. `ObjectFinder`
    /// keeps paths to all existing buckets in memory to
    /// reduce I/O requests and improve performance.
    ///
    /// However, this means a new [`ObjectFinder`] needs
    /// to be obtained to make newly created buckets
    /// visible.
    ///
    /// # Example
    ///
    /// ```
    /// use tocco_s3::fs::Archive;
    /// use tocco_s3::hash::Hash;
    ///
    /// let dir = tempfile::tempdir().unwrap().path().join("archive");
    /// let archive = Archive::init(&dir).unwrap();
    ///
    /// // Obtain finder
    /// let mut finder = archive.object_finder().unwrap();
    ///
    /// // Check if objects exists, reuse for better performance
    /// let hash1 =
    ///     Hash::new("4df9cf10cc84937a36d57e463109afc2bb40b73b983f9708d2a976f92f72fb6a").unwrap();
    /// let hash2 =
    ///     Hash::new("cb2c4ddfdb06fb4c65b71466166bfa162715d36457444dda7a8c622c6500d182").unwrap();
    /// assert!(!finder.exists_with_hash(&hash1).unwrap());
    /// assert!(!finder.exists_with_hash(&hash2).unwrap());
    /// ```
    pub fn object_finder(&self) -> io::Result<ObjectFinder> {
        ObjectFinder::new(&self.path)
    }

    /// Find objects by hash searching only given buckets.
    ///
    /// Bucket which do not exist are ignored.
    ///
    /// # Examples
    ///
    /// ```
    /// use tocco_s3::fs::Archive;
    /// use tocco_s3::hash::Hash;
    ///
    /// let dir = tempfile::tempdir().unwrap().path().join("archive");
    /// let archive = Archive::init(&dir).unwrap();
    ///
    /// // Obtain finder
    /// let mut finder = archive.object_finder_for_buckets(&["bucket1", "bucket2"]).unwrap();
    ///
    /// // Check if objects exists, reuse for better performance
    /// let hash1 =
    ///     Hash::new("4df9cf10cc84937a36d57e463109afc2bb40b73b983f9708d2a976f92f72fb6a").unwrap();
    /// let hash2 =
    ///     Hash::new("cb2c4ddfdb06fb4c65b71466166bfa162715d36457444dda7a8c622c6500d182").unwrap();
    /// assert!(!finder.exists_with_hash(&hash1).unwrap());
    /// assert!(!finder.exists_with_hash(&hash2).unwrap());
    /// ```
    ///
    /// [`io::ErrorKind::InvalidInput`] is returned if bucket name
    /// isn't valid:
    ///
    /// ```
    /// use tocco_s3::fs::Archive;
    /// use tocco_s3::hash::Hash;
    ///
    /// let dir = tempfile::tempdir().unwrap().path().join("archive");
    /// let archive = Archive::init(&dir).unwrap();
    ///
    /// assert!(archive
    ///     .object_finder_for_buckets(["not/valid"].iter())
    ///     .is_err());
    /// assert!(archive
    ///     .object_finder_for_buckets(["/not-valid"].iter())
    ///     .is_err());
    /// assert!(archive
    ///     .object_finder_for_buckets(["not-valid/"].iter())
    ///     .is_err());
    /// assert!(archive.object_finder_for_buckets(["/"].iter()).is_err());
    /// assert!(archive.object_finder_for_buckets(["."].iter()).is_err());
    /// assert!(archive.object_finder_for_buckets([".."].iter()).is_err());
    /// assert!(archive.object_finder_for_buckets([""].iter()).is_err());
    /// ```
    pub fn object_finder_for_buckets<I, B>(&self, buckets: I) -> io::Result<ObjectFinder>
    where
        I: IntoIterator<Item = B>,
        B: AsRef<str>,
    {
        let mut bucket_paths = Vec::new();
        for bucket in buckets {
            let bucket = bucket.as_ref();
            object::validate_bucket_name(bucket)?;
            bucket_paths.push(self.path.join(bucket));
        }
        Ok(ObjectFinder {
            buckets: bucket_paths,
            last_bucket: None,
        })
    }

    /// Get object from disk
    ///
    /// ```
    /// use tocco_s3::fs::Archive;
    /// use tocco_s3::hash::Hash;
    ///
    /// let dir = tempfile::tempdir().unwrap().path().join("archive");
    /// let archive = Archive::init(&dir).unwrap();
    /// let content = b"data";
    /// let hash =
    ///     Hash::new("3a6eb0790f39ac87c94f3856b2dd2c5d110e6811602261a9a923d3bb23adc8b7").unwrap();
    ///
    /// let mut reader = &content[..];
    /// let obj1 = archive
    ///     .create_object("bucket1", &hash, &mut reader)
    ///     .unwrap();
    ///
    /// let obj2 = archive.get_object("bucket1", &hash).unwrap().unwrap();
    ///
    /// assert_eq!(obj1, obj2);
    /// ```
    ///
    /// [`None`] is returned when object doesn't exist:
    ///
    /// ```
    /// use tocco_s3::fs::Archive;
    /// use tocco_s3::hash::Hash;
    ///
    /// let dir = tempfile::tempdir().unwrap().path().join("archive");
    /// let archive = Archive::init(&dir).unwrap();
    ///
    /// let hash =
    ///     Hash::new("ff9e17b6d74acd985df8567e30592f23bb0333575ec92c31106a245e95a020f7").unwrap();
    /// assert!(archive.get_object("bucket1", &hash).unwrap().is_none());
    /// ```
    pub fn get_object(&self, bucket: &str, hash: &Hash) -> io::Result<Option<Object>> {
        super::object::get_object(&self.path, bucket, hash)
    }

    ///  Get object on disk without checking if it exists
    ///
    /// Like [`Self::get_object`] but will not look up the object on disk to check
    /// if it exists.
    ///
    /// Use when you know object is valid and exists.
    ///
    /// # Panics
    ///
    /// Panics if bucket name is invalid (i.e. contains '/' or is ".", ".." or empty).
    #[must_use]
    pub fn get_object_unchecked(&self, bucket: &str, hash: &Hash) -> Object {
        object::validate_bucket_name(bucket).expect("invalid bucket name");
        let path = [&self.path, Path::new(bucket), Path::new(&hash.hex_digest())]
            .iter()
            .collect();
        super::object::Object::new(path)
    }

    /// Create object on disk
    ///
    /// Object is first written to a temporary file, the hash is verified, and if
    /// it matches the file is renamed.
    ///
    /// [`File::sync_all`] is used to ensure content is written to disk before renaming
    /// it. This ensures the file won't be truncated after an OS crash or hardware
    /// failure. However, no attempt is made to sync any directories and, thus, there is no
    /// guarantee files created will exists after such an event.
    ///
    /// ```
    /// use tocco_s3::fs::Archive;
    /// use tocco_s3::hash::Hash;
    ///
    /// let dir = tempfile::tempdir().unwrap().path().join("archive");
    /// let archive = Archive::init(&dir).unwrap();
    /// let bucket = "bucket1";
    /// let content = b"test data";
    /// let hash =
    ///     Hash::new("916f0027a575074ce72a331777c3478d6513f786a591bd892da1a577bf2335f9").unwrap();
    ///
    /// let mut reader = &content[..];
    /// archive.create_object(&bucket, &hash, &mut reader).unwrap();
    ///
    /// assert_eq!(
    ///     dir.join(&bucket)
    ///         .join(hash.hex_digest())
    ///         .metadata()
    ///         .unwrap()
    ///         .len(),
    ///     content.len() as u64
    /// );
    /// ```
    ///
    /// # Errors
    ///
    /// * [`io::Error`]s as result of file/directory creating or removal are
    ///   passed through. Temporary file created during download is removed.
    /// * [`io::Error`]s as result reading and writing are passed through.
    /// * [`io::ErrorKind::AlreadyExists`] is returned when object already exists:
    ///
    ///   ```
    ///   use tocco_s3::fs::Archive;
    ///   use tocco_s3::hash::Hash;
    ///
    ///   let dir = tempfile::tempdir().unwrap().path().join("archive");
    ///   let archive = Archive::init(&dir).unwrap();
    ///   let bucket = "bucket1";
    ///   let content = b"test data";
    ///   let hash = Hash::new("916f0027a575074ce72a331777c3478d6513f786a591bd892da1a577bf2335f9").unwrap();
    ///
    ///   // create object
    ///   archive.create_object(&bucket, &hash, &mut &content[..]).unwrap();
    ///
    ///   // already exists
    ///   assert_eq!(
    ///       archive.create_object(&bucket, &hash, &mut &content[..]).unwrap_err().kind(),
    ///       std::io::ErrorKind::AlreadyExists,
    ///   );
    ///   ```
    /// * [`io::ErrorKind::InvalidData`] is returned when the hash does not match:
    ///
    ///   ```
    ///   use tocco_s3::fs::Archive;
    ///   use tocco_s3::hash::Hash;
    ///
    ///   let dir = tempfile::tempdir().unwrap().path().join("archive");
    ///   let archive = Archive::init(&dir).unwrap();
    ///   let bucket = "bucket1";
    ///   let content = b"test data";
    ///   let hash = Hash::new("0000000000000000000000000000000000000000000000000000000000000000").unwrap();
    ///
    ///   assert_eq!(
    ///       archive.create_object(&bucket, &hash, &mut &content[..]).unwrap_err().kind(),
    ///       std::io::ErrorKind::InvalidData,
    ///   );
    ///   ```
    pub fn create_object(
        &self,
        bucket: &str,
        hash: &Hash,
        reader: &mut dyn Read,
    ) -> io::Result<Object> {
        super::object::create_object(&self.path, bucket, hash, reader)
    }

    /// Return DB server information from `config.toml`.
    #[must_use]
    pub fn db_servers(&self) -> &[DbServer] {
        &self.config.db_servers
    }

    /// Whether dry-run is enabled for object removal
    #[must_use]
    pub fn removal_dry_run_enabled(&self) -> bool {
        self.config.removal.dry_run
    }

    /// Retention time for removed objects
    ///
    /// For how much longer objects are kept after
    /// they are no longer referenced by any DB.
    #[must_use]
    #[allow(clippy::missing_panics_doc)]
    pub fn retention_time(&self) -> Duration {
        Duration::days(
            self.config
                .removal
                .retention_time_days
                .try_into()
                .expect("invalid retention time"),
        )
    }

    /// Open state DB associated with archive
    pub fn state_db(&self) -> Result<state::Db, Error> {
        let path = self.path.join(&self.config.archive.state_db_path);
        state::open_or_init(&path)
    }

    /// Min. number of objects that must exist in archive
    ///
    /// Defined in `config.toml`.
    #[must_use]
    pub fn min_objects(&self) -> u64 {
        self.config.archive.min_objects
    }

    /// Cloudscale API key
    ///
    /// Used to retrieve objects users and corresponding keys.
    #[must_use]
    pub fn cloudscale_api_key(&self) -> Option<&str> {
        match &self.config.s3.keys {
            Keys::CloudscaleApiKey(api_key) => Some(api_key),
            Keys::Keys(_) => None,
        }
    }

    /// Get S3 API keys
    ///
    /// Returns a `HashMap` where the key is the bucket name and the value
    /// the [`s3::Key`] that belongs to it.
    pub async fn s3_keys(&self) -> Result<HashMap<String, s3::Key>, Error> {
        match &self.config.s3.keys {
            Keys::CloudscaleApiKey(api_key) => {
                let keys = cloudscale::users(api_key)
                    .await?
                    .iter()
                    .map(|(bucket, user)| (bucket.clone(), user.key().clone()))
                    .collect();
                Ok(keys)
            }
            Keys::Keys(keys) => Ok(keys.clone()),
        }
    }

    /// S3 endpoint
    #[must_use]
    pub fn s3_endpoint(&self) -> &str {
        &self.config.s3.endpoint
    }
}

fn init_archive(path: &Path) -> Result<(), Error> {
    if path.try_exists()? {
        return Err(io::Error::new(io::ErrorKind::AlreadyExists, "archive already exists").into());
    }
    let tmp_path = path.join(TEMP_DIR_NAME);
    std::fs::create_dir_all(tmp_path)?;
    let mut file = File::create(config_path(path))?;
    file.write_all(DEFAULT_CONFIG.as_bytes())?;
    Ok(())
}

fn ssh_uri(server: &DbServer) -> String {
    use std::fmt::Write;

    let mut uri = "ssh://".to_string();
    if let Some(ssh_user) = &server.ssh_user {
        write!(&mut uri, "{ssh_user}@").expect("writing to string failed");
    }
    uri.push_str(&server.host_name);
    if let Some(ssh_port) = &server.ssh_port {
        write!(&mut uri, ":{ssh_port}").expect("writing to string failed");
    }
    uri
}

#[derive(Clone, Debug, Deserialize)]
struct Config {
    archive: ArchiveConfig,
    db_servers: Vec<DbServer>,
    removal: RemovalConfig,
    s3: S3Config,
}

#[derive(Clone, Debug, Deserialize)]
struct ArchiveConfig {
    #[serde(default = "default_state_db_path")]
    state_db_path: String,

    #[serde(default = "one")]
    min_objects: u64,
}

#[derive(Clone, Debug, Deserialize)]
struct S3Config {
    endpoint: String,

    #[serde(flatten)]
    keys: Keys,
}

#[derive(Clone, Debug, Deserialize)]
enum Keys {
    #[serde(rename = "cloudscale_api_key")]
    CloudscaleApiKey(String),

    #[serde(rename = "keys")]
    Keys(HashMap<String, s3::Key>),
}

#[derive(Clone, Debug, Deserialize)]
struct RemovalConfig {
    #[serde(default)]
    dry_run: bool,
    retention_time_days: u64,
}

/// DB server configuration from `config.toml`.
#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct DbServer {
    host_name: String,
    ssh_user: Option<String>,
    ssh_port: Option<u16>,
    #[serde(default = "one")]
    min_objects: u64,
}

fn one() -> u64 {
    1
}

fn default_state_db_path() -> String {
    "state.sqlite".to_string()
}

impl DbServer {
    /// Name of DB server.
    ///
    /// Host name only.
    #[must_use]
    pub fn name(&self) -> &str {
        &self.host_name
    }

    /// ssh:// URI of host
    #[must_use]
    pub fn ssh_uri(&self) -> String {
        ssh_uri(self)
    }

    /// Min. objects expected on server.
    #[must_use]
    pub fn min_objects(&self) -> u64 {
        self.min_objects
    }
}

fn config_path(archive_dir: &Path) -> PathBuf {
    archive_dir.join(CONFIG_NAME)
}

fn parse_config(path: &Path) -> Result<Config, Error> {
    let content = std::fs::read_to_string(path).map_err(|err| match err.kind() {
        io::ErrorKind::NotFound => io::Error::new(io::ErrorKind::NotFound, "not an archive"),
        _ => err,
    })?;

    Ok(toml::from_str(&content)?)
}

#[cfg(test)]
mod test {
    use super::*;
    use std::fs;

    #[test]
    fn ssh_uri_test() {
        let server = DbServer {
            host_name: "db.example.net".to_string(),
            ssh_user: None,
            ssh_port: None,
            min_objects: 0,
        };
        assert_eq!(&ssh_uri(&server), "ssh://db.example.net");

        let server = DbServer {
            host_name: "db1.example.org".to_string(),
            ssh_user: Some("user".to_string()),
            ssh_port: None,
            min_objects: 0,
        };
        assert_eq!(&ssh_uri(&server), "ssh://user@db1.example.org");

        let server = DbServer {
            host_name: "db1.example.org".to_string(),
            ssh_user: None,
            ssh_port: Some(2222),
            min_objects: 0,
        };
        assert_eq!(&ssh_uri(&server), "ssh://db1.example.org:2222");

        let server = DbServer {
            host_name: "db1.example.net".to_string(),
            ssh_user: Some("postgres".to_string()),
            ssh_port: Some(22),
            min_objects: 12,
        };
        assert_eq!(&ssh_uri(&server), "ssh://postgres@db1.example.net:22");
    }

    #[test]
    fn retention_time_too_low() {
        let dir = tempfile::tempdir().unwrap().path().join("archive");
        let _ = Archive::init(&dir).unwrap();
        let config = r#"
            [archive]

            [s3]
            endpoint = "https://objects.rma.cloudscale.ch"

            # setting keys instead
            # cloudscale_api_key = "unused"

            [s3.keys.tocco-nice-abc]
            access_key = "access_key_id_of_abc"
            secret_key = "secret_of_abc"

            [s3.keys.tocco-nice-xyz]
            access_key = "access_key_id_of_xyz"
            secret_key = "secret_of_xyz"

            [removal]
            retention_time_days = 79

            [[db_servers]]
            host_name = "localhost"
        "#;
        fs::write(dir.join("config.toml"), config).unwrap();
        let err = Archive::open(&dir).unwrap_err();
        assert_eq!(
            format!("{err}"),
            "Refusing to accept retention_time_days config lower than 90 days."
        );
    }
}
