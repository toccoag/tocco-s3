//! Object backup stored in file system
//!
//! Obtain objects via [`crate::fs::Archive`]. See
//! also documentation on [`crate::fs`].

use crate::common;
use crate::hash::{is_hash_valid, Hash, HashReader};
use crate::{Archive, Result};
use aws_sdk_s3::primitives::ByteStream as S3ByteStream;
use fallible_iterator::FallibleIterator;
#[cfg(unix)]
use nix::fcntl;
use std::cmp::Ordering;
use std::ffi::OsStr;
use std::fmt;
use std::fs::{self, DirEntry};
use std::io::{self, ErrorKind, Read, Write};
#[cfg(unix)]
use std::os::fd::{AsFd, AsRawFd};
use std::path;
use std::path::{Path, PathBuf};
use std::rc::Rc;

/// A hash object on disk
///
/// `Object` can be obtained via [`crate::fs::Archive::create_object`] or
/// [`crate::fs::Archive::get_object`].
#[derive(Eq)]
pub struct Object {
    path: PathBuf,
}

/// Non-Hash object in backup archive
#[derive(Eq, PartialEq, Ord, PartialOrd)]
pub struct NonHashObject {
    bucket_dir: Rc<PathBuf>,
    relative_path: PathBuf,
}

impl NonHashObject {
    /// Bucket name
    #[must_use]
    pub fn bucket(&self) -> &str {
        self.bucket_dir
            .file_name()
            .expect("missing bucket dir")
            .to_str()
            .expect("non-UTF8 bucket name")
    }

    /// Path relative to archive
    ///
    /// In form `"{bucket}/{key}"`.
    #[must_use]
    pub fn relative_path(&self) -> &Path {
        &self.relative_path
    }

    /// Convert into path relative to archive
    ///
    /// In form `"{bucket}/{key}"`.
    #[must_use]
    pub fn into_relative_path(self) -> PathBuf {
        self.relative_path
    }

    /// Absolute path to object in archive
    #[must_use]
    pub fn absolute_path(&self) -> PathBuf {
        self.bucket_dir.join(&self.relative_path)
    }
}

impl Object {
    /// Create hash object
    ///
    /// It's up to the caller to ensure path ends in `"{bucket}/{hash}"`.
    pub(crate) fn new(path: PathBuf) -> Self {
        let obj = Object { path };

        if cfg!(debug_assertions) {
            let _ = obj.bucket();
            drop(obj.hash());
        }

        obj
    }

    /// Delete this object from disk
    ///
    ///  ```
    ///  use std::io;
    ///  use tocco_s3::fs::Archive;
    ///  use tocco_s3::hash::Hash;
    ///
    ///  let dir = tempfile::tempdir().unwrap().path().join("archive");
    ///  let archive = Archive::init(&dir).unwrap();
    ///  let bucket = "bucket1";
    ///  let hash = Hash::new("e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855").unwrap();
    ///
    ///  // create object
    ///  let object = archive.create_object(&bucket, &hash, &mut io::empty()).unwrap();
    ///
    ///  // object exists
    ///  assert_eq!(archive.get_object(&bucket, &hash).unwrap().unwrap(), object);
    ///
    ///  // delete object
    ///  object.delete().unwrap();
    ///
    ///  // object gone
    ///  assert!(archive.get_object(&bucket, &hash).unwrap().is_none());
    ///  ```
    pub fn delete(self) -> io::Result<()> {
        fs::remove_file(self.path)
    }

    /// Get name of S3 buckets to which this `Objects` belongs
    #[must_use]
    #[allow(clippy::missing_panics_doc)]
    pub fn bucket(&self) -> &str {
        self.path
            .parent()
            .expect("object missing in path")
            .file_name()
            .expect("bucket missing in path")
            .to_str()
            .expect("non-utf8 bucket name")
    }

    /// Get `Hash` of this object
    #[must_use]
    #[allow(clippy::missing_panics_doc)]
    pub fn hash(&self) -> Hash {
        Hash::new(
            self.path
                .file_name()
                .expect("object missing in path")
                .to_str()
                .expect("non-utf8 bucket name"),
        )
        .expect("file name not a valid hash")
    }

    /// Copy content of object to `writer`
    ///
    /// [`fcntl::posix_fadvise`] is used to advise the OS to cache content
    /// in advance of reading and to drop the cache after reading. This makes
    /// this method unsuitable for repeated reads.
    ///
    /// Errors
    ///
    /// * [`io::Error`]s as result of file/directory creating or removal are
    ///   passed through.
    /// * [`io::Error`]s as result reading and writing are passed through.
    /// * [`io::ErrorKind::InvalidData`] is returned when the hash does not match.
    ///
    ///   ```
    ///   use std::{fs, io};
    ///   use tocco_s3::fs::Archive;
    ///   use tocco_s3::hash::Hash;
    ///
    ///   let dir = tempfile::tempdir().unwrap().path().join("archive");
    ///   let archive = Archive::init(&dir).unwrap();
    ///   let bucket = "bucket1";
    ///   let content = b"test data";
    ///   let hash = Hash::new("0000000000000000000000000000000000000000000000000000000000000000").unwrap();
    ///
    ///   let bucket_path = dir.join(&bucket);
    ///   fs::create_dir(&bucket_path).unwrap();
    ///   let object_path = bucket_path.join(&hash.hex_digest());
    ///   fs::File::create(&object_path).unwrap();
    ///
    ///   let object = archive.get_object(&bucket, &hash).unwrap().unwrap();
    ///   assert_eq!(
    ///       object.copy_content(&mut io::sink()).unwrap_err().kind(),
    ///       std::io::ErrorKind::InvalidData
    ///   );
    ///   ```
    pub fn copy_content<W>(&self, mut writer: &mut W) -> io::Result<()>
    where
        W: Write,
    {
        let mut file = fs::File::open(&self.path)?;
        // Advise OS to read ahead
        #[cfg(unix)]
        fadvise(
            &file,
            fcntl::PosixFadviseAdvice::POSIX_FADV_WILLNEED,
            0,
            50 * 1024 * 1024, // 50 MiB
        )?;

        {
            let mut reader = HashReader::new(&mut file, &self.hash());
            io::copy(&mut reader, &mut writer)?;
        }

        // Advise OS that we won't need (cached) content again
        #[cfg(unix)]
        fadvise(&file, fcntl::PosixFadviseAdvice::POSIX_FADV_DONTNEED, 0, 0)?;
        Ok(())
    }

    /// Obtain reader for object
    ///
    /// # Errors
    ///
    /// * [`io::Error`] is returned when file can't be opened.
    /// * When hash does not match [`io::ErrorKind::InvalidData`] is returned
    ///   on last read.
    pub fn read(&self) -> io::Result<HashReader> {
        let file = fs::File::open(&self.path)?;
        Ok(HashReader::new(file, &self.hash()))
    }

    /// Obtain bytes stream for use by S3 SDK
    ///
    /// Note that this reader, unlike [`Self::read()`], does not
    /// verify the objects checksum.
    pub async fn s3_sdk_read(&self) -> Result<S3ByteStream> {
        Ok(S3ByteStream::from_path(&self.path).await?)
    }

    /// Check file content against checksum
    ///
    /// Returns [`io::ErrorKind::InvalidData`] when checksum
    /// doesn't match.
    pub fn check_integrity(&self) -> io::Result<()> {
        self.copy_content(&mut io::sink())
    }

    /// Returns `(bucket, object)`
    #[must_use]
    fn id(&self) -> (&OsStr, &OsStr) {
        let mut iter = self.path.iter().rev();
        let object = iter.next().expect("object missing in path");
        let bucket = iter.next().expect("bucket missing in path");
        (bucket, object)
    }
}

impl fmt::Debug for Object {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let (bucket, object) = self.id();
        write!(
            f,
            r#""{}/{}""#,
            bucket.to_string_lossy().escape_debug(),
            object.to_string_lossy().escape_debug()
        )
    }
}

impl fmt::Debug for NonHashObject {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            r#""{}/{}""#,
            self.bucket().escape_debug(),
            self.relative_path.to_string_lossy().escape_debug(),
        )
    }
}

impl PartialEq for Object {
    fn eq(&self, other: &Object) -> bool {
        self.id() == other.id()
    }
}

impl Ord for Object {
    fn cmp(&self, other: &Object) -> Ordering {
        self.id().cmp(&other.id())
    }
}

impl PartialOrd for Object {
    fn partial_cmp(&self, other: &Object) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

/// Finder for finding objects with a specific hash
///
/// Create via [`crate::fs::Archive::object_finder()`].
#[allow(clippy::module_name_repetitions)]
pub struct ObjectFinder {
    /// All directories in root directory (i.e. all buckets)
    pub(crate) buckets: Vec<PathBuf>,

    /// Path to last bucket where an object was found.
    ///
    /// See [`Self::exists_with_hash()`]
    pub(crate) last_bucket: Option<PathBuf>,
}

impl ObjectFinder {
    pub(crate) fn new<P>(path: &P) -> io::Result<ObjectFinder>
    where
        P: AsRef<Path>,
    {
        let mut buckets = Vec::new();
        for dirent in path.as_ref().read_dir()? {
            match dirent {
                Ok(dirent) if dirent.metadata()?.is_dir() => {
                    buckets.push(dirent.path());
                }
                Ok(_) => {
                    // not a directory
                }
                Err(e) => return Err(e),
            }
        }
        Ok(ObjectFinder {
            buckets,
            last_bucket: None,
        })
    }

    /// Find all objects with given `hash`
    ///
    /// Returns an iterator which yields objects from all buckets
    /// matching given `hash`.
    ///
    /// Objects are returned in arbitrary order.
    pub fn all_with_hash<'a>(&'a self, hash: &'a Hash) -> FindObjectsByHash<'a> {
        FindObjectsByHash {
            root_dir: self.buckets.iter(),
            hash,
        }
    }

    /// Test whether an object with given `hash` exists in any bucket.
    ///
    /// This function is optimized for sucessive calls where the object
    /// is likely in the same bucket. See code.
    ///
    /// ```
    /// use tocco_s3::fs::Archive;
    /// use tocco_s3::hash::Hash;
    ///
    /// let dir = tempfile::tempdir().unwrap().path().join("archive");
    /// let archive = Archive::init(&dir).unwrap();
    /// let hash = "c5177285e72ffe78743f44d6c05c25f2ef912f067ddc1178f1066f2eedfcabfa";
    ///
    /// // no such object
    /// let mut finder = archive.object_finder().unwrap();
    /// assert!(!finder.exists_with_hash(&Hash::new(hash).unwrap()).unwrap());
    ///
    /// std::fs::create_dir(dir.join("bucket1")).unwrap();
    /// std::fs::File::create(dir.join(format!("bucket1/{}", hash))).unwrap();
    ///
    /// // object found
    /// let mut finder = archive.object_finder().unwrap();
    /// assert!(finder.exists_with_hash(&Hash::new(hash).unwrap()).unwrap());
    /// ```
    pub fn exists_with_hash(&mut self, hash: &Hash) -> io::Result<bool> {
        if let Some(last_bucket) = &self.last_bucket {
            // Fast path
            //
            // In 99.99% of cases, consecutive objects are in the same bucket.
            // To avoid scanning possibly hundreds of buckets, try to find
            // the object in the same bucket as the last one first.

            let object_path = last_bucket.join(hash.hex_digest());
            if is_path_an_object(&object_path)? {
                return Ok(true);
            }
        }

        if let Some(last_bucket) = self.all_with_hash(hash).next()? {
            let Object { mut path } = last_bucket;
            path.pop();
            self.last_bucket = Some(path);
            Ok(true)
        } else {
            Ok(false)
        }
    }
}

/// Iterator over all objects with given `hash`.
///
/// Can be created via [`ObjectFinder::all_with_hash()`].
#[must_use]
pub struct FindObjectsByHash<'a> {
    /// Root directory (i.e. directory that contains buckets)
    root_dir: std::slice::Iter<'a, PathBuf>,
    hash: &'a Hash,
}

impl FallibleIterator for FindObjectsByHash<'_> {
    type Item = Object;
    type Error = io::Error;

    fn next(&mut self) -> io::Result<Option<Object>> {
        let hex_digest = self.hash.hex_digest();
        loop {
            if let Some(path) = self.root_dir.next() {
                let path = path.join(&hex_digest);
                if is_path_an_object(&path)? {
                    break Ok(Some(Object::new(path)));
                }
            } else {
                break Ok(None);
            }
        }
    }
}

fn is_path_an_object(path: &Path) -> io::Result<bool> {
    match path.metadata() {
        Ok(meta) if meta.is_file() => Ok(true),
        Ok(_) => {
            info!("Ignoring non-regular file {:?}.", path);
            Ok(false)
        }
        Err(e) if e.kind() == io::ErrorKind::NotFound => {
            // not found in bucket
            Ok(false)
        }
        Err(e) => Err(e),
    }
}

pub(crate) fn get_object<P>(dir: &P, bucket: &str, hash: &Hash) -> io::Result<Option<Object>>
where
    P: AsRef<Path>,
{
    let mut path = dir.as_ref().to_path_buf();
    path.push(bucket);
    path.push(hash.hex_digest());
    match path.metadata() {
        Ok(m) if m.is_file() => Ok(Some(Object::new(path))),
        Ok(_) => Ok(None),
        Err(e) if e.kind() == io::ErrorKind::NotFound => Ok(None),
        Err(e) => Err(e),
    }
}

pub(crate) fn create_object<P>(
    dir: P,
    bucket: &str,
    hash: &Hash,
    reader: &mut dyn Read,
) -> io::Result<Object>
where
    P: AsRef<Path>,
{
    let mut reader = HashReader::new(reader, hash);
    let mut path = dir.as_ref().to_path_buf();

    path.push(bucket);
    maybe_create_dir(&path)?;

    path.push(hash.hex_digest());
    if path.try_exists()? {
        return Err(io::Error::new(
            io::ErrorKind::AlreadyExists,
            "object already exists",
        ));
    }

    let path_tmp = common::temp_file_path(dir.as_ref(), bucket, &hash.hex_digest());
    {
        // `create()` truncates existing file
        let mut file = fs::File::create(&path_tmp)?;
        io::copy(&mut reader, &mut file)?;
        file.flush()?;
        file.sync_all()?;
    }

    fs::rename(&path_tmp, &path)?;
    Ok(Object::new(path))
}

/// Create directory if it doesn't exist
fn maybe_create_dir(path: &dyn AsRef<Path>) -> io::Result<()> {
    match fs::create_dir(path) {
        Err(e) if e.kind() == io::ErrorKind::AlreadyExists => Ok(()),
        result => result,
    }
}

/// Iterator over all objects in all buckets
///
/// Can be constructed via [`crate::fs::Archive::all_objects`].
pub struct ListObjects {
    /// Buckets whose objects are listed
    buckets: Vec<String>,

    /// Location of archive
    root_dir: PathBuf,

    /// Open directories
    ///
    /// The first directory reader is the bucket directory. Others are
    /// subdirectories thereof.
    dirs: Vec<fs::ReadDir>,

    /// Path to bucket within archive
    bucket_dir: Option<Rc<PathBuf>>,
}

impl ListObjects {
    pub(crate) fn new(archive: &Archive) -> io::Result<Self> {
        let mut obj = ListObjects {
            buckets: archive.buckets()?,
            root_dir: archive.path().to_owned(),
            dirs: Vec::new(),
            bucket_dir: None,
        };
        obj.next_bucket()?;
        Ok(obj)
    }

    pub(crate) fn new_for_buckets(archive: &Archive, buckets: &[&str]) -> io::Result<Self> {
        let mut obj = ListObjects {
            buckets: buckets.iter().map(|b| (*b).to_string()).collect(),
            root_dir: archive.path().to_owned(),
            dirs: Vec::new(),
            bucket_dir: None,
        };
        obj.next_bucket()?;
        Ok(obj)
    }

    /// Advance to next bucket directory.
    fn next_bucket(&mut self) -> io::Result<()> {
        debug_assert!(
            self.dirs.len() < 2,
            "cannot change bucket while scanning sub-directory"
        );
        loop {
            match self.buckets.pop() {
                Some(bucket) => {
                    let bucket_path = self.root_dir.join(bucket);
                    match bucket_path.read_dir() {
                        Ok(reader) => {
                            self.dirs = vec![reader];
                            self.bucket_dir = Some(Rc::new(bucket_path));
                            break;
                        }
                        Err(e) if e.kind() == io::ErrorKind::NotFound => {
                            // bucket / file vanished meanwhile, try next …
                        }
                        Err(e) => return Err(e),
                    };
                }
                None => {
                    // No bucket left, all done.
                    self.dirs = Vec::new();
                    self.bucket_dir = None;
                    break;
                }
            }
        }
        Ok(())
    }

    fn process_dir_entry(
        &mut self,
        object: &DirEntry,
        is_bucket_dir: bool,
    ) -> io::Result<Option<ArchiveEntry>> {
        let file_type = object.file_type()?;
        if file_type.is_file() {
            let file_name = object.file_name();
            let file_name = file_name.to_str().expect("non-utf8 file name");
            if is_bucket_dir && is_hash_valid(file_name) {
                Ok(Some(ArchiveEntry::Hash(Object::new(object.path()))))
            } else {
                let bucket_dir = self
                    .bucket_dir
                    .as_ref()
                    .expect("no bucket while processing bucket");
                let object = NonHashObject {
                    bucket_dir: Rc::clone(bucket_dir),
                    relative_path: object
                        .path()
                        .strip_prefix(&**bucket_dir)
                        .expect("not subdirectory of its own bucket")
                        .to_owned(),
                };
                Ok(Some(ArchiveEntry::NonHash(object)))
            }
        } else if file_type.is_dir() {
            self.dirs.push(object.path().read_dir()?);
            // continue search in sub-directory during next loop …
            Ok(None)
        } else {
            Err(io::Error::new(
                ErrorKind::InvalidData,
                format!("unsupported file type for {:?}", object.path()),
            ))
        }
    }
}

impl FallibleIterator for ListObjects {
    type Item = ArchiveEntry;
    type Error = io::Error;

    fn next(&mut self) -> io::Result<Option<ArchiveEntry>> {
        loop {
            let is_bucket_dir = self.dirs.len() == 1;
            match &mut self.dirs[..] {
                [] => {
                    // no more buckets
                    break Ok(None);
                }
                [.., bucket_dir] => {
                    match bucket_dir.next() {
                        Some(Ok(object)) => {
                            if let Some(object) = self.process_dir_entry(&object, is_bucket_dir)? {
                                break Ok(Some(object));
                            }
                            // directory entry not an object, continue search in next loop
                        }
                        Some(Err(e)) if e.kind() == io::ErrorKind::NotFound => {
                            info!("File vanished during directory scan: {e}");
                            // continue search in next loop
                        }
                        Some(Err(e)) => {
                            break Err(io::Error::new(
                                e.kind(),
                                format!("failed to advance to next object: {e}"),
                            ))
                        }
                        None => {
                            // End of directory
                            if is_bucket_dir {
                                self.next_bucket()?;
                                // continue search in next bucket on next loop
                            } else {
                                self.dirs.pop();
                                // continue search in parent dir on next loop
                            }
                        }
                    }
                }
            }
        }
    }
}

pub(crate) fn validate_bucket_name(name: &str) -> io::Result<()> {
    if !name.is_empty()
        && name != "."
        && name != ".."
        && name.chars().all(|c| !path::is_separator(c))
    {
        Ok(())
    } else {
        Err(io::Error::new(
            io::ErrorKind::InvalidInput,
            format!("{name:?} not a valid bucket name"),
        ))
    }
}

/// An object in the local archive
#[derive(Debug, PartialEq, Eq)]
pub enum ArchiveEntry {
    Hash(Object),
    NonHash(NonHashObject),
}

impl ArchiveEntry {
    #[must_use]
    pub fn into_hash_object(self) -> Option<Object> {
        match self {
            ArchiveEntry::Hash(obj) => Some(obj),
            ArchiveEntry::NonHash(_) => None,
        }
    }
}

impl Ord for ArchiveEntry {
    fn cmp(&self, other: &Self) -> Ordering {
        match (self, other) {
            (ArchiveEntry::Hash(a), ArchiveEntry::Hash(b)) => a.cmp(b),
            (ArchiveEntry::NonHash(a), ArchiveEntry::NonHash(b)) => a.cmp(b),
            (ArchiveEntry::Hash(a), ArchiveEntry::NonHash(b))
            | (ArchiveEntry::NonHash(b), ArchiveEntry::Hash(a)) => a.path.cmp(&b.absolute_path()),
        }
    }
}

impl PartialOrd for ArchiveEntry {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

/// Advise OS about file usage
#[cfg(unix)]
fn fadvise(
    file: &fs::File,
    advice: fcntl::PosixFadviseAdvice,
    start: i64,
    end: i64,
) -> io::Result<()> {
    let fd = file.as_fd().as_raw_fd();
    match fcntl::posix_fadvise(fd, start, end, advice) {
        Ok(()) => Ok(()),
        Err(e) => {
            let e = io::Error::from(e);
            Err(io::Error::new(e.kind(), format!("fadvise failed: {e}")))
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn object_debug() {
        let obj = Object::new(PathBuf::from("path/to/archive/bucket/fbed703cb018e3651391d69c3605e1e16d193a011b7b99e83e5557f0666a0392"));
        assert_eq!(
            format!("{obj:?}"),
            r#""bucket/fbed703cb018e3651391d69c3605e1e16d193a011b7b99e83e5557f0666a0392""#,
        );
    }

    #[test]
    fn non_hash_object_debug() {
        let obj = NonHashObject {
            bucket_dir: Rc::new(PathBuf::from("/path/to/archive/bucket")),
            relative_path: PathBuf::from("path/in/bucket"),
        };
        assert_eq!(format!("{obj:?}"), r#""bucket/path/in/bucket""#,);
    }
}

#[cfg(test)]
#[cfg(feature = "nightly")]
mod benchmarks {
    use super::*;
    use crate::test::Bencher;
    use rand::prelude::*;

    #[bench]
    #[cfg(feature = "slow_benches")]
    fn fs_object_lookup_50_000_buckets_and_100_000_objects(b: &mut Bencher) {
        fs_object_lookup_internal(b, 50_000, 100_000);
    }

    #[bench]
    fn fs_object_lookup_50_000_buckets_and_0_objects(b: &mut Bencher) {
        fs_object_lookup_internal(b, 50_000, 0);
    }

    #[bench]
    #[cfg(feature = "slow_benches")]
    fn fs_object_lookup_10_000_buckets_and_1_000_000_objects(b: &mut Bencher) {
        fs_object_lookup_internal(b, 10_000, 1_000_000);
    }

    #[bench]
    fn fs_object_lookup_10_000_buckets_and_30_000_objects(b: &mut Bencher) {
        fs_object_lookup_internal(b, 10_000, 30_000);
    }

    #[bench]
    #[cfg(feature = "slow_benches")]
    fn fs_object_lookup_100_buckets_and_1_000_000_objects(b: &mut Bencher) {
        fs_object_lookup_internal(b, 100, 1_000_000);
    }

    #[bench]
    fn fs_object_lookup_100_buckets_and_1_000_objects(b: &mut Bencher) {
        fs_object_lookup_internal(b, 100, 1_000);
    }

    #[bench]
    fn fs_object_lookup_10_buckets_and_1_000_000_objects(b: &mut Bencher) {
        fs_object_lookup_internal(b, 10, 1_000_000);
    }

    fn fs_object_lookup_internal(b: &mut Bencher, bucket_count: usize, object_count: usize) {
        use crate::hash::to_hex;
        use rand::prelude::*;

        let mut rng = SmallRng::from_os_rng();
        let dir = tempfile::tempdir().unwrap();
        let dir = dir.path();
        let buckets: Vec<_> = (0..=bucket_count).map(|i| format!("bucket{i}")).collect();

        for bucket in &buckets {
            fs::create_dir(dir.join(bucket)).unwrap();
        }

        for _ in 0..=object_count {
            let bucket = buckets.choose(&mut rng).unwrap();
            let mut hash = [0_u8; 32];
            rng.fill_bytes(&mut hash);
            fs::File::create(dir.join(bucket).join(to_hex(&hash))).unwrap();
        }

        // Sync directories to lower likelihood of async FS operations
        // causing jiter in our measurements. Objects are empty, so
        // there should not be much need to sync those.
        fs::File::open(dir).unwrap().sync_all().unwrap();
        for bucket in &buckets {
            fs::File::open(dir.join(bucket))
                .unwrap()
                .sync_all()
                .unwrap();
        }

        // Just search for a random hash, which is virtually impossible
        // to exist. This, however, means that this measurements do not
        // fully correspond to what we do on production. There an object
        // is likely to be existing exactly once in an arbitrary bucket.
        // Thus, in production we likely have to search about half as
        // many buckets.
        let mut hash = [0_u8; 32];
        rng.fill_bytes(&mut hash);
        let hash = Hash::new(&to_hex(&hash)).unwrap();
        let mut finder = ObjectFinder::new(&dir).unwrap();
        b.iter(|| {
            assert!(!finder.exists_with_hash(&hash).unwrap());
        });
    }

    #[bench]
    fn look_up_among_30_000_objects_in_100_buckets_using_fast_path(b: &mut Bencher) {
        // Because all objects are are in one bucket, fast path can always be used.
        let buckets_used = 1;
        find_objects_which_are_known_to_exist(b, 100, buckets_used, 30_000);
    }

    #[bench]
    fn look_up_among_30_000_objects_in_100_buckets_using_slow_path(b: &mut Bencher) {
        // Because objects are distributed among `buckets_used` in round-robin
        // fashion, fast path can never be used.
        let buckets = 100;
        let buckets_used = buckets;
        find_objects_which_are_known_to_exist(b, buckets, buckets_used, 30_000);
    }

    fn find_objects_which_are_known_to_exist(
        b: &mut Bencher,
        buckets: usize,
        buckets_used: usize,
        object_count: usize,
    ) {
        assert!(buckets >= buckets_used);
        let dir = tempfile::tempdir().unwrap();

        // create buckets
        let object_buckets = || (0..buckets).map(|i| format!("bucket-{i}"));
        for bucket in object_buckets() {
            fs::create_dir(dir.path().join(bucket)).unwrap();
        }

        // Create objects
        let object_hashes = || (0..object_count).map(|i| format!("{i:064x}"));
        let mut buckets_shuffled: Vec<_> = object_buckets().collect();
        // Order in which buckets are searched depends on the order
        // in which buckets are return by the file system. To avoid
        // bias, use a random set of buckets.
        //
        // Order heavily depends on the file system. BTRFS, for instance,
        // returns directory entries, usually, in the order in which they
        // were created. So, use shuffled list of buckets here but not
        // during creation.
        buckets_shuffled.shuffle(&mut rand::rng());
        let mut endless_bucket_iter = buckets_shuffled.iter().take(buckets_used).cycle();
        for hash in object_hashes() {
            let bucket = endless_bucket_iter.next().unwrap();
            let object_path: PathBuf = [&dir.path(), &Path::new(&bucket), &Path::new(&hash)]
                .iter()
                .collect();
            fs::File::create(&object_path).unwrap();
        }

        // Find objects
        let mut finder = ObjectFinder::new(&dir).unwrap();
        let hashes: Vec<_> = object_hashes().map(|h| Hash::new(&h).unwrap()).collect();
        let mut endless_hash_iter = hashes.iter().cycle();
        // Look up one object to initialize fast path. So, first
        // measurement isn't off.
        assert!(finder
            .exists_with_hash(endless_hash_iter.next().unwrap())
            .unwrap());
        b.iter(|| {
            let hash = endless_hash_iter.next().unwrap();
            assert!(finder.exists_with_hash(hash).unwrap());
        });
    }
}
