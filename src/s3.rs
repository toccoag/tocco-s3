//! S3 storage interaction

use crate::hash::{is_hash_valid, Hash};
use crate::{Error, Result};
use async_stream::try_stream;
use aws_credential_types::provider::SharedCredentialsProvider;
use aws_credential_types::Credentials;
use aws_sdk_s3 as s3;
use aws_smithy_types::timeout::TimeoutConfig;
use futures::Stream;
use s3::config::retry::RetryConfig;
use s3::config::{Region, SharedAsyncSleep, StalledStreamProtectionConfig};
use serde::Deserialize;
use std::cell::OnceCell;
use std::collections::HashMap;
use std::fmt;
use std::path::{self, Component, Path, PathBuf};
use std::result::Result as StdResult;
use std::time::Duration;
use std::time::SystemTime;

/// Create an S3 Client
#[allow(clippy::missing_panics_doc)]
#[must_use]
pub fn client(endpoint: &str, access_key_id: &str, secret_access_key: &str) -> s3::Client {
    let credentials = Credentials::new(access_key_id, secret_access_key, None, None, "main");
    let credentials = SharedCredentialsProvider::new(credentials);
    let builder = aws_sdk_s3::config::Builder::new()
        .app_name(aws_types::app_name::AppName::new("Tocco-S3").expect("invalid app name"))
        .credentials_provider(credentials)
        .endpoint_url(endpoint.to_owned())
        .retry_config(RetryConfig::adaptive())
        .sleep_impl(SharedAsyncSleep::new(
            aws_smithy_async::rt::sleep::TokioSleep::new(),
        ))
        .stalled_stream_protection(
            StalledStreamProtectionConfig::enabled()
                .grace_period(Duration::from_secs(300))
                .build(),
        )
        .timeout_config(
            TimeoutConfig::builder()
                // This timeouts need to be high because of the
                // async nature of this code.
                .read_timeout(Duration::from_secs(300))
                .connect_timeout(Duration::from_secs(300))
                .build(),
        );
    let builder = builder
        .region(Some(Region::new("rma")))
        .force_path_style(true);
    let config = builder.build();
    s3::Client::from_conf(config)
}

/// An S3 access key
///
/// Obtain via [`crate::fs::Archive::s3_keys()`] or
/// [`crate::cloudscale::users()`].
#[derive(Clone, Deserialize)]
pub struct Key {
    /// Access key ID
    pub access_key: String,

    /// Secret access key
    pub secret_key: String,
}

impl fmt::Debug for Key {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.debug_struct("Key")
            .field("access_key", &self.access_key)
            .field("secret_key", &"************")
            .finish()
    }
}

/// Per-bucket cache for S3 Clients
pub struct ClientCache {
    clients: HashMap<String, Client>,
    endpoint: String,
}

pub struct Client {
    client: OnceCell<s3::Client>,
    keys: Key,
}

impl ClientCache {
    /// Create cache for given buckets.
    ///
    /// `s3_keys` contains the access and secrets keys for accessing
    /// a bucket. The `HashMaps` key is the bucket name.
    #[must_use]
    pub fn new(endpoint: &str, s3_keys: HashMap<String, Key>) -> Self {
        let cache = s3_keys
            .into_iter()
            .map(|(user_name, key)| {
                let value = Client {
                    client: OnceCell::new(),
                    keys: key,
                };
                (user_name, value)
            })
            .collect();
        ClientCache {
            clients: cache,
            endpoint: endpoint.to_string(),
        }
    }

    /// Get S3 client for given `bucket`.
    #[must_use]
    pub fn client(&self, bucket: &str) -> Option<s3::Client> {
        let cache_item = self.clients.get(bucket)?;
        let client = cache_item.client.get_or_init(|| {
            client(
                &self.endpoint,
                &cache_item.keys.access_key,
                &cache_item.keys.secret_key,
            )
        });
        // Reference counted internally. So, .clone()ing is cheap.
        Some(client.clone())
    }
}

/// An object on S3
pub struct Object {
    bucket: String,
    key: String,
    last_modified: SystemTime,
    size: u64,
}

impl Object {
    /// Bucket to which object belongs.
    #[must_use]
    pub fn bucket(&self) -> &str {
        &self.bucket
    }

    /// Path within a bucket (also called key).
    #[must_use]
    pub fn path(&self) -> &str {
        &self.key
    }

    /// Time object was last modified.
    #[must_use]
    pub fn last_modified(&self) -> SystemTime {
        self.last_modified
    }

    /// Object size in bytes.
    #[must_use]
    pub fn size(&self) -> u64 {
        self.size
    }

    /// Hash of object content.
    ///
    /// This is based on the objects [`Self::path()`] and
    /// is only available when the path is a hash. `Err(())`
    /// is returned if it isn't.
    pub fn hash(&self) -> StdResult<Hash, ()> {
        Hash::new(&self.key)
    }

    /// Test if [`Self::path()`] is a hash.
    #[must_use]
    pub fn is_hash(&self) -> bool {
        is_hash_valid(&self.key)
    }
}

impl fmt::Debug for Object {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("s3::Object")
            .field("bucket", &self.bucket)
            .field("key", &self.key)
            .field("last_modified", &self.last_modified)
            .field("size", &self.size)
            .finish()
    }
}

impl fmt::Display for Object {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "s3://{}/{}", self.bucket, self.key)
    }
}

/// Obtain a [`Stream`] listing all objects in bucket.
///
/// Objects are yielded sorted by byte-order of [`Object::path()`].
#[allow(clippy::missing_panics_doc)]
pub fn list_objects<'a>(
    client: &'a s3::Client,
    bucket: &str,
) -> impl Stream<Item = Result<Object>> + 'a {
    let bucket = bucket.to_owned();
    try_stream! {
        let builder_template = client.list_objects_v2().bucket(&bucket);
        let mut next_page = {
            let builder = builder_template.clone();
            Some(tokio::spawn(builder.send()))
        };

        loop {
            let output = if let Some(next_page) = next_page.take() {
                trace!(
                    "Next page was {} when we needed it.",
                    if next_page.is_finished() {
                        "ready"
                    } else {
                        "NOT ready"
                    }
                );
                next_page.await.expect("worker thread panicked")?
            } else {
                break
            };

            // Immediately start off-thread fetching of next page.
            if let Some(continuation_token) = output.next_continuation_token {
                let builder = builder_template.clone();
                next_page = Some(tokio::spawn(builder.continuation_token(continuation_token).send()));
            }

            if let Some(contents) = output.contents {
                if let Some(obj) = contents.first() {
                    trace!(
                        "First object of {} in page is {:?}",
                        contents.len(),
                        obj.key.as_ref().expect("object has no key"),
                    );
                }
                for object in contents {
                    yield Object {
                        bucket: bucket.to_string(),
                        last_modified: (*object
                            .last_modified()
                            .expect("object has no modification timestamp"))
                            .try_into()
                            .expect("modification time cannot be represented as SystemTime"),
                        size: object.size.expect("object has no size").try_into().expect("negative object size"),
                        key: {
                            let key = object.key.expect("object has no key");
                            validate_key_is_valid_path(&key)?;
                            key
                        }
                    };
                }
            }
        }
    }
}

/// Validate S3 key is safe for use as `Path`
///
/// ## Errors
///
/// * Returns an error when a `key` cannot be converted without
///   loss:
///
///   For instance, path `"./a/path"` would become `"a/path"`
///   in the local archive, and `"a//path"` would become
///   `"a/path"`.
///
///   Such objects cannot be restored with the correct key.
///
/// * Returns an error if the resulting path would contain an
///   ".." element or be an absolute path. This would potentially
///   allow overwriting files outside the corresponding bucket.
fn validate_key_is_valid_path(key: &str) -> Result<()> {
    const {
        assert!(path::MAIN_SEPARATOR == '/');
    }

    let key_as_path = Path::new(&key);
    let key_as_normalized_path: PathBuf = key_as_path
        .components()
        // Filter "..", ".", leading "/", etc.
        .filter(|c| matches!(c, Component::Normal(_)))
        .collect();

    // Comparing `OsStr` because `"//"` and `"/"` are considered
    // equal for `Path`.
    if key_as_path.as_os_str() != key_as_normalized_path.as_os_str() {
        Err(Error::Custom(format!(
            "S3 key which cannot be safely converted to Path: {key:?}"
        )))
    } else if key.contains('\0') {
        Err(Error::Custom(format!("NUL character in s3 key: {key:?}")))
    } else {
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use futures::future;
    use std::time::{Duration, SystemTime};
    use tokio::pin;
    use tokio_stream::StreamExt;

    #[tokio::test]
    async fn list_objects() {
        let _ = env_logger::builder().is_test(true).try_init();

        // There is paging after 1000 objects. Test the boundry.
        tokio::join!(
            create_and_list_n_objects(0),
            create_and_list_n_objects(1),
            create_and_list_n_objects(999),
            create_and_list_n_objects(1000),
            create_and_list_n_objects(1001),
        );
    }

    async fn create_and_list_n_objects(object_count: usize) {
        let (client, bucket) = test_utils::create_bucket().await;
        let keys_created: Vec<_> = (0..object_count)
            .map(|i| {
                let client = &client;
                let bucket = &bucket;
                async move {
                    let content = format!("content-{i}");
                    test_utils::create_s3_object(client, bucket, &content).await
                }
            })
            .collect();
        let mut keys_created = future::join_all(keys_created).await;
        keys_created.sort();

        let keys_listed: Vec<_> = super::list_objects(&client, &bucket)
            .map(|r| r.unwrap().path().to_owned())
            .collect()
            .await;

        assert_eq!(keys_created, keys_listed);
    }

    #[tokio::test]
    async fn last_modified_timestamp() {
        let (client, bucket) = test_utils::create_bucket().await;
        let _ = test_utils::create_s3_object(&client, &bucket, "<arbitrary content>").await;
        let lister = super::list_objects(&client, &bucket);
        pin!(lister);
        let only_object = lister.try_next().await.unwrap().unwrap();
        assert!(lister.try_next().await.unwrap().is_none());

        let now = SystemTime::now();
        assert!(only_object.last_modified() < now + Duration::from_secs(10));
        assert!(only_object.last_modified() > now - Duration::from_secs(10));
    }

    #[tokio::test]
    async fn keys_which_are_not_valid_and_normalized_paths() {
        let questionable_keys = [
            // current directory
            ".",
            "./path",
            "a/./path",
            "a/path/.",
            // parent directory
            "..",
            "../../../../../../../../../path",
            "path/..",
            "a/../path",
            // empty elements
            "a//path",
            "a/path//",
            // absolute
            "/path",
            // absolute, alternate namespace
            "//path",
            // directory indicator
            "path/",
            // NUL character
            "\0",
            "path\0",
            "\0path",
        ];

        for key in questionable_keys {
            assert!(super::validate_key_is_valid_path(key).is_err());
        }
    }

    #[tokio::test]
    async fn keys_which_are_valid_and_normalized_paths() {
        let good_keys = [
            "6e2586b9f63569c644eaebaaf3254eabc877f4e33bdb65b78d6a67f5480c36a0",
            "a/valid/path.ext",
            "a\\path",
            "å/p∀th",
        ];

        for key in good_keys {
            assert!(super::validate_key_is_valid_path(key).is_ok());
        }
    }
}
