use async_stream::try_stream;
use aws_sdk_s3 as s3;
use clap::Args;
use fallible_iterator::{FallibleIterator, IteratorExt};
use futures::Stream;
use log::{debug, error, info, trace};
use s3::primitives::ByteStream;
use std::cmp;
use std::path::{Path, PathBuf};
use std::sync::Arc;
use tocco_s3::fs::object::ArchiveEntry;
use tocco_s3::fs::Archive;
use tocco_s3::hash::Hash;
use tocco_s3::s3::ClientCache;
use tocco_s3::Result;
use tokio::task;
use tokio_stream::StreamExt;

#[derive(Args)]
pub(crate) struct Restore {
    /// Directory where backup archive is located
    #[arg()]
    directory: PathBuf,

    /// Restore mode
    ///
    /// Restore distinguishes between two object types:
    ///
    /// hash:     A hash object is one whose key is a valid, hex-encoded
    ///           SHA2 hash. This is what Nice uses. This hash is the hash
    ///           of the content. Thus, an objects content cannot change;
    ///           the object needs to be recreated with a different key
    ///           instead..
    ///
    /// non-hash: All other objects. As of writing, Nice does not use such
    ///           objects and they are not expected to be part of any
    ///           backups.
    ///
    /// Restore modes:
    ///
    /// hash-only:      Limits restore to hash objects. Objects that already
    ///                 exist on S3 are ignored. Because the key on S3 is
    ///                 the hash over the content, the content can never
    ///                 change.
    ///
    /// all-if-missing: Restores all objects, hash and non-hash objects. Objects
    ///                 that already exist on S3 are ignored.
    #[arg(value_enum, verbatim_doc_comment)]
    mode: RestoreMode,

    /// Buckets
    ///
    /// Restore given buckets
    #[arg(conflicts_with = "all", required_unless_present = "all")]
    buckets: Option<Vec<String>>,

    /// Restore all buckets
    #[arg(long, short)]
    all: bool,

    /// Max. concurrent uploads
    #[arg(long, short, value_parser(clap::value_parser!(u8).range(1..=250)), default_value_t=25)]
    concurrent_uploads: u8,

    /// Show what would be done without actually doing it.
    ///
    /// Prints all objects that would be uploaded.
    #[arg(long)]
    dry_run: bool,
}

#[derive(Clone, Copy, clap::ValueEnum)]
enum RestoreMode {
    /// Restore hash objects only.
    HashOnly,

    /// Restore hash and non-hash objects if missing
    AllIfMissing,
}

async fn restore(
    archive: &Arc<Archive>,
    buckets: &[String],
    client_cache: &ClientCache,
    args: &Restore,
) -> Result<()> {
    for bucket in buckets {
        info!("Processing bucket {bucket:?}");
        let client = client_cache.client(bucket).expect("missing S3 key");
        restore_bucket(archive, bucket, &client, args).await?;
    }
    Ok(())
}

async fn restore_bucket(
    archive: &Arc<Archive>,
    bucket: &str,
    client: &s3::Client,
    args: &Restore,
) -> Result<()> {
    // Sorting objects into two `Vec`s for hash and non-hash objects to improve
    // memory density. This also takes the object apart and only stores its hash
    // and relative path in `hash_objects` and `non_hash_objects`, respectively.
    // Also to save memory.
    let mut hash_objects = Vec::new();
    let mut non_hash_objects = Vec::new();
    for object in archive.objects_for_buckets(&[bucket])?.iterator() {
        match object? {
            ArchiveEntry::Hash(obj) => {
                debug_assert_eq!(bucket, obj.bucket());
                hash_objects.push(obj.hash());
            }
            ArchiveEntry::NonHash(obj) => match args.mode {
                RestoreMode::HashOnly => (),
                RestoreMode::AllIfMissing => non_hash_objects.push(obj.into_relative_path()),
            },
        }
    }
    hash_objects.sort_unstable();
    non_hash_objects.sort_unstable();

    let to_upload = objects_to_upload(client, bucket, hash_objects, non_hash_objects);
    tokio::pin!(to_upload);
    let mut join_set = task::JoinSet::new();

    let schedule_next = |join_set: &mut task::JoinSet<_>, object| match object {
        Ok(ObjectToUpload::HashObject(hash)) => {
            let client = client.clone();
            let object = archive.get_object_unchecked(bucket, &hash);
            let dry_run = args.dry_run;
            join_set.spawn(async move { upload_hash_object(&client, &object, dry_run).await });
            Ok(())
        }
        Ok(ObjectToUpload::NonHashObject(key)) => {
            let client = client.clone();
            let bucket = bucket.to_owned();
            let dry_run = args.dry_run;
            let key = key.clone();
            let archive_path = archive.path().to_owned();
            join_set.spawn(async move {
                upload_non_hash_object(&client, &archive_path, &bucket, &key, dry_run).await
            });
            Ok(())
        }
        Err(e) => Err(e),
    };

    // Schedule at most `args.concurrent_uploads` jobs.
    while let Some(object) = to_upload.next().await {
        schedule_next(&mut join_set, object)?;
        if join_set.len() >= usize::from(args.concurrent_uploads) {
            break;
        }
    }

    // Then schedule a new job per job completed.
    while let Some(result) = join_set.join_next().await {
        match result {
            Ok(Ok(())) => (),
            Ok(Err(e)) => return Err(e),
            Err(e) => panic!("worker panicked: {e}"),
        }
        if let Some(object) = to_upload.next().await {
            schedule_next(&mut join_set, object)?;
        }
    }
    Ok(())
}

#[derive(Debug)]
enum ObjectToUpload {
    HashObject(Hash),
    NonHashObject(PathBuf),
}

fn objects_to_upload<'a>(
    client: &'a s3::Client,
    bucket: &'a str,
    hash_objects: Vec<Hash>,
    non_hash_objects: Vec<PathBuf>,
) -> impl Stream<Item = Result<ObjectToUpload>> + 'a {
    debug_assert!(hash_objects.is_sorted());
    debug_assert!(non_hash_objects.is_sorted());

    try_stream! {
        let s3_objects = tocco_s3::s3::list_objects(client, bucket);
        tokio::pin!(s3_objects);
        let mut hash_objects = hash_objects.into_iter();
        let mut non_hash_objects = non_hash_objects.into_iter();

        let mut next_s3_object = match s3_objects.next().await {
            Some(obj) => Some(obj?),
            None => None,
        };
        let mut next_hash_object = hash_objects.next();
        let mut next_non_hash_object = non_hash_objects.next();
        loop {
            match (&next_s3_object, &next_hash_object, &next_non_hash_object) {
                // Done
                (_, None, None) => {
                    trace!("No local objects for upload remain.");
                    break;
                }

                // Already exists, no upload needed
                (Some(s3_obj), Some(hash_obj), _) if s3_obj.hash().as_ref() == Ok(hash_obj) => {
                    trace!("{s3_obj} already exists on S3, skipping ...");
                    next_s3_object = match s3_objects.next().await {
                        Some(obj) => Some(obj?),
                        None => None,
                    };
                    next_hash_object = hash_objects.next();
                }
                (Some(s3_obj), _, Some(non_hash_obj)) if Path::new(s3_obj.path()) == non_hash_obj => {
                    trace!("{s3_obj} already exists on S3, skipping ...");
                    next_s3_object = match s3_objects.next().await {
                        Some(obj) => Some(obj?),
                        None => None,
                    };
                    next_non_hash_object = non_hash_objects.next();
                }

                // Object on S3 missing locally
                (Some(s3), Some(hash), Some(non_hash))
                    if Path::new(s3.path())
                        < cmp::min(Path::new(&hash.hex_digest()), non_hash.as_path()) =>
                {
                    trace!("{s3} missing locally.");
                    next_s3_object = match s3_objects.next().await {
                        Some(obj) => Some(obj?),
                        None => None,
                    };
                }
                (Some(s3), Some(hash), None)
                    if Path::new(s3.path()) < Path::new(&hash.hex_digest()) =>
                {
                    trace!("{s3} missing locally.");
                    next_s3_object = match s3_objects.next().await {
                        Some(obj) => Some(obj?),
                        None => None,
                    };
                }
                (Some(s3), None, Some(non_hash)) if Path::new(s3.path()) < non_hash.as_path() => {
                    trace!("{s3} missing locally.");
                    next_s3_object = match s3_objects.next().await {
                        Some(obj) => Some(obj?),
                        None => None,
                    };
                }

                // Upload
                (_, Some(hash), None) => {
                    yield ObjectToUpload::HashObject(hash.to_owned());
                    next_hash_object = hash_objects.next();
                }
                (_, None, Some(non_hash)) => {
                    yield ObjectToUpload::NonHashObject(non_hash.to_owned());
                    next_non_hash_object = non_hash_objects.next();
                }
                (_, Some(hash), Some(non_hash))
                    if Path::new(&hash.hex_digest()) > non_hash.as_path() =>
                {
                    yield ObjectToUpload::NonHashObject(non_hash.to_owned());
                    next_non_hash_object = non_hash_objects.next();
                }
                (_, Some(hash), Some(_non_hash)) => {
                    yield ObjectToUpload::HashObject(hash.to_owned());
                    next_hash_object = hash_objects.next();
                }
            }
        }
    }
}

async fn upload_hash_object(
    client: &aws_sdk_s3::Client,
    object: &tocco_s3::fs::Object,
    dry_run: bool,
) -> Result<()> {
    debug!("Uploading hash object: {object:?}.");
    let key = object.hash().hex_digest();

    #[cfg(test)]
    assert!(!test_utils::s3_object_exists(object.bucket(), &key).await);

    if dry_run {
        println!("dry-run: would upload {}/{key}", object.bucket());
        return Ok(());
    }

    // TODO Can't use reader which verifies checksum currently.
    //
    // See https://github.com/awslabs/aws-sdk-rust/issues/541
    // let reader = object.read()?;

    // TODO use multipart upload.
    //
    // `put_object()` is limited to an object size of 5 GiB and
    // multi-part upload should be used here. The SDK does not
    // currently offer a high-level implementation for it. Let's
    // wait and hope it gets added soon.
    //
    // https://github.com/awslabs/aws-sdk-rust/issues/494#issuecomment-1075712702
    let reader = object.s3_sdk_read().await?;
    client
        .put_object()
        .bucket(object.bucket())
        .key(key)
        // Unsupported and ignored by Ceph RADOS Gateway used by Cloudscale.
        // Ticket for implementing SHA256 support: https://tracker.ceph.com/issues/63951
        .checksum_sha256(object.hash().base64_digest())
        .body(reader)
        .send()
        .await?;
    Ok(())
}

async fn upload_non_hash_object(
    client: &aws_sdk_s3::Client,
    archive_path: &Path,
    bucket: &str,
    object_path: &Path,
    dry_run: bool,
) -> Result<()> {
    debug!("Uploading non-hash object in bucket {bucket:?} with key {object_path:?}.");

    #[cfg(test)]
    assert!(
        !test_utils::s3_object_exists(bucket, object_path.to_str().expect("non-UTF8 object key"))
            .await
    );

    if dry_run {
        println!("dry-run: would upload {bucket}/{}", object_path.display());
        return Ok(());
    }

    let path: PathBuf = [archive_path, Path::new(bucket), object_path]
        .iter()
        .collect();
    // See comments in `upload_hash_object()`.
    let reader = ByteStream::from_path(path).await?;
    client
        .put_object()
        .bucket(bucket)
        .key(object_path.to_str().expect("non-UTF8 object key"))
        .body(reader)
        .send()
        .await?;
    Ok(())
}

pub(crate) async fn main(args: &Restore) -> u8 {
    let archive = match Archive::open(&args.directory) {
        Ok(archive) => Arc::new(archive),
        Err(e) => {
            error!("Failed to open archive: {e}");
            return 1;
        }
    };

    let s3_keys = match archive.s3_keys().await {
        Ok(keys) => keys,
        Err(e) => {
            error!("Failed to fetch S3 keys: {e}");
            return 1;
        }
    };

    let all_buckets = match archive.buckets() {
        Ok(buckets) => buckets,
        Err(e) => {
            error!("Failed to list buckets: {e}");
            return 1;
        }
    };

    let buckets = if let Some(selected_buckets) = &args.buckets {
        debug_assert!(!args.all);
        selected_buckets
            .iter()
            .map(|b| Ok(b))
            .transpose_into_fallible()
            .map(|bucket| {
                if all_buckets.contains(bucket) {
                    Ok(bucket)
                } else {
                    error!("Bucket {bucket:?} not found in archive.");
                    Err(())
                }
            })
            .cloned()
            .collect()
    } else {
        debug_assert!(args.all);
        Ok(all_buckets)
    };
    let Ok(buckets) = buckets else {
        return 2;
    };
    if buckets.is_empty() {
        error!("No bucket to restore.");
        return 1;
    }

    for bucket in &buckets {
        if !s3_keys.contains_key(&bucket[..]) {
            error!("Missing S3 key for bucket {bucket:?}.");
            return 2;
        }
    }

    let client_cache = ClientCache::new(archive.s3_endpoint(), s3_keys);
    if let Err(e) = restore(&archive, &buckets, &client_cache, args).await {
        error!("Restore failed: {e}");
        return 1;
    };

    0
}

#[cfg(test)]
mod tests {
    use super::*;

    use rand::distr::{Alphanumeric, SampleString};
    use rand::rngs::SmallRng;
    use rand::{random, Rng, RngCore, SeedableRng};
    use std::fs;
    use tokio::fs::File;
    use tokio::io::AsyncWriteExt as _;

    struct TestObject {
        bucket: String,
        key: String,
        content: String,
    }

    impl TestObject {
        fn is_hash_object(&self) -> bool {
            self.hash().is_some()
        }

        fn hash(&self) -> Option<Hash> {
            Hash::new(&self.key).ok()
        }
    }

    fn test_data(bucket_name1: &str, bucket_name2: &str) -> Vec<TestObject> {
        vec![
            TestObject {
                bucket: bucket_name1.to_string(),
                key: "51bbfa74f8660493f40fd72068f63af436ee13c283ca84c373d9690ff2f1f83c".to_string(),
                content: "data-1".to_string(),
            },
            TestObject {
                bucket: bucket_name1.to_string(),
                key: "00c2022f72beeabc82c8f02099df7abebe43292bac3f44bf63f5827a8c50255a".to_string(),
                content: "data-2".to_string(),
            },
            TestObject {
                bucket: bucket_name1.to_string(),
                key: "824c73f33ea8acde000d446a45f47b3c72dd645800f7a255de04040b33f35a53".to_string(),
                content: "data-3".to_string(),
            },
            TestObject {
                bucket: bucket_name1.to_string(),
                key: "f6a63b91aba4aac54cffbebdc6156d34abadbe990707bf2eca1fd14ffffd2e14".to_string(),
                content: "data-4".to_string(),
            },
            TestObject {
                bucket: bucket_name1.to_string(),
                key: "33e268d1b10ee19c4495ad3a5e02428c67465e5f4b150539c465398b6fa48920".to_string(),
                content: "data-5".to_string(),
            },
            TestObject {
                bucket: bucket_name1.to_string(),
                key: "an-unexpected-key".to_string(),
                content: "data-7".to_string(),
            },
            TestObject {
                bucket: bucket_name2.to_string(),
                key: "51bbfa74f8660493f40fd72068f63af436ee13c283ca84c373d9690ff2f1f83c".to_string(),
                content: "data-1".to_string(),
            },
            TestObject {
                bucket: bucket_name2.to_string(),
                key: "e7eded20736cae697676b000193f685046e4a7b38de954b72917cee7e516526a".to_string(),
                content: "data-7".to_string(),
            },
            TestObject {
                bucket: bucket_name2.to_string(),
                key: "an-unexpected-key-two".to_string(),
                content: "data-8".to_string(),
            },
        ]
    }

    fn create_test_data(bucket_name1: &str, bucket_name2: &str) -> (tempfile::TempDir, Archive) {
        let temp_dir = tempfile::tempdir().unwrap();
        let archive_dir = temp_dir.path().join("archive");
        let archive = Archive::init(&archive_dir).unwrap();
        let archive = update_archive_config(archive, bucket_name1, bucket_name2);

        for obj in test_data(bucket_name1, bucket_name2) {
            test_utils::create_object_on_disk(&archive, &obj.bucket, &obj.key, &obj.content);
        }

        (temp_dir, archive)
    }

    fn update_archive_config(archive: Archive, bucket_name1: &str, bucket_name2: &str) -> Archive {
        let endpoint = test_utils::s3_endpoint();
        let (key_id, secret) = test_utils::s3_key();
        let config = format!(
            r#"
            [archive]

            [s3]
            endpoint = "{endpoint}"

            [removal]
            retention_time_days = 180

            [s3.keys.{bucket_name1}]
            access_key = "{key_id}"
            secret_key = "{secret}"

            [s3.keys.{bucket_name2}]
            access_key = "{key_id}"
            secret_key = "{secret}"

            [[db_servers]]
            host_name = "localhost"
        "#
        );
        fs::write(archive.path().join("config.toml"), config).unwrap();
        Archive::open(&archive.path()).unwrap()
    }

    #[tokio::test]
    async fn full_restore() {
        let _ = env_logger::builder().is_test(true).try_init();
        let bucket_name1 = "full-restore-one";
        let bucket_name2 = "full-restore-two";
        let (_temp, archive) = create_test_data(bucket_name1, bucket_name2);

        test_utils::create_bucket_with_name(bucket_name1).await;
        test_utils::create_bucket_with_name(bucket_name2).await;
        let args = Restore {
            all: true,
            buckets: None,
            concurrent_uploads: 25,
            directory: archive.path().to_owned(),
            dry_run: false,
            mode: RestoreMode::AllIfMissing,
        };
        let ret = main(&args).await;
        assert_eq!(ret, 0);
        for obj in test_data(bucket_name1, bucket_name2) {
            test_utils::assert_s3_object_content(&obj.bucket, &obj.key, &obj.content).await;
        }
    }

    #[tokio::test]
    async fn restore_non_hash_objects_to_single_bucket() {
        async fn run(args: &Restore) {
            let ret = main(args).await;
            assert_eq!(ret, 0);
            for obj in test_data(BUCKET_NAME1, BUCKET_NAME2) {
                if obj.bucket == BUCKET_NAME1 && obj.is_hash_object() {
                    test_utils::assert_s3_object_content(&obj.bucket, &obj.key, &obj.content).await;
                } else {
                    assert!(!test_utils::s3_object_exists(&obj.bucket, &obj.key).await);
                }
            }
        }

        const BUCKET_NAME1: &str = "single-restore-one";
        const BUCKET_NAME2: &str = "single-restore-two";

        let _ = env_logger::builder().is_test(true).try_init();
        let (_temp, archive) = create_test_data(BUCKET_NAME1, BUCKET_NAME2);

        test_utils::create_bucket_with_name(BUCKET_NAME1).await;
        let args = Restore {
            all: false,
            buckets: Some(vec![BUCKET_NAME1.to_string()]),
            concurrent_uploads: 25,
            directory: archive.path().to_owned(),
            dry_run: false,
            mode: RestoreMode::HashOnly,
        };

        // Initial run
        run(&args).await;

        // Change / corrupt files in archive
        for obj in test_data(BUCKET_NAME1, BUCKET_NAME2) {
            let path: PathBuf = [archive.path(), Path::new(&obj.bucket), Path::new(&obj.key)]
                .iter()
                .collect();
            assert!(path.try_exists().unwrap());
            let mut file = File::create(path).await.unwrap();
            file.write_all(b"corrupted").await.unwrap();
        }

        // Re-run to ensure files are not updated when they already exist.
        run(&args).await;
    }

    #[tokio::test]
    async fn dry_run() {
        let _ = env_logger::builder().is_test(true).try_init();
        let bucket_name1 = "dry-run-one";
        let bucket_name2 = "dry-run-two";
        let (_temp, archive) = create_test_data(bucket_name1, bucket_name2);

        test_utils::create_bucket_with_name(bucket_name1).await;
        test_utils::create_bucket_with_name(bucket_name2).await;
        let args = Restore {
            all: true,
            buckets: None,
            concurrent_uploads: 25,
            directory: archive.path().to_owned(),
            dry_run: true,
            mode: RestoreMode::AllIfMissing,
        };
        let ret = main(&args).await;
        assert_eq!(ret, 0);
        for obj in test_data(bucket_name1, bucket_name2) {
            assert!(!test_utils::s3_object_exists(&obj.bucket, &obj.key).await);
        }
    }

    #[tokio::test]
    async fn no_such_bucket() {
        let _ = env_logger::builder().is_test(true).try_init();
        let bucket1 = "unused1";
        let bucket2 = "unused2";
        let (_temp, archive) = create_test_data(bucket1, bucket2);

        let args = Restore {
            all: false,
            buckets: Some(vec![
                bucket1.to_string(),
                "no-such-bucket".to_string(),
                bucket2.to_string(),
            ]),
            concurrent_uploads: 25,
            directory: archive.path().to_owned(),
            dry_run: false,
            mode: RestoreMode::HashOnly,
        };
        let ret = main(&args).await;
        assert_eq!(ret, 2);
    }

    #[tokio::test]
    async fn upload_hash_object() {
        let _ = env_logger::builder().is_test(true).try_init();
        let client = test_utils::s3_client();
        let temp_dir = tempfile::tempdir().unwrap();
        let bucket = "upload-hash-object";
        let hash =
            Hash::new("2cfa0dec88b75f6db9e6c210fb274948099f0ea5b645750fbf55e428c4190aad").unwrap();
        let content = "arbitrary content";
        let archive = Archive::init(&temp_dir.path().join("archive")).unwrap();
        let object = archive
            .create_object(bucket, &hash, &mut content.as_bytes())
            .unwrap();
        test_utils::create_bucket_with_name(bucket).await;

        // Dry-run
        {
            let dry_run = true;
            super::upload_hash_object(&client, &object, dry_run)
                .await
                .unwrap();
            assert!(!test_utils::s3_object_exists(bucket, &hash.hex_digest()).await);
        }

        // Wet-run
        {
            let dry_run = false;
            super::upload_hash_object(&client, &object, dry_run)
                .await
                .unwrap();
            test_utils::assert_s3_object_content(bucket, &hash.hex_digest(), content).await;
        }
    }

    #[tokio::test]
    async fn upload_non_hash_object() {
        let _ = env_logger::builder().is_test(true).try_init();
        let client = test_utils::s3_client();
        let temp_dir = tempfile::tempdir().unwrap();
        let archive = Archive::init(&temp_dir.path().join("archive")).unwrap();
        let bucket = "upload-non-hash-object";
        let key = "a/complex/key";
        let content = "some content";
        test_utils::create_bucket_with_name(bucket).await;
        test_utils::create_object_on_disk(&archive, bucket, key, content);

        // Dry-run
        {
            let dry_run = true;
            super::upload_non_hash_object(&client, archive.path(), bucket, Path::new(key), dry_run)
                .await
                .unwrap();
            assert!(!test_utils::s3_object_exists(bucket, key).await);
        }

        // Wet-run
        {
            let dry_run = false;
            super::upload_non_hash_object(&client, archive.path(), bucket, Path::new(key), dry_run)
                .await
                .unwrap();
            test_utils::assert_s3_object_content(bucket, key, content).await;
        }
    }

    #[tokio::test]
    async fn objects_to_upload() {
        let _ = env_logger::builder().is_test(true).try_init();
        let rng_seed: u64 = random();
        let mut rng = SmallRng::seed_from_u64(rng_seed);
        println!("Master seed: {rng_seed}.");
        for _ in 0..25 {
            objects_to_upload_helper(rng.random()).await;
        }
    }

    async fn objects_to_upload_helper(rng_seed: u64) {
        let bucket = format!("objects-to-upload-{rng_seed}");
        let mut rng = SmallRng::seed_from_u64(rng_seed);
        test_utils::create_bucket_with_name(&bucket).await;
        let client = test_utils::s3_client();

        // Objects available locally for upload. Some may have
        // been uploaded already.
        let mut local_objects = Vec::new();

        for _ in 0..15 {
            // Maybe upload an object to S3 without adding it to `local_object`
            // to simulate a pre-existing object on S3.
            if rng.random_bool(0.3) {
                let key = random_key(&mut rng);
                trace!("Simulating pre-existing S3 object: {}", key);
                test_utils::create_empty_object_on_s3(&bucket, &key).await;
            }

            // Generate objects with random keys for upload.
            let new_objects = random_objects(&mut rng);
            trace!("Objects added {new_objects:#?}.");

            // Scan S3 to see which objects still need to be uploaded. Objects in
            // `new_objects` should be missing.
            local_objects.extend(new_objects.iter().cloned());
            local_objects.sort_unstable();
            let (hash_objects, non_hash_objects) = split_objects(&local_objects);
            let mut to_upload: Vec<_> =
                super::objects_to_upload(&client, &bucket, hash_objects, non_hash_objects)
                    .map(|obj| match obj.unwrap() {
                        ObjectToUpload::HashObject(hash) => hash.hex_digest(),
                        ObjectToUpload::NonHashObject(path) => path.display().to_string(),
                    })
                    .collect()
                    .await;
            to_upload.sort_unstable();
            assert_eq!(new_objects, to_upload);

            // Simulate upload to S3.
            for key in new_objects {
                test_utils::create_empty_object_on_s3(&bucket, &key).await;
            }

            // Maybe remove a random object from `local_objects`. `objects_to_upload()`
            // should ignore objects missing locally.
            if !local_objects.is_empty() && rng.random_bool(0.2) {
                let random_idx = rng.random_range(0..local_objects.len());
                let removed = local_objects.swap_remove(random_idx);
                trace!("Simulating removal of local object: {removed}");
            }
        }
    }

    /// Split `objects` into `(hash_objects, non_hash_objects)`
    fn split_objects(objects: &[String]) -> (Vec<Hash>, Vec<PathBuf>) {
        let hash_objects: Vec<_> = objects
            .iter()
            .filter_map(|key| Hash::new(key).ok())
            .collect();
        let non_hash_objects: Vec<_> = objects
            .iter()
            .filter(|key| Hash::new(key).is_err())
            .map(|o| Path::new(o).to_path_buf())
            .collect();
        (hash_objects, non_hash_objects)
    }

    /// Create 0..=3 random test objects
    fn random_objects(mut rng: &mut dyn RngCore) -> Vec<String> {
        let len = rng.random_range(0..=3);
        let mut objects: Vec<_> = (0..len).map(|_| random_key(&mut rng)).collect();
        objects.sort_unstable();
        objects
    }

    /// Random S3 key
    fn random_key(mut rng: &mut dyn RngCore) -> String {
        if rng.random_bool(0.8) {
            // Hash object
            let hash: [u8; 32] = rng.random();
            Hash::from_bytes(&hash).unwrap().hex_digest()
        } else {
            // Non-hash object
            let max_path_elements = rng.random_range(1..=3);
            let element: Vec<_> = (0..=max_path_elements)
                .map(|_| {
                    let len = rng.random_range(5..=12);
                    Alphanumeric.sample_string(&mut rng, len)
                })
                .collect();
            element.join("/")
        }
    }

    #[tokio::test]
    #[ignore = "checksum not verified by Ceph on Cloudscale"] // TODO
    async fn upload_incorrect_checksum() {
        const CORRECT_CONTENT: &str = "some content";
        const INCORRECT_CONTENT: &str = "corrupted content";
        let key = "290f493c44f5d63d06b374d0a5abd292fae38b92cab2fae5efefe1b0e9347f56";
        let (client, bucket) = test_utils::create_bucket().await;
        let (_temp, archive) = create_test_data(&bucket, "unused");

        let upload = |content: &'static str| async {
            test_utils::create_object_on_disk(&archive, &bucket, key, content);
            let object = archive.get_object_unchecked(&bucket, &Hash::new(key).unwrap());
            super::upload_hash_object(&client, &object, false).await
        };

        assert!(upload(INCORRECT_CONTENT).await.is_err());
        assert!(!test_utils::s3_object_exists(&bucket, key).await);

        assert!(upload(CORRECT_CONTENT).await.is_ok());
        assert!(test_utils::s3_object_exists(&bucket, key).await);
    }
}
