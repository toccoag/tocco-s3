//! Verify integrity of objects that have been backed up to disk.
//!
//! Used to check integrity of S3 backups created for objects in
//! Tocco.
//!
//! Objects use a SHA2 hash as object key. This hash is verified
//! against the objects content. See also [`tocco_s3::fs`].
//!
//! ## Example
//!
//! ```text
//! $ ./tocco-s3-integrity-check backup/
//! [2023-01-12T18:37:36Z ERROR tocco_s3_integrity_check] Error while verifying object "tocco-nice-lmb/105442993607ed18dd95b87fb010e018bf981208f1795882214da42864561634": checksum mismatch
//! ```
//!
//! For debugging, `RUST_LOG` can be used to increase verbosity
//!
//! ```
//! $ RUST_LOG=info ./tocco-s3-integrity-check backup/
//! [2023-01-12T18:39:08Z INFO  tocco_s3::fs] ignoring non-object file "./target/release"
//! [2023-01-12T18:39:08Z INFO  tocco_s3::hash] Encountered hash "e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855" but expected "105442993607ed18dd95b87fb010e018bf981208f1795882214da42864561634".
//! [2023-01-12T18:39:08Z ERROR tocco_s3_integrity_check] Error while verifying object "tocco-nice-lmb/105442993607ed18dd95b87fb010e018bf981208f1795882214da42864561634": checksum mismatch
//! [2023-01-12T18:39:08Z INFO  tocco_s3_integrity_check] Successful validation of "tocco-nice-lmb/dc1f1d952c136380d90183e4d1b05fec230605a00bc80b87d984718ca11e3974"
//! ```

use clap::Args;
use fallible_iterator::FallibleIterator;
use log::{error, info};
use std::io;
use std::path::{Path, PathBuf};
use tocco_s3::fs::Archive;
use tocco_s3::Error;

/// Verify integrity of objects that have been backed up to disk.
///
/// DIRECTORY is expected contain files using `{bucket_name}/{object_hash}`
/// as directory / file name.
///
/// Objects use a SHA2 hash as object key. This hash is verified
/// against the objects content. See also [`tocco_s3::fs`].
///
/// On error, a non-zero return value is returned.
#[derive(Args)]
pub(crate) struct CheckContent {
    /// Directory where backups are located
    #[arg()]
    directory: PathBuf,
}

#[derive(Clone, Copy, Debug, Default, PartialEq)]
#[must_use]
struct Status {
    seen: u64,
    failed: u64,
}

/// Run integrity check on all objects
///
/// Returns `(object_count, error_count)`.
fn check_archive_integrity<P>(path: &P) -> Result<Status, Error>
where
    P: AsRef<Path>,
{
    let mut status = Status::default();
    let archive = Archive::open(path)?;
    let mut iter = archive.all_objects()?;
    let mut state_db = archive.state_db()?;

    let commit_size = 100_000;
    let mut object_buffer = Vec::with_capacity(commit_size);
    loop {
        let iter_inner = iter
            .by_ref()
            .filter_map(|entry| Ok(entry.into_hash_object()))
            .take(commit_size);
        for object in iter_inner.iterator() {
            status.seen += 1;
            object_buffer.push(object?);
        }
        if object_buffer.is_empty() {
            break;
        }
        for object in &object_buffer {
            match object.check_integrity() {
                Ok(()) => info!("Successful validation of {object:?}"),
                Err(e) if e.kind() == io::ErrorKind::NotFound => {
                    // This can lead to the object being re-inserted
                    // into the state DB incorrectly. This happens
                    // rarely and the object will be removed again,
                    // when not seen for some time.
                    info!("Object {object:?} vanished meanwhile.")
                }
                Err(e) => {
                    status.failed += 1;
                    error!("Error while verifying object {object:?}: {e}");
                }
            }
        }
        state_db.insert_last_seen_times(object_buffer.iter().map(|obj| obj.hash()))?;
        object_buffer.truncate(0);
    }

    let min_objects = archive.min_objects();
    if status.seen < min_objects {
        error!(
            "Expected to find at least {min_objects} objects but found only {}.",
            status.seen
        );
        Err(Error::Custom("too few objects seen in archive".to_string()))
    } else {
        state_db.update_last_archive_scan_time()?;
        Ok(status)
    }
}

pub(crate) fn main(args: &CheckContent) -> u8 {
    match check_archive_integrity(&args.directory) {
        Ok(status) => {
            println!(
                "Processed {} object(s) and encountered {} error(s).",
                status.seen, status.failed
            );
            match status.failed {
                0 => 0,
                _ => 1,
            }
        }
        Err(e) => {
            println!("Failure while checking object content: {e}");
            1
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use chrono::Duration;
    use std::fs;
    use std::io;
    use std::thread;
    use std::time;
    use tempfile::TempDir;
    use tocco_s3::hash::Hash;
    use tocco_s3::state::ObjectsOlderThanPager;

    #[test]
    fn main() {
        let test_data = [
            (
                "bucket1",
                b"content1",
                "d0b425e00e15a0d36b9b361f02bab63563aed6cb4665083905386c55d5b679fa",
            ),
            (
                "bucket1",
                b"content2",
                "dab741b6289e7dccc1ed42330cae1accc2b755ce8079c2cd5d4b5366c9f769a6",
            ),
            (
                "bucket2",
                b"content3",
                "3edb4af0a0f7c03b911f09f72820d409dd0c9d86d183cac8a35848a8fc30a756",
            ),
        ];
        let (_temp_dir, archive) = create_archive_with_min_objects(0);
        let state_db = archive.state_db().unwrap();
        for (bucket, content, hash) in test_data {
            let mut reader = &content[..];
            archive
                .create_object(bucket, &Hash::new(hash).unwrap(), &mut reader)
                .unwrap();
        }
        let initial_scan_time = state_db.last_archive_scan_time().unwrap();

        // archive integrity good
        assert_eq!(
            check_archive_integrity(&archive.path()).unwrap(),
            Status { seen: 3, failed: 0 }
        );
        let scan_time = state_db.last_archive_scan_time().unwrap();
        assert!(scan_time > initial_scan_time);

        // corrupt object
        {
            let path = archive
                .path()
                .join("bucket1/dab741b6289e7dccc1ed42330cae1accc2b755ce8079c2cd5d4b5366c9f769a6");
            let mut file = fs::File::create(path).unwrap();
            let mut reader = &b"corrupted content"[..];
            io::copy(&mut reader, &mut file).unwrap();
        }
        assert_eq!(scan_time, state_db.last_archive_scan_time().unwrap());

        // archive integrity bad
        assert_eq!(
            check_archive_integrity(&archive.path()).unwrap(),
            Status { seen: 3, failed: 1 }
        );
    }

    #[test]
    fn min_count() {
        fn create_one_object(archive: &Archive) {
            let mut reader = &b"data"[..];
            let hash =
                Hash::new("3a6eb0790f39ac87c94f3856b2dd2c5d110e6811602261a9a923d3bb23adc8b7")
                    .unwrap();
            archive
                .create_object("bucket1", &hash, &mut reader)
                .unwrap();
        }

        {
            let (_temp_dir, archive) = create_archive_with_min_objects(0);
            assert_eq!(
                check_archive_integrity(&archive.path()).unwrap(),
                Status { seen: 0, failed: 0 }
            );
        }
        {
            let (_temp_dir, archive) = create_archive_with_min_objects(1);
            assert_eq!(
                check_archive_integrity(&archive.path())
                    .unwrap_err()
                    .to_string(),
                "too few objects seen in archive"
            );
        }
        {
            let (_temp_dir, archive) = create_archive_with_min_objects(0);
            create_one_object(&archive);
            assert_eq!(
                check_archive_integrity(&archive.path()).unwrap(),
                Status { seen: 1, failed: 0 }
            );
        }
        {
            let (_temp_dir, archive) = create_archive_with_min_objects(1);
            create_one_object(&archive);
            assert_eq!(
                check_archive_integrity(&archive.path()).unwrap(),
                Status { seen: 1, failed: 0 }
            );
        }
        {
            let (_temp_dir, archive) = create_archive_with_min_objects(2);
            assert_eq!(
                check_archive_integrity(&archive.path())
                    .unwrap_err()
                    .to_string(),
                "too few objects seen in archive"
            );
        }
    }

    fn create_archive_with_min_objects(min_objects: u64) -> (TempDir, Archive) {
        let temp_dir = tempfile::tempdir().unwrap();
        let archive_dir = temp_dir.path().join("archive");
        let archive = Archive::init(&archive_dir).unwrap();

        let config = format!(
            r#"
            [archive]
            min_objects = {min_objects}

            [s3]
            cloudscale_api_key = "unused"
            endpoint = "https://objects.rma.cloudscale.ch"

            [removal]
            retention_time_days = 180
            dry_run = false

            [[db_servers]]
            host_name = "localhost"
        "#
        );
        fs::write(archive.config_path(), config).unwrap();
        (temp_dir, Archive::open(&archive_dir).unwrap())
    }

    #[test]
    fn object_last_seen_time() {
        let dir = tempfile::tempdir().unwrap();
        let dir = dir.path().join("archive");
        let objects_in_db = [
            "4e07408562bedb8b60ce05c1decfe3ad16b72230967de01f640b7e4729b49fce",
            "6b86b273ff34fce19d6b804eff5a3f5747ada4eaa22f1d49c01e52ddb7875b4b",
        ];
        let objects_on_disk = [
            (
                "4e07408562bedb8b60ce05c1decfe3ad16b72230967de01f640b7e4729b49fce",
                "nice_agogis",
                b"3",
            ),
            (
                "6b86b273ff34fce19d6b804eff5a3f5747ada4eaa22f1d49c01e52ddb7875b4b",
                "nice_abc",
                b"1",
            ),
            // missing in DB
            (
                "d4735e3a265e16eee03f59718b9b5d03019c07d8b6c51f90da3a666eec13ab35",
                "nice_agogis",
                b"2",
            ),
        ];
        let archive = Archive::init(&dir).unwrap();
        let mut db = archive.state_db().unwrap();

        for (hash, bucket, content) in objects_on_disk {
            let hash = Hash::new(hash).unwrap();
            archive
                .create_object(bucket, &hash, &mut content.as_slice())
                .unwrap();
        }

        db.update_last_seen_times(objects_in_db.iter().map(|h| Hash::new(h).unwrap()))
            .unwrap();

        thread::sleep(time::Duration::from_secs(2));

        let status = check_archive_integrity(&dir).unwrap();
        assert_eq!(status.seen, 3);
        assert_eq!(status.failed, 0);

        // check_archive_integrity() did not touch any existing objects. Thus, last_seen
        // is > 2 seconds for objects that were in DB already.
        let observed = objects_older_than(&archive, Duration::seconds(2));
        assert_eq!(observed, objects_in_db);

        let observed = objects_older_than(&archive, Duration::zero());
        let expected: Vec<_> = objects_on_disk.iter().map(|(hash, _, _)| *hash).collect();
        // check_archive_integrity() added object missing in DB.
        assert_eq!(observed, expected);
    }

    fn objects_older_than(archive: &Archive, duration: Duration) -> Vec<String> {
        let mut pager = ObjectsOlderThanPager::new(duration, 100);
        archive
            .state_db()
            .unwrap()
            .next_objects_older_than(&mut pager)
            .unwrap()
            .unwrap()
            .iter()
            .map(|h| h.hex_digest().clone())
            .collect()
    }
}
