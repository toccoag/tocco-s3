//! Initialize backup archive

use clap::Args;
use std::path::PathBuf;
use tocco_s3::fs::Archive;

/// List DB servers
///
/// Lists one server per line, without port or username.
#[derive(Args)]
pub(crate) struct ListServers {
    /// Path to backup archive.
    #[arg()]
    directory: PathBuf,
}

pub(crate) fn main(args: &ListServers) -> u8 {
    let archive = match Archive::open(&args.directory) {
        Ok(archive) => archive,
        Err(e) => {
            let directory = &args.directory;
            eprintln!("Failed to open archive at {directory:?}: {e}");
            return 1;
        }
    };
    for server in archive.db_servers() {
        println!("{}", server.name());
    }
    0
}
