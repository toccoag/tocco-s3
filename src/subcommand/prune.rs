use aws_sdk_s3 as s3;
use chrono::Duration;
use clap::Args;
use fallible_iterator::FallibleIterator;
use futures::future;
use lazy_static::lazy_static;
use log::{debug, error, info, warn, Level};
use std::collections::{HashMap, HashSet};
use std::path::PathBuf;
use std::sync::{Arc, Mutex};
use tocco_s3::fs::object::ObjectFinder;
use tocco_s3::fs::{self, Archive};
use tocco_s3::hash::Hash;
use tocco_s3::state::{self, ObjectsOlderThanPager};
use tocco_s3::Error;
use tokio::task;

macro_rules! log_once_per_key {
    ($level:expr, $key:expr, $($arg:tt)+) => {
        if log::log_enabled!($level) {
            lazy_static! {
                static ref SEEN: Mutex<HashSet<String>> = Mutex::new(HashSet::new());
            };
            let mut seen = SEEN.lock().expect("poisoned lock");
            if !seen.contains($key) {
                seen.insert($key.to_owned());
                log::log!($level, $($arg)+);
            }
        }
    };
}

#[derive(Clone, Copy)]
enum Mode {
    DryRun,
    WetRun,
}

#[derive(Args)]
pub(crate) struct Prune {
    /// Directory where backups are located
    #[arg()]
    directory: PathBuf,

    /// Show what would be removed without actual removal.
    ///
    /// Dry-run mode enabled via `config.toml` overrides this
    /// setting.
    #[arg(long, short)]
    dry_run: bool,

    /// Override dry_run specified in config.toml.
    ///
    /// Only allowed in combination with BUCKETS.
    #[arg(long, conflicts_with = "dry_run", requires_if("true", "buckets"))]
    dry_run_override: bool,

    /// Buckets
    ///
    /// Limit removal to these buckets. May be given multiple times
    /// to specify multiple buckets.
    #[arg()]
    buckets: Option<Vec<String>>,
}

async fn remove_objects(
    archive: &Archive,
    buckets: &Option<Vec<String>>,
    retention_time: Duration,
    mode: Mode,
) -> Result<u64, Error> {
    let page_size = 100_000;
    let mut count = 0_u64;
    let mut pager = ObjectsOlderThanPager::new(retention_time, page_size);
    let mut db = archive.state_db()?;
    let finder = match buckets {
        Some(buckets) => archive.object_finder_for_buckets(buckets.iter()),
        None => archive.object_finder(),
    }?;
    let finder = Arc::new(finder);
    let s3_keys = archive.s3_keys().await?;
    let client_cache = tocco_s3::s3::ClientCache::new(archive.s3_endpoint(), s3_keys);

    sanity_checks(&mut db)?;

    info!("Fetching first page of objects.");
    while let Some(page) = db.next_objects_older_than(&mut pager)? {
        info!("Fetched page of {} objects.", { page.len() });
        info!(
            "First object in page is {:?}",
            page.first()
                .map(|h| h.hex_digest())
                .unwrap_or_else(|| "<none>".to_string())
        );
        remove_chunk_of_objects(&finder, page, &client_cache, mode).await?;
        match mode {
            Mode::WetRun => {
                if buckets.is_none() {
                    info!("Removing {} objects from state DB.", page.len());
                    db.delete_objects(page)?;
                } else {
                    // We'd have to check if the object *only* belongs to one or more of the
                    // selected buckets, but no other bucket, to be sure it can be removed from the
                    // state DB. Note that the state DB does not track to which bucket an object belongs.
                    // Because selecting buckets is only for debugging, let's not bother. State DB will
                    // be cleaned up on the next run over all buckets.
                    debug!(
                        "Updating state DB not supported when running on only a set of buckets, \
                         skipping ..."
                    );
                }
            }
            Mode::DryRun => {
                println!(
                    "dry-run: Would remove {} objects from state DB.",
                    page.len()
                );
            }
        }
        count += page.len() as u64;
    }
    Ok(count)
}

fn sanity_checks(db: &mut state::Db) -> Result<(), Error> {
    let max_age_days = 8;
    let max_age_ts = chrono::Local::now() - chrono::Duration::days(max_age_days);

    {
        let last_db_scan = db.last_db_scan_time()?;
        if last_db_scan < max_age_ts {
            return Err(Error::Custom(format!(
                "There has been no DB scan for more than {max_age_days} days. Last scan happened \
              {last_db_scan}. Refusing to remove any objects. Run tocco-s3 check-availability \
              first.",
            )));
        }
    }

    {
        let last_archive_scan = db.last_archive_scan_time()?;
        if last_archive_scan < max_age_ts {
            return Err(Error::Custom(format!(
            "There has been no archive scan for more than {max_age_days} days. Last scan happened \
              {last_archive_scan}. Refusing to remove any objects. Run tocco-s3 check-content \
              first.",
            )));
        }
    }

    {
        let last_s3_backup_time = db.last_s3_backup_time()?;
        if last_s3_backup_time < max_age_ts {
            return Err(Error::Custom(format!(
            "There has been no archive scan for more than {max_age_days} days. Last scan happened \
              {last_s3_backup_time}. Refusing to remove any objects. Run tocco-s3 backup \
              first.",
            )));
        }
    }

    Ok(())
}

async fn remove_chunk_of_objects(
    finder: &Arc<ObjectFinder>,
    hashes: &'_ [Hash],
    client_cache: &tocco_s3::s3::ClientCache,
    mode: Mode,
) -> Result<(), Error> {
    info!(
        "Scanning disk for objects with any of {} hashes",
        hashes.len()
    );
    let mut join_set = task::JoinSet::new();
    for hash in hashes {
        let hash = hash.clone();
        let finder = Arc::clone(finder);
        join_set.spawn(async {
            task::spawn_blocking(move || {
                let objects = finder.all_with_hash(&hash).collect::<Vec<_>>()?;
                if objects.is_empty() {
                    info!("No object with hash {hash:?} found on disk while removing.");
                }
                Ok::<_, Error>(objects)
            })
            .await
            .expect("worker panicked")
        });
    }
    let mut objects = Vec::new();
    while let Some(objects_with_hash) = join_set.join_next().await {
        let objects_with_hash = objects_with_hash.expect("worker panicked")?;
        objects.extend(objects_with_hash);
    }
    info!(
        "Scanned disk for objects with any of {} hashes",
        hashes.len()
    );
    // Removing from S3 first because, when resuming after an error, we
    // need the copy on disk to determine the bucket name(s).
    remove_objects_from_s3(&objects, client_cache, mode).await?;
    remove_objects_from_disk(objects, mode).await?;
    Ok(())
}

async fn remove_objects_from_s3(
    objects: &[fs::Object],
    client_cache: &tocco_s3::s3::ClientCache,
    mode: Mode,
) -> Result<(), Error> {
    let mut grouped_by_bucket = HashMap::new();
    for object in objects {
        grouped_by_bucket
            .entry(object.bucket())
            .or_insert_with(|| Vec::new())
            .push(object);
    }

    for (bucket, objects) in grouped_by_bucket {
        if let Some(client) = client_cache.client(bucket) {
            info!(
                "Removing {} objects from s3 bucket {bucket:?}.",
                objects.len()
            );
            let mut tasks = Vec::new();
            match mode {
                Mode::WetRun => {
                    tasks.push(async {
                        let mut already_removed = 0;
                        // 1_000 is the max. allowed by API.
                        for chunk in objects.chunks(1_000) {
                            already_removed += remove_objects_from_bucket(&client, chunk).await?;
                        }
                        if already_removed > 0 {
                            info!("{already_removed} objects did not exist when removing objects from {bucket:?}.");
                        };
                        Ok::<_, Error>(())
                    });
                    future::try_join_all(tasks).await?;
                }
                Mode::DryRun => {
                    for object in objects {
                        println!("dry-run: Would remove {object:?} from S3.");
                    }
                }
            }
        } else {
            log_once_per_key!(
                Level::Info,
                bucket,
                "No S3 key for bucket {bucket} found. Skipping removal from S3 for all objects in \
                 bucket. Installation and bucket were likely removed.",
            );
        }
    }
    Ok(())
}

async fn remove_objects_from_bucket(
    client: &s3::Client,
    objects: &[&fs::Object],
) -> Result<u64, Error> {
    debug_assert!(objects.len() <= 1_000);
    let bucket = objects[0].bucket();
    debug!(
        "Removing {} objects from S3 bucket {bucket:?} in a single request.",
        objects.len()
    );
    let objects: Vec<_> = objects
        .iter()
        .inspect(|obj| debug_assert_eq!(bucket, obj.bucket()))
        .map(|obj| {
            s3::types::ObjectIdentifier::builder()
                .key(obj.hash().hex_digest())
                .build()
                .expect("cannot build object identifier")
        })
        .collect();
    let objects = s3::types::Delete::builder()
        .set_objects(Some(objects))
        .build()
        .expect("cannot build delete request");
    let resp = client
        .delete_objects()
        .set_bucket(Some(bucket.to_string()))
        .set_delete(Some(objects))
        .send()
        .await?;
    debug!(
        "Removal stats for bucket {bucket}: {} deleted, {} failed.",
        resp.deleted.map(|i| i.len()).unwrap_or(0),
        resp.errors.as_ref().map(|i| i.len()).unwrap_or(0),
    );
    let mut already_removed = 0;
    let mut hard_errors = 0_u64;
    if let Some(errors) = resp.errors {
        for error in errors {
            match error.code.as_deref() {
                Some("NoSuchKey") => info!(
                    "Object {} not found in bucket {bucket}.",
                    error.key().as_ref().unwrap_or(&"<unknown>")
                ),
                Some("NoSuchBucket") => {
                    info!(
                        "Bucket {bucket} not found while removing {}.",
                        error.key().as_ref().unwrap_or(&"<unknown>")
                    );
                    already_removed += 1;
                }
                _ => {
                    error!(
                        "Failed to remove object {} from bucket {bucket}: {}: {}",
                        error.key().as_ref().unwrap_or(&"<unknown>"),
                        error.code().as_ref().unwrap_or(&"<unknown>"),
                        error.message().as_ref().unwrap_or(&"<unknown>"),
                    );
                    hard_errors += 1;
                }
            }
        }
    }
    if hard_errors == 0 {
        Ok(already_removed)
    } else {
        Err(Error::Custom(format!(
            "failed to remove {hard_errors} object(s)"
        )))
    }
}

async fn remove_objects_from_disk(objects: Vec<fs::Object>, mode: Mode) -> Result<(), Error> {
    match mode {
        Mode::WetRun => {
            let len = objects.len();
            info!("Removing {len} objects from disk.");
            let tasks: Vec<_> = objects
                .into_iter()
                .inspect(|obj| debug!("Removing {obj:?} from disk."))
                .map(|obj| async {
                    task::spawn_blocking(|| obj.delete())
                        .await
                        .expect("worker panicked")
                })
                .collect();
            future::try_join_all(tasks).await?;
            info!("Removed {len} objects from disk.");
        }
        Mode::DryRun => {
            for obj in objects {
                println!("dry-run: Would remove {obj:?} from disk.");
            }
        }
    }
    Ok(())
}

pub(crate) async fn main(args: &Prune) -> u8 {
    let archive = match Archive::open(&args.directory) {
        Ok(archive) => archive,
        Err(e) => {
            error!("Failed to open archive: {e}");
            return 1;
        }
    };

    if let Some(buckets) = &args.buckets {
        for bucket in buckets {
            if !bucket.starts_with("tocco-nice-") {
                warn!(
                    "Bucket named {bucket:?} does not start with \"tocco-nice-\". This is likely \
                     a mistake."
                );
            }
        }
    }
    let mode = if args.dry_run_override {
        assert!(args.buckets.is_some());
        Mode::WetRun
    } else if args.dry_run || archive.removal_dry_run_enabled() {
        Mode::DryRun
    } else {
        Mode::WetRun
    };
    match remove_objects(&archive, &args.buckets, archive.retention_time(), mode).await {
        Ok(count) => {
            println!("Successfully removed {count} objects.");
        }
        Err(err) => {
            error!("pruning aborted: {err:?}");
            eprintln!("Failure during object removal: {err}");
            return 1;
        }
    }
    0
}

#[cfg(test)]
mod tests {
    use super::*;
    use clap::Parser;
    use std::io::Write;

    #[test]
    fn override_requires_buckets() {
        let mut args = vec!["command", "prune", "--dry-run-override", "/path/to/archive"];

        // Missing bucket
        let err = crate::Args::try_parse_from(&args).err().unwrap();
        assert_eq!(err.kind(), clap::error::ErrorKind::MissingRequiredArgument);

        // Bucket specified
        args.push("bucket");
        let args = crate::Args::try_parse_from(&args).unwrap();
        let args = match args.command {
            crate::Commands::Prune(ref args) => args,
            _ => panic!(),
        };
        assert_eq!(args.buckets.as_ref().unwrap(), &["bucket"]);
        assert!(args.dry_run_override);
    }

    struct TestArchive {
        archive: Archive,
        dir: PathBuf,
        _temp_dir: tempfile::TempDir,
    }

    fn set_up_archive(dry_run: bool) -> TestArchive {
        let temp_dir = tempfile::tempdir().unwrap();
        let dir = temp_dir.path().join("archive");
        let archive = Archive::init(&dir).unwrap();
        {
            let mut config = std::fs::File::create(archive.config_path()).unwrap();
            let endpoint = test_utils::s3_endpoint();
            let (key_id, secret) = test_utils::s3_key();
            write!(
                &mut config,
                r#"
                    [archive]
                    min_objects = 1

                    [removal]
                    retention_time_days = 180
                    dry_run = {dry_run}

                    [s3]
                    endpoint = "{endpoint}"

                    [s3.keys.bucket1a]
                    access_key = "{key_id}"
                    secret_key = "{secret}"

                    [s3.keys.bucket2a]
                    access_key = "{key_id}"
                    secret_key = "{secret}"

                    [s3.keys.bucket2b]
                    access_key = "{key_id}"
                    secret_key = "{secret}"

                    [s3.keys.bucket2c]
                    access_key = "{key_id}"
                    secret_key = "{secret}"

                    [s3.keys.bucket2d]
                    access_key = "{key_id}"
                    secret_key = "{secret}"

                    [s3.keys.bucket3a]
                    access_key = "{key_id}"
                    secret_key = "{secret}"

                    [s3.keys.bucket4a]
                    access_key = "{key_id}"
                    secret_key = "{secret}"

                    [[db_servers]]
                    host_name = "unused"
                "#
            )
            .unwrap();
        }
        // reopen to reload config
        let archive = Archive::open(&dir).unwrap();
        let state_db = archive.state_db().unwrap();
        state_db.update_last_db_scan_time().unwrap();
        state_db.update_last_archive_scan_time().unwrap();
        TestArchive {
            archive,
            dir,
            _temp_dir: temp_dir,
        }
    }

    #[tokio::test]
    async fn test_main() {
        let _ = env_logger::builder().is_test(true).try_init();

        let _ = env_logger::builder()
            .filter_level(log::LevelFilter::Debug)
            .is_test(true)
            .try_init();
        let test_archive = set_up_archive(false);
        let mut state_db = test_archive.archive.state_db().unwrap();

        let bucket = "bucket1a";
        let objects_to_keep = [
            Hash::new("2e7d2c03a9507ae265ecf5b5356885a53393a2029d241394997265a1a25aefc6").unwrap(),
            Hash::new("3e23e8160039594a33894f6564e1b1348bbd7a0088d42c4acb73eeaed59c009d").unwrap(),
            Hash::new("ca978112ca1bbdcafac231b39a23dc4da786eff8147c4e72b9807785afee48bb").unwrap(),
        ];
        let objects_to_remove = [
            Hash::new("18ac3e7343f016890c510e93f935261169d9e3f565436429830faf0934f4f8e4").unwrap(),
            Hash::new("252f10c83610ebca1a059c0bae8255eba2f95be4d1d7bcfa89d7248a82d9f111").unwrap(),
            Hash::new("3f79bb7b435b05321651daefd374cdc681dc06faa65e374e38337b88ca046dea").unwrap(),
        ];

        let now = chrono::Local::now();
        let a_181_days_ago = now - chrono::Duration::days(181);

        for object in objects_to_keep.iter().chain(&objects_to_remove) {
            test_utils::create_empty_object_on_disk(
                &test_archive.archive,
                bucket,
                &object.hex_digest(),
            );
            test_utils::create_empty_object_on_s3(bucket, &object.hex_digest()).await;
        }

        state_db.update_last_seen_times(&objects_to_keep).unwrap();

        for object in &objects_to_remove {
            state_db.set_last_seen_time(object, a_181_days_ago).unwrap();
        }

        let args = Prune {
            directory: test_archive.dir.clone(),
            dry_run: false,
            dry_run_override: false,
            buckets: None,
        };
        state_db.update_last_s3_backup_time().unwrap();
        assert_eq!(main(&args).await, 0);

        for object in &objects_to_keep {
            assert!(does_object_exist(&test_archive.archive, bucket, object).await);
            assert!(state_db.last_seen_time(object).unwrap().is_some());
        }

        for object in &objects_to_remove {
            assert!(!does_object_exist(&test_archive.archive, bucket, object).await);
            assert!(state_db.last_seen_time(object).unwrap().is_none());
        }

        let mut pager = ObjectsOlderThanPager::new(chrono::Duration::zero(), 10);
        let page = state_db
            .next_objects_older_than(&mut pager)
            .unwrap()
            .unwrap();
        assert_eq!(page, objects_to_keep);
    }

    #[tokio::test]
    async fn test_main_with_buckets() {
        let _ = env_logger::builder().is_test(true).try_init();

        let test_archive = set_up_archive(false);
        let mut state_db = test_archive.archive.state_db().unwrap();

        let buckets_to_clean = vec!["bucket2a".to_string(), "bucket2b".to_string()];
        // In selected `buckets_to_clean`
        let objects_to_keep = [
            (
                "bucket2c",
                Hash::new("ca978112ca1bbdcafac231b39a23dc4da786eff8147c4e72b9807785afee48bb")
                    .unwrap(),
            ),
            (
                "bucket2d",
                Hash::new("3e23e8160039594a33894f6564e1b1348bbd7a0088d42c4acb73eeaed59c009d")
                    .unwrap(),
            ),
            (
                "bucket2c",
                Hash::new("2e7d2c03a9507ae265ecf5b5356885a53393a2029d241394997265a1a25aefc6")
                    .unwrap(),
            ),
        ];

        // Not in selected `buckets_to_clean`
        let objects_to_remove = [
            (
                "bucket2a",
                Hash::new("18ac3e7343f016890c510e93f935261169d9e3f565436429830faf0934f4f8e4")
                    .unwrap(),
            ),
            (
                "bucket2a",
                Hash::new("3f79bb7b435b05321651daefd374cdc681dc06faa65e374e38337b88ca046dea")
                    .unwrap(),
            ),
            (
                "bucket2b",
                Hash::new("252f10c83610ebca1a059c0bae8255eba2f95be4d1d7bcfa89d7248a82d9f111")
                    .unwrap(),
            ),
        ];

        let now = chrono::Local::now();
        let a_181_days_ago = now - chrono::Duration::days(181);

        for (bucket, object) in objects_to_keep.iter().chain(&objects_to_remove) {
            test_utils::create_empty_object_on_disk(
                &test_archive.archive,
                bucket,
                &object.hex_digest(),
            );
            test_utils::create_empty_object_on_s3(bucket, &object.hex_digest()).await;
        }

        for (_bucket, object) in objects_to_remove.iter().chain(&objects_to_keep) {
            state_db.set_last_seen_time(object, a_181_days_ago).unwrap();
        }

        let args = Prune {
            directory: test_archive.dir.clone(),
            dry_run: false,
            dry_run_override: false,
            buckets: Some(buckets_to_clean),
        };
        state_db.update_last_s3_backup_time().unwrap();
        assert_eq!(main(&args).await, 0);

        for (bucket, object) in &objects_to_keep {
            assert!(does_object_exist(&test_archive.archive, bucket, object).await);
            assert!(state_db.last_seen_time(object).unwrap().is_some());
        }

        for (bucket, object) in &objects_to_remove {
            assert!(!does_object_exist(&test_archive.archive, bucket, object).await);

            // State DB should not be updated as an object (hash) could be referenced from
            // other buckets than the ones listed in `buckets_to_clean`. Thus, the row
            // referencing it should be `Some(_)`.
            assert!(state_db.last_seen_time(object).unwrap().is_some());
        }
    }

    #[tokio::test]
    async fn dry_run() {
        let _ = env_logger::builder().is_test(true).try_init();

        let test_archive = set_up_archive(false);
        let mut state_db = test_archive.archive.state_db().unwrap();

        let bucket = "bucket3a";
        let object =
            Hash::new("ca978112ca1bbdcafac231b39a23dc4da786eff8147c4e72b9807785afee48bb").unwrap();
        let now = chrono::Local::now();
        let a_181_days_ago = now - chrono::Duration::days(181);

        test_utils::create_empty_object_on_disk(
            &test_archive.archive,
            bucket,
            &object.hex_digest(),
        );
        test_utils::create_empty_object_on_s3(bucket, &object.hex_digest()).await;
        state_db
            .set_last_seen_time(&object, a_181_days_ago)
            .unwrap();

        // Dry run should not touch anything

        let args = Prune {
            directory: test_archive.dir.clone(),
            dry_run: true,
            dry_run_override: false,
            buckets: None,
        };
        state_db.update_last_s3_backup_time().unwrap();
        assert_eq!(main(&args).await, 0);
        assert!(does_object_exist(&test_archive.archive, bucket, &object).await);
        assert!(state_db.last_seen_time(&object).unwrap().is_some());

        // Without dry run, object is removed

        let args = Prune {
            directory: test_archive.dir.clone(),
            dry_run: false,
            dry_run_override: false,
            buckets: None,
        };
        assert_eq!(main(&args).await, 0);
        assert!(!does_object_exist(&test_archive.archive, bucket, &object).await);
        assert!(state_db.last_seen_time(&object).unwrap().is_none());
    }

    #[tokio::test]
    async fn large_number_of_objects() {
        let _ = env_logger::builder().is_test(true).try_init();

        let test_archive = set_up_archive(false);
        let mut state_db = test_archive.archive.state_db().unwrap();

        let bucket = "bucket4a";
        // More than 1_000 to ensure more than one request is needed to
        // remove objects from S3.
        let objects: Vec<_> = (0..1001)
            .map(|i| Hash::new(&format!("a{i:063}")).unwrap())
            .collect();
        let now = chrono::Local::now();
        let a_181_days_ago = now - chrono::Duration::days(181);

        for object in &objects {
            test_utils::create_empty_object_on_disk(
                &test_archive.archive,
                bucket,
                &object.hex_digest(),
            );
            test_utils::create_empty_object_on_s3(bucket, &object.hex_digest()).await;
            state_db.set_last_seen_time(object, a_181_days_ago).unwrap();
        }

        let args = Prune {
            directory: test_archive.dir.clone(),
            dry_run: false,
            dry_run_override: false,
            buckets: None,
        };
        state_db.update_last_s3_backup_time().unwrap();
        assert_eq!(main(&args).await, 0);

        for object in &objects {
            assert!(!does_object_exist(&test_archive.archive, bucket, object).await);
            assert!(state_db.last_seen_time(object).unwrap().is_none());
        }
    }

    async fn does_object_exist(archive: &Archive, bucket: &str, hash: &Hash) -> bool {
        let in_archive = archive
            .object_finder_for_buckets([bucket])
            .unwrap()
            .exists_with_hash(hash)
            .unwrap();
        let on_s3 = test_utils::s3_object_exists(bucket, &hash.hex_digest()).await;
        match (in_archive, on_s3) {
            (true, true) => true,
            (false, false) => false,
            (true, false) => panic!("{bucket}/{hash} only exists in archive."),
            (false, true) => panic!("{bucket}/{hash} only exists on S3."),
        }
    }
}
