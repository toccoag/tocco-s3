//! Verify all objects on referenced on DB server exist in archive.

use clap::Args;
use fallible_iterator::FallibleIterator;
use log::{error, info};
use std::io;
use std::ops;
use std::path::PathBuf;
use tocco_s3::db;
use tocco_s3::db::ssh::{self, DbServer};
use tocco_s3::error::Error;
use tocco_s3::fs::object::ObjectFinder;
use tocco_s3::fs::Archive;
use tocco_s3::state;

#[derive(Clone, Copy, Debug, Default)]
#[must_use]
struct Status {
    seen: u64,
    missing: u64,
}

impl ops::Add<Status> for Status {
    type Output = Status;

    fn add(self, other: Status) -> Status {
        Status {
            seen: self.seen + other.seen,
            missing: self.missing + other.missing,
        }
    }
}

impl ops::AddAssign<Status> for Status {
    fn add_assign(&mut self, other: Status) {
        self.seen += other.seen;
        self.missing += other.missing;
    }
}

impl Status {
    fn ok(&self) -> Result<u64, Status> {
        if self.missing > 0 {
            Err(*self)
        } else {
            Ok(self.seen)
        }
    }
}

/// Fetch all referenced objects from DB servers defined in
/// config.toml and verify they exist locally.
///
/// The DB server sends a list with all hashes for all objects
/// referenced by any DB. This hash is then searched in
/// the backup archive. When the object doesn't exist
/// in any bucket, an error is reported.
///
/// Additionally, it is checked that at least `min_objects`
/// (as defined in config.toml) exist on the server.
///
/// On error, a non-zero return value is returned.
#[derive(Args)]
pub(crate) struct CheckAvailability {
    /// Directory of backup archive.
    #[arg()]
    directory: PathBuf,

    /// Command used to to fetch list of objects.
    ///
    /// Instead to connect to all DB servers in config.toml
    /// to fetch the corresponding list, execute this
    /// command in the user's shell and use its stdout.
    ///
    /// For debugging only.
    #[arg(long)]
    #[cfg(unix)]
    command: Option<String>,

    /// Ignore objects younger than N hours when fetching
    /// list of objects from DB servers.
    #[arg(long, default_value = "48")]
    min_age_hours: u32,
}

/// Run integrity check on all objects on all servers.
///
/// Returns `(object_seen, object_failed)`.
fn check_archive_integrity(archive: &Archive, db_servers: Vec<DbServer>) -> Result<Status, Error> {
    let mut overall_status = Status::default();

    let mut finder = match archive.object_finder() {
        Ok(v) => v,
        Err(e) => {
            error!("Failed to read buckets: {e}");
            return Err(e.into());
        }
    };
    let mut state_db = archive.state_db()?;
    for mut db_server in db_servers {
        info!("Processing {db_server}");
        let status = check_against_db_server(&mut state_db, &mut finder, &mut db_server)?;
        overall_status += status;

        match status.ok() {
            Ok(ok) => {
                println!("Found {ok} objects on {db_server} with none missing.");
            }
            Err(Status { seen, missing }) => {
                println!("Found {seen} objects on {db_server} with {missing} missing.");
            }
        }
    }

    state_db.update_last_db_scan_time()?;

    Ok(overall_status)
}

fn check_against_db_server(
    state_db: &mut state::Db,
    finder: &mut ObjectFinder,
    db_server: &mut DbServer,
) -> Result<Status, Error> {
    let mut proc = match db_server.spawn() {
        Ok(conn) => conn,
        Err(e) => {
            error!("Failed to connect to {db_server}: {e}");
            return Err(e.into());
        }
    };
    let mut status = Status::default();
    // `take()` to make sure stdout gets closed when reader is `Drop`ped.
    let mut reader = proc.stdout.take().expect("missing stdout");
    let mut reader = db::parse::ObjectReader::new(&mut reader).iterator();
    let commit_size = 100_000;
    let mut object_buffer = Vec::with_capacity(commit_size);
    loop {
        let reader_inner = reader.by_ref().take(commit_size);
        for object in reader_inner {
            status.seen += 1;
            match object {
                Ok(object) => {
                    object_buffer.push(object);
                }
                Err(e) => {
                    error!("Failed to parse object in stream from {db_server}: {e}");
                    return Err(e.into());
                }
            }
        }
        if object_buffer.is_empty() {
            break;
        }
        for object in &object_buffer {
            if let Err(e) = assert_object_in_archive(finder, object) {
                status.missing += 1;
                error!("Failed to find {object:?} referenced on {db_server}: {e}");
            }
        }
        state_db.update_last_seen_times(object_buffer.iter().map(|o| o.hash()))?;
        object_buffer.truncate(0);
    }

    match proc.wait() {
        Err(e) => {
            error!("Failed to fetch commands exit status: {e}.");
            Err(e.into())
        }
        Ok(status) if !status.success() => {
            error!("Command to fetch object list from {db_server} exited with an error: {status}.");
            Err(io::Error::new(io::ErrorKind::Other, "command exited with error").into())
        }
        Ok(_) if status.seen < db_server.min_objects() => {
            error!(
                "Expected to find at least {} objects on {db_server} but only found {}",
                db_server.min_objects(),
                status.seen,
            );
            Err(io::Error::new(io::ErrorKind::Other, "Found too few objects on server").into())
        }
        Ok(_) => Ok(status),
    }
}

fn assert_object_in_archive(
    finder: &mut ObjectFinder,
    object: &db::parse::Object,
) -> io::Result<()> {
    if finder.exists_with_hash(object.hash())? {
        Ok(())
    } else {
        Err(io::Error::new(
            io::ErrorKind::NotFound,
            "object missing in backup archive",
        ))
    }
}

pub(crate) fn main(args: &CheckAvailability) -> u8 {
    let archive = match Archive::open(&args.directory) {
        Ok(v) => v,
        Err(e) => {
            error!("Failed to open backup archive: {e}");
            return 1;
        }
    };

    #[cfg(unix)]
    let commands = match &args.command {
        Some(ref command) => ssh::custom_db_server(command, 0),
        None => ssh::db_servers(&archive, args.min_age_hours),
    };

    #[cfg(not(unix))]
    let commands = ssh::db_servers(&archive, args.min_age_hours);

    let status = match check_archive_integrity(&archive, commands) {
        Ok(status) => status,
        Err(e) => {
            println!("Error during integrity check: {e}");
            return 1;
        }
    };
    match status.ok() {
        Ok(seen) => {
            println!("Processed {seen} object(s) and found no missing objects.");
            0
        }
        Err(Status { seen, missing }) => {
            println!("Processed {seen} object(s) and found {missing} missing objects.");
            println!("See previously printed errors.");
            1
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use chrono::{DateTime, FixedOffset};
    use std::time::Duration;
    use test_utils::create_empty_object_on_disk;
    use tocco_s3::state::ObjectsOlderThanPager;

    #[test]
    fn main_test() {
        let dir = tempfile::tempdir().unwrap();
        let dir = dir.path().join("archive");
        let command = r#"\
            echo c5523e9396c7ea5052a93fce910fd233070ad6371d3df4bdc9b9e4b9fc4f1cd4,nice_master
            echo 591792475f07d17c980040225deae48092077c2e8cbe093cd1daa9c9245a2bbb,nice_master
            echo d2f3ee796e88e47b95a617055108e4aa245aa1a820b4b3f64a0fd487ca7e321e,nice_agogis
            echo f9784fc64440c922a5703c76a2633770deb953d8d96c96eae002521da8e51d04,nice_agogis
            echo 7a96c4f8dac68f63e44487ce45f280d6c87db95e1012628f3a805dcbab1c2f1e,nice_tds
            echo 591792475f07d17c980040225deae48092077c2e8cbe093cd1daa9c9245a2bbb,nice_uiae"#;
        let args = CheckAvailability {
            directory: dir.clone(),
            command: Some(command.to_string()),
            min_age_hours: 36,
        };
        let archive = Archive::init(&dir).unwrap();

        // all objects missing
        let status = check_archive_integrity(&archive, ssh::custom_db_server(command, 0)).unwrap();
        assert_eq!(status.seen, 6);
        assert_eq!(status.missing, 6);
        assert!(status.ok().is_err());
        assert_eq!(main(&args), 1);

        create_empty_object_on_disk(
            &archive,
            "nice_master",
            "c5523e9396c7ea5052a93fce910fd233070ad6371d3df4bdc9b9e4b9fc4f1cd4",
        );

        // most objects missing
        let status = check_archive_integrity(&archive, ssh::custom_db_server(command, 0)).unwrap();
        assert_eq!(status.seen, 6);
        assert_eq!(status.missing, 5);
        assert!(status.ok().is_err());
        assert_eq!(main(&args), 1);

        create_empty_object_on_disk(
            &archive,
            "nice_uiae",
            // this object exists twice
            "591792475f07d17c980040225deae48092077c2e8cbe093cd1daa9c9245a2bbb",
        );

        // some objects missing
        let status = check_archive_integrity(&archive, ssh::custom_db_server(command, 6)).unwrap();
        assert_eq!(status.seen, 6);
        assert_eq!(status.missing, 3);
        assert!(status.ok().is_err());
        assert_eq!(main(&args), 1);

        create_empty_object_on_disk(
            &archive,
            "nice_agogis",
            "d2f3ee796e88e47b95a617055108e4aa245aa1a820b4b3f64a0fd487ca7e321e",
        );
        create_empty_object_on_disk(
            &archive,
            "nice_agogis",
            "f9784fc64440c922a5703c76a2633770deb953d8d96c96eae002521da8e51d04",
        );
        create_empty_object_on_disk(
            &archive,
            "nice_tds",
            "7a96c4f8dac68f63e44487ce45f280d6c87db95e1012628f3a805dcbab1c2f1e",
        );

        // no objects missing
        let status = check_archive_integrity(&archive, ssh::custom_db_server(command, 6)).unwrap();
        assert_eq!(status.seen, 6);
        assert_eq!(status.missing, 0);
        assert!(status.ok().is_ok());
        assert_eq!(main(&args), 0);

        // Fewer than `min_objects` found.
        assert!(check_archive_integrity(&archive, ssh::custom_db_server(command, 7)).is_err());
    }

    #[test]
    fn failed_command() {
        let dir = tempfile::tempdir().unwrap();
        let dir = dir.path().join("archive");
        let command = r#"\
            echo c5523e9396c7ea5052a93fce910fd233070ad6371d3df4bdc9b9e4b9fc4f1cd4,nice_master
            exit 1"#;
        let args = CheckAvailability {
            directory: dir.clone(),
            command: Some(command.to_string()),
            min_age_hours: 36,
        };
        let archive = Archive::init(&dir).unwrap();

        assert!(check_archive_integrity(&archive, ssh::custom_db_server(command, 0)).is_err());
        assert_eq!(main(&args), 1);

        create_empty_object_on_disk(
            &archive,
            "nice_master",
            "c5523e9396c7ea5052a93fce910fd233070ad6371d3df4bdc9b9e4b9fc4f1cd4",
        );

        assert!(check_archive_integrity(&archive, ssh::custom_db_server(command, 0)).is_err());
        assert_eq!(main(&args), 1);
    }

    #[test]
    fn invalid_object_lines_from_db_server() {
        let test_data = [
            r#"
                # ok
                echo c5523e9396c7ea5052a93fce910fd233070ad6371d3df4bdc9b9e4b9fc4f1cd4,nice_master;

                # invalid hash
                echo gggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggg,nice_master

                # ok
                echo c5523e9396c7ea5052a93fce910fd233070ad6371d3df4bdc9b9e4b9fc4f1cd4,nice_master
            "#,
            r#"
                # ok
                echo c5523e9396c7ea5052a93fce910fd233070ad6371d3df4bdc9b9e4b9fc4f1cd4,nice_master

                # invalid
                echo

                # ok
                echo c5523e9396c7ea5052a93fce910fd233070ad6371d3df4bdc9b9e4b9fc4f1cd4,nice_master
            "#,
        ];

        for command in test_data {
            let dir = tempfile::tempdir().unwrap();
            let dir = dir.path().join("archive");
            let args = CheckAvailability {
                directory: dir.clone(),
                command: Some(command.to_string()),
                min_age_hours: 36,
            };
            let archive = Archive::init(&dir).unwrap();

            assert!(check_archive_integrity(&archive, ssh::custom_db_server(command, 0)).is_err());
            assert_eq!(main(&args), 1);

            create_empty_object_on_disk(
                &archive,
                "nice_master",
                "c5523e9396c7ea5052a93fce910fd233070ad6371d3df4bdc9b9e4b9fc4f1cd4",
            );

            assert!(check_archive_integrity(&archive, ssh::custom_db_server(command, 0)).is_err());
            assert_eq!(main(&args), 1);
        }
    }

    #[test]
    fn no_such_archive() {
        let args = CheckAvailability {
            directory: PathBuf::from("."),
            command: Some("true".to_string()),
            min_age_hours: 36,
        };
        assert_eq!(main(&args), 1);
    }

    #[test]
    fn last_scan_time() {
        let dir = tempfile::tempdir().unwrap();
        let dir = dir.path().join("archive");
        let command = r#"\
            echo c5523e9396c7ea5052a93fce910fd233070ad6371d3df4bdc9b9e4b9fc4f1cd4,nice_master
            echo invalid
            echo c5523e9396c7ea5052a93fce910fd233070ad6371d3df4bdc9b9e4b9fc4f1cd4,nice_agogis
        "#;
        let args = CheckAvailability {
            directory: dir.clone(),
            command: Some(command.to_string()),
            min_age_hours: 36,
        };
        let archive = Archive::init(&dir).unwrap();

        assert!(check_archive_integrity(&archive, ssh::custom_db_server(command, 0)).is_err());
        assert_eq!(main(&args), 1);

        // Not updated on error
        assert_eq!(
            archive.state_db().unwrap().last_db_scan_time().unwrap(),
            DateTime::<FixedOffset>::parse_from_rfc3339("0000-01-01T00:00:00Z").unwrap()
        );

        let command = r#"\
            echo c5523e9396c7ea5052a93fce910fd233070ad6371d3df4bdc9b9e4b9fc4f1cd4,nice_master
            echo c5523e9396c7ea5052a93fce910fd233070ad6371d3df4bdc9b9e4b9fc4f1cd4,nice_agogis
        "#;

        let status = check_archive_integrity(&archive, ssh::custom_db_server(command, 0)).unwrap();
        assert_eq!(status.seen, 2);
        assert_eq!(status.missing, 2);
        assert!(status.ok().is_err());

        // Updated on missing objects
        let ts = archive.state_db().unwrap().last_db_scan_time().unwrap();
        assert!(ts > DateTime::<FixedOffset>::parse_from_rfc3339("2023-01-01T00:00:00Z").unwrap());

        // Timestamp limited to one-second precission
        std::thread::sleep(Duration::from_secs(1));

        create_empty_object_on_disk(
            &archive,
            "nice_master",
            "c5523e9396c7ea5052a93fce910fd233070ad6371d3df4bdc9b9e4b9fc4f1cd4",
        );

        let status = check_archive_integrity(&archive, ssh::custom_db_server(command, 0)).unwrap();
        assert_eq!(status.seen, 2);
        assert_eq!(status.missing, 0);
        assert!(status.ok().is_ok());

        // Updated on no missing objects
        assert!(archive.state_db().unwrap().last_db_scan_time().unwrap() > ts);
    }

    #[test]
    fn object_last_seen_time() {
        let dir = tempfile::tempdir().unwrap();
        let dir = dir.path().join("archive");
        let dead_objects = r#"\
            echo c5523e9396c7ea5052a93fce910fd233070ad6371d3df4bdc9b9e4b9fc4f1cd4,nice_agogis
            echo 8f233e5f892d3f0fbb1883b78a07e13c5075af28de2ad30372b5044d44901de8,nice_abc
        "#;
        let live_objects = r#"\
            echo f65ea2558b94f7147f10d8d71b92fe0a1c38f069e0b39c290c23ee5859182ff9,nice_agogis
            echo 2dcc4f8cf7f29281c66dfd2078f056f1a9a98e1ddf1b5a0a90550fc3131dd749,nice_abc
        "#;
        let all_objects = dead_objects.to_string() + live_objects;
        let archive = Archive::init(&dir).unwrap();

        // Update last seen timestamp for all objects
        let status =
            check_archive_integrity(&archive, ssh::custom_db_server(&all_objects, 0)).unwrap();
        assert_eq!(status.seen, 4);
        assert_eq!(status.missing, 4);
        assert!(status.ok().is_err());

        let mut pager = ObjectsOlderThanPager::new(chrono::Duration::zero(), 12);
        let hashes = archive
            .state_db()
            .unwrap()
            .next_objects_older_than(&mut pager)
            .unwrap()
            .unwrap();
        assert_eq!(hashes.len(), 4);

        let mut pager = ObjectsOlderThanPager::new(chrono::Duration::seconds(10), 12);
        let hashes = archive
            .state_db()
            .unwrap()
            .next_objects_older_than(&mut pager)
            .unwrap();
        assert!(hashes.is_none());

        std::thread::sleep(Duration::from_secs(5));

        // Update last seen timestamp for live objects only
        let status =
            check_archive_integrity(&archive, ssh::custom_db_server(live_objects, 0)).unwrap();
        assert_eq!(status.seen, 2);
        assert_eq!(status.missing, 2);
        assert!(status.ok().is_err());

        let mut pager = ObjectsOlderThanPager::new(chrono::Duration::seconds(3), 12);
        let hashes = archive
            .state_db()
            .unwrap()
            .next_objects_older_than(&mut pager)
            .unwrap()
            .unwrap();
        assert_eq!(hashes.len(), 2);
        assert_eq!(
            hashes[0].hex_digest(),
            "8f233e5f892d3f0fbb1883b78a07e13c5075af28de2ad30372b5044d44901de8"
        );
        assert_eq!(
            hashes[1].hex_digest(),
            "c5523e9396c7ea5052a93fce910fd233070ad6371d3df4bdc9b9e4b9fc4f1cd4"
        );
    }
}
