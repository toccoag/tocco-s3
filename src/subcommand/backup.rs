use aws_sdk_s3 as s3;
use chrono::{DateTime, Local};
use clap::Args;
use log::{debug, error, info, trace, warn};
use std::collections::HashMap;
use std::fs;
use std::io;
use std::path::{Path, PathBuf};
use std::sync::Arc;
use tocco_s3::common;
use tocco_s3::fs::Archive;
use tocco_s3::hash::Hash;
use tocco_s3::s3::Key;
use tocco_s3::Result;
use tokio::{pin, task};
use tokio_stream::StreamExt;
use tokio_util::io::SyncIoBridge;

#[derive(Args)]
pub(crate) struct Backup {
    /// Directory where backup archive is located
    #[arg()]
    directory: PathBuf,

    /// Buckets
    ///
    /// Limit backup to given buckets. Buckets which
    /// do not exist are ignored silently.
    ///
    /// Intended for debugging only.
    #[arg()]
    buckets: Option<Vec<String>>,

    /// Max. concurrent downloads
    ///
    /// This limits the number of download tasks running
    /// concurrently. The download task also includes
    /// checking if an objects already exists locally.
    /// Thus, this too affectively limits the number of
    /// concurrent FS requests to check for existing
    /// objects.
    #[arg(long, short, value_parser(clap::value_parser!(u8).range(1..=250)), default_value_t=25)]
    concurrent_downloads: u8,
}

async fn backup(
    archive: &Arc<Archive>,
    s3_endpoint: &str,
    s3_keys: &HashMap<String, Key>,
    concurrent_downloads: usize,
) -> Result<()> {
    for (bucket, key) in s3_keys {
        info!("Processing bucket {bucket:?}");
        if let Err(e) = backup_bucket(archive, bucket, s3_endpoint, key, concurrent_downloads).await
        {
            error!("Failure while processing bucket {bucket:?}: {e}.");
            return Err(e);
        }
    }
    Ok(())
}

async fn backup_bucket(
    archive: &Arc<Archive>,
    bucket: &str,
    s3_endpoint: &str,
    s3_key: &Key,
    concurrent_downloads: usize,
) -> Result<()> {
    let client = Arc::new(tocco_s3::s3::client(
        s3_endpoint,
        &s3_key.access_key,
        &s3_key.secret_key,
    ));
    let objects = tocco_s3::s3::list_objects(&client, bucket);
    pin!(objects);

    let schedule_task = |tasks: &mut task::JoinSet<_>, object| {
        let client = Arc::clone(&client);
        let archive = Arc::clone(archive);
        tasks.spawn(async move {
            match download(client, archive, &object).await {
                Ok(()) => {
                    trace!("Successfully backed up {object}.");
                    Ok(())
                }
                Err(e) => {
                    error!("Failed to back up {object}: {e}.");
                    Err(e)
                }
            }
        });
    };

    let mut tasks = task::JoinSet::new();

    // Schedule initial tasks
    while let Some(object) = objects.try_next().await? {
        schedule_task(&mut tasks, object);
        if tasks.len() >= concurrent_downloads {
            break;
        }
    }

    loop {
        match tasks.join_next().await {
            Some(Ok(result)) => result?,
            Some(Err(e)) => panic!("worker thread panicked: {e}"),
            None => break,
        }

        // schedule new task for every task completed
        if let Some(object) = objects.try_next().await? {
            schedule_task(&mut tasks, object);
        }
    }

    Ok(())
}

async fn download(
    client: Arc<s3::Client>,
    archive: Arc<Archive>,
    object: &tocco_s3::s3::Object,
) -> Result<()> {
    trace!("Processing object {object}.");
    match object.hash() {
        Ok(hash) => download_hash_object(archive, &client, object, hash).await,
        Err(()) => download_non_hash_object(archive, &client, object).await,
    }
}

/// Download objects whose key is a [`Hash`].
async fn download_hash_object(
    archive: Arc<Archive>,
    client: &s3::Client,
    object: &tocco_s3::s3::Object,
    hash: Hash,
) -> Result<()> {
    let get_object = {
        let archive = Arc::clone(&archive);
        let bucket = object.bucket().to_owned();
        let hash = hash.clone();
        task::spawn_blocking(move || archive.get_object(&bucket, &hash))
    };
    if get_object.await.expect("worker thread panicked")?.is_some() {
        // Because the key is the hash of the content, the content cannot
        // change and we never need to update the object. Also, the object
        // is downloaded to a temporary file first, synced to disk and then
        // renamed. So, the content can't be truncated either.
        trace!("Object {object} already exists on disk.");
        return Ok(());
    }
    let object_reader = client
        .get_object()
        .bucket(object.bucket())
        .key(object.path())
        .send()
        .await?
        .body
        .into_async_read();
    let mut object_reader = SyncIoBridge::new(object_reader);
    let bucket = object.bucket().to_owned();
    let create_object_result =
        task::spawn_blocking(move || archive.create_object(&bucket, &hash, &mut object_reader))
            .await
            .expect("worker thread panicked");
    match create_object_result {
        Ok(fs_object) => {
            debug!("Object {fs_object:?} created in archive.");
            Ok(())
        }
        Err(e) if e.kind() == io::ErrorKind::AlreadyExists => {
            warn!("Object {object} created in archive meanwhile. Is a concurrent backup running?");
            Ok(())
        }
        Err(e) => Err(e.into()),
    }
}

/// Download objects whose key is NOT a [`Hash`].
async fn download_non_hash_object(
    archive: Arc<Archive>,
    client: &s3::Client,
    object: &tocco_s3::s3::Object,
) -> Result<()> {
    let path = PathBuf::from_iter([
        &archive.path() as &dyn AsRef<Path>,
        &object.bucket(),
        &object.path(),
    ]);
    match path.metadata() {
        Ok(file_metadata) => {
            let file_ts = file_metadata
                .modified()
                .expect("file has no modification timestamp");
            let object_ts = object.last_modified();
            if (file_ts, file_metadata.len()) == (object_ts, object.size()) {
                trace!("Object {object} up-to-date.");
                return Ok(());
            }
            debug!(
                "Updating outdated {object} ({} -> {}).",
                DateTime::<Local>::from(file_ts),
                DateTime::<Local>::from(object_ts)
            );
        }
        Err(e) if e.kind() == io::ErrorKind::NotFound => {
            debug!("Downloading new object {object}.");
            fs::create_dir_all(path.parent().expect("no bucket directory"))?;
        }
        Err(e) => return Err(e.into()),
    };

    let tmp_path = common::temp_file_path(archive.path(), object.bucket(), object.path());
    trace!("Downloading {object} to temporary file at {tmp_path:?}.");
    {
        let mut object_reader = client
            .get_object()
            .bucket(object.bucket())
            .key(object.path())
            .send()
            .await?
            .body
            .into_async_read();
        // create() truncates existing file
        let mut file = tokio::fs::File::create(&*tmp_path).await?;
        tokio::io::copy(&mut object_reader, &mut file).await?;
        {
            // TODO use async version once available
            //
            // https://github.com/tokio-rs/tokio/issues/6368

            let file = file.try_clone().await?.into_std().await;
            let timestamp = object.last_modified();
            task::spawn_blocking(move || file.set_modified(timestamp))
                .await
                .expect("worker thread panicked")?;
        }
        file.sync_all().await?;
    }
    trace!("Moving {object} to final destination at {path:?}");
    tokio::fs::rename(&*tmp_path, path).await?;
    Ok(())
}

pub(crate) async fn main(args: &Backup) -> u8 {
    let archive = match Archive::open(&args.directory) {
        Ok(archive) => Arc::new(archive),
        Err(e) => {
            error!("Failed to open archive: {e}");
            return 1;
        }
    };

    let mut s3_keys = match archive.s3_keys().await {
        Ok(keys) => keys,
        Err(e) => {
            error!("Failed to fetch S3 keys: {e}");
            return 1;
        }
    };

    if let Some(buckets) = &args.buckets {
        s3_keys.retain(|k, _v| buckets.contains(k));
    }

    if let Err(e) = backup(
        &archive,
        archive.s3_endpoint(),
        &s3_keys,
        args.concurrent_downloads.into(),
    )
    .await
    {
        error!("Backup failed: {e}");
        return 1;
    };

    if args.buckets.is_none() {
        if let Err(e) = archive.state_db().map(|db| db.update_last_s3_backup_time()) {
            error!("Failed to update last backup run timestamp in state DB: {e}.");
            return 1;
        }
    }

    0
}

#[cfg(test)]
mod tests {
    use super::*;

    #[tokio::test]
    async fn download_hash_object() {
        let _ = env_logger::builder().is_test(true).try_init();

        let content = "content of hash file";
        let dir = tempfile::tempdir().unwrap().path().join("archive");
        let archive = Arc::new(Archive::init(&dir).unwrap());

        // create object
        let (client, bucket) = test_utils::create_bucket().await;
        let object_key = test_utils::create_s3_object(&client, &bucket, content).await;

        // list objects
        let lister = tocco_s3::s3::list_objects(&client, &bucket);
        pin!(lister);
        let s3_object = lister.try_next().await.unwrap().unwrap();
        assert_eq!(s3_object.path(), object_key);
        assert!(lister.try_next().await.unwrap().is_none());

        // download object
        let hash = s3_object.hash().unwrap();
        super::download_hash_object(archive.clone(), &client, &s3_object, hash.clone())
            .await
            .unwrap();

        let fs_object = archive.get_object(&bucket, &hash).unwrap().unwrap();
        fs_object.check_integrity().unwrap();
    }

    #[tokio::test]
    async fn download_non_hash_object() {
        let _ = env_logger::builder().is_test(true).try_init();

        let path_in_bucket = "file/hierarchy/pointing/somewhere";
        let dir = tempfile::tempdir().unwrap().path().join("archive");
        let archive = Arc::new(Archive::init(&dir).unwrap());
        let (client, bucket) = test_utils::create_bucket().await;

        // create object
        let object_v1 = upload_and_redownload(
            &client,
            &archive,
            &bucket,
            path_in_bucket,
            "content version 1",
        )
        .await;
        assert_eq!(object_v1.size(), 17);

        // update object
        let object_v2 = upload_and_redownload(
            &client,
            &archive,
            &bucket,
            path_in_bucket,
            "content version 2.0",
        )
        .await;
        assert_eq!(object_v2.size(), 19);

        assert!(object_v1.last_modified() < object_v2.last_modified());
    }

    async fn upload_and_redownload(
        client: &s3::Client,
        archive: &Arc<Archive>,
        bucket: &str,
        path_in_bucket: &str,
        content: &str,
    ) -> tocco_s3::s3::Object {
        // create / update object on S3
        test_utils::create_s3_object_with_key(client, bucket, path_in_bucket, content).await;

        // Get only object from S3
        let lister = tocco_s3::s3::list_objects(client, bucket);
        pin!(lister);
        let s3_object = lister.try_next().await.unwrap().unwrap();
        assert_eq!(s3_object.path(), path_in_bucket);
        assert!(lister.try_next().await.unwrap().is_none());

        // Download to backup archive
        super::download_non_hash_object(archive.clone(), client, &s3_object)
            .await
            .unwrap();

        // Verify content
        let absolute_path = archive.path().join(bucket).join(path_in_bucket);
        assert_eq!(fs::read_to_string(absolute_path).unwrap(), content);

        s3_object
    }
}
