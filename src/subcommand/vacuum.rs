//! VACUUM database
//!
//! <https://www.sqlite.org/lang_vacuum.html>

use log::error;
use std::path::PathBuf;
use tocco_s3::fs::Archive;

#[derive(clap::Args)]
pub(crate) struct Args {
    /// Directory where backups are located
    #[arg()]
    directory: PathBuf,
}

pub(crate) fn main(args: &Args) -> u8 {
    let archive = match Archive::open(&args.directory) {
        Ok(archive) => archive,
        Err(e) => {
            error!("Failed to open archive: {e}");
            return 1;
        }
    };
    let db = archive.state_db().expect("failed to obtain state DB");
    db.vacuum().expect("VACUUM failed");
    0
}
