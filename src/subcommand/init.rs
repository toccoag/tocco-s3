//! Initialize backup archive

use clap::Args;
use std::path::PathBuf;
use tocco_s3::fs::Archive;

/// Initialized backup archive with a default config.
#[derive(Args)]
pub(crate) struct Init {
    /// Path at which backup archive is to be created.
    ///
    /// Path must not yet exist. Subdirectories are
    /// created recusively as needed.
    #[arg()]
    directory: PathBuf,
}

pub(crate) fn main(args: &Init) -> u8 {
    let archive = match Archive::init(&args.directory) {
        Ok(archive) => archive,
        Err(e) => {
            let directory = &args.directory;
            eprintln!("Failed to crate archive at {directory:?}: {e}");
            return 1;
        }
    };
    println!("Edit config file at {:?} as needed.", archive.config_path());
    0
}
