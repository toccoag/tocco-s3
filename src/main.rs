#![allow(clippy::manual_let_else)]
#![allow(clippy::needless_raw_string_hashes)]
#![allow(clippy::redundant_closure)]
#![allow(clippy::redundant_closure_for_method_calls)]
#![allow(clippy::single_match_else)]
// warn for development
//
// This are set to deny during CI but allowed locally
// to ease development.
//
// Sync with .gitlab-ci.yml
#![warn(clippy::dbg_macro)]
#![warn(clippy::unwrap_used)]

mod subcommand;

use clap::{Parser, Subcommand};
use std::process::ExitCode;
use subcommand::{
    backup, check_availability, check_content, init, list_servers, prune, restore, vacuum,
};

/// Tocco S3 backup utility
#[derive(Parser)]
#[command(
    after_help = "RUST_LOG env. var. can be used to adjust logging:\n\n\
                    - Enable debug logging: RUST_LOG=debug ...\n\
                    - Limit logs to tocco-s3 (without libraries): RUST_LOG=tocco_s3=debug\n\n\
                  See https://docs.rs/env_logger/latest/env_logger/#enabling-logging",
    verbatim_doc_comment
)]
pub(crate) struct Args {
    #[command(subcommand)]
    command: Commands,
}

#[derive(Subcommand)]
pub(crate) enum Commands {
    /// Check if all objects are available in backup archive
    CheckAvailability(check_availability::CheckAvailability),

    /// Check content of objects in FS against hashes
    CheckContent(check_content::CheckContent),

    /// Back up S3 object to archive
    ///
    /// Back up object from all S3 buckets to the
    /// on-disk archive.
    Backup(backup::Backup),

    /// Restore objects from archive
    Restore(restore::Restore),

    /// Remove unreference objects from S3
    Prune(prune::Prune),

    /// Initialize backup archive
    Init(init::Init),

    /// List DB servers
    ListServers(list_servers::ListServers),

    /// VACUUM DB
    Vacuum(vacuum::Args),
}

#[tokio::main]
async fn main() -> ExitCode {
    env_logger::init();
    let args = Args::parse();

    let ret = match args.command {
        Commands::Backup(ref args) => backup::main(args).await,
        Commands::CheckAvailability(ref args) => check_availability::main(args),
        Commands::CheckContent(ref args) => check_content::main(args),
        Commands::Init(ref args) => init::main(args),
        Commands::ListServers(ref args) => list_servers::main(args),
        Commands::Prune(ref args) => prune::main(args).await,
        Commands::Restore(ref args) => restore::main(args).await,
        Commands::Vacuum(ref args) => vacuum::main(args),
    };
    ExitCode::from(ret)
}
