//! Connect to DB server to fetch list of objects.
use crate::fs::Archive;
use std::fmt;
use std::io;
use std::ops::{Deref, DerefMut};
use std::path::PathBuf;
use std::process::{Command, Stdio};

/// DB server from which objects are fetched.
pub struct DbServer {
    /// Command to connect to server and fetch
    /// list of objects.
    command: Command,

    /// Minimum number of objects that must exist on server.
    min_objects: u64,

    /// String identifying server (for log messages).
    display: String,
}

impl DbServer {
    /// Start process which connects to DB server.
    pub fn spawn(&mut self) -> io::Result<Child> {
        let child = self.command.spawn()?;
        Ok(Child { inner: child })
    }

    /// Minimum number of objects that must exist
    /// on DB server.
    #[must_use]
    pub fn min_objects(&self) -> u64 {
        self.min_objects
    }
}

impl fmt::Display for DbServer {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", &self.display)
    }
}

/// SSH child process
///
/// Implements [`Deref`] and [`DerefMut`] for [`std::process::Child`].
///
/// Unlike [`std::process::Child`] the process is killed on [`Drop`].
pub struct Child {
    inner: std::process::Child,
}

impl Drop for Child {
    fn drop(&mut self) {
        let _ = self.inner.kill();
    }
}

impl Deref for Child {
    type Target = std::process::Child;
    fn deref(&self) -> &std::process::Child {
        &self.inner
    }
}

impl DerefMut for Child {
    fn deref_mut(&mut self) -> &mut std::process::Child {
        &mut self.inner
    }
}

/// Extract DB servers from archive.
///
/// `min_age_hours` tells remote server to only return
/// objects older than N hours.
#[must_use]
pub fn db_servers(archive: &Archive, min_age_hours: u32) -> Vec<DbServer> {
    archive
        .db_servers()
        .iter()
        .map(|db_server| {
            let uri = db_server.ssh_uri();
            let mut command = Command::new("ssh");
            command
                .arg("-n")
                .arg("-T")
                .arg("-o")
                .arg("BatchMode=yes")
                .arg("-o")
                .arg("ServerAliveInterval=60")
                .arg(&uri)
                .arg("tocco-list-objects")
                .stdout(Stdio::piped());
            if min_age_hours > 0 {
                let datetime = n_hours_before(chrono::Local::now(), min_age_hours);
                command.arg("--created-before").arg(&datetime);
            }
            DbServer {
                command,
                display: uri,
                min_objects: db_server.min_objects(),
            }
        })
        .collect()
}

/// Return timestamp `hours` behind `now`.
fn n_hours_before(now: chrono::DateTime<chrono::Local>, hours: u32) -> String {
    let now: chrono::DateTime<chrono::Utc> = now.into();
    let ignore_hours = chrono::Duration::hours(i64::from(hours));
    let datetime = now - ignore_hours;
    // The default, nano-second-precission, confuses Python's datetime module
    // in old version of Python.
    datetime.to_rfc3339_opts(chrono::SecondsFormat::Secs, false)
}

/// Construct mock DB server from command
///
/// Return a single mock DB server where connecting to it
/// is simulated via given `shell_command`.
#[cfg(unix)]
#[must_use]
pub fn custom_db_server(shell_command: &str, min_objects: u64) -> Vec<DbServer> {
    let shell = nix::unistd::User::from_uid(nix::unistd::Uid::current())
        .ok()
        .flatten()
        .map(|user| user.shell)
        .unwrap_or_else(|| PathBuf::from("/bin/sh"));
    let mut command = Command::new(shell);
    command.arg("-c").arg(shell_command).stdout(Stdio::piped());
    let display = "<custom command>".to_string();
    vec![DbServer {
        command,
        min_objects,
        display,
    }]
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn n_hours_before_test() {
        let now_str = "2030-05-16T18:23:25.553483455Z";
        let before_str = "2030-05-14T17:23:25+00:00";
        let now: chrono::DateTime<chrono::Local> = chrono::DateTime::parse_from_rfc3339(now_str)
            .unwrap()
            .into();
        assert_eq!(&n_hours_before(now, 49), &before_str);
    }
}
