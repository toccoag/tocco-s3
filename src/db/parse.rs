//! Parse list of objects from a DB server
//!
//! # Data format
//!
//! The following format is used when fetching a list of objects
//! from a DB server:
//!
//! ```text
//! {{ hash of object }},{{ db_name }}
//! ```
//! Lines that don't adhere to this format are reported as error and parsing
//! is continued. All lines must end with a single `'\n'`.
//!
//! Example:
//!
//! ```text
//! 91816f9bf1e491573819e2583d48ba44137dee3f9f7cf5b4f9f9c480beab9373,nice_bbg
//! 7d06173421f67acebbde6719151effca17db7ad45e5b6cacff982664a3938a90,nice_bbg
//! e98e1cc83322a374ba6109a881b2af82dc3e9f5d93e01c38ac79149c29548cef,nice_bbg
//! 3d4ac9fcdb67dd39f356478baec608cd2ccc50559467b71505678e07e2d8600d,nice_dlc
//! b5c81b9e77668ed5d5314fc0a31f6b6d202e1bcf16a3e997aa218f8292364529,nice_dlc
//! ```
//!
//! [`ObjectReader`] implements reading data in this format.

use crate::hash::Hash;
use fallible_iterator::FallibleIterator;
use std::io::{self, BufRead, BufReader};

/// Object referenced in a DB.
///
/// Yielded by [`ObjectReader`].
#[derive(Debug)]
pub struct Object {
    hash: Hash,

    /// Untrusted and unsanitized DB name.
    ///
    /// Coming from untrusted server and should not
    /// be used blindly.
    #[allow(dead_code)] // used by `Debug`
    db_name: String,
}

impl Object {
    /// Hash of object as recorded in DB.
    #[must_use]
    pub fn hash(&self) -> &Hash {
        &self.hash
    }
}

/// Parse list of objects and DB names.
///
/// See also [module documentation](`self`).
pub struct ObjectReader<R> {
    inner: BufReader<R>,
}

impl<R> ObjectReader<R>
where
    R: io::Read,
{
    pub fn new(inner: R) -> ObjectReader<R> {
        ObjectReader {
            inner: BufReader::new(inner),
        }
    }
}

impl<R> FallibleIterator for ObjectReader<R>
where
    R: io::Read,
{
    type Item = Object;
    type Error = io::Error;

    fn next(&mut self) -> io::Result<Option<Object>> {
        let mut line = String::new();
        let size = self.inner.read_line(&mut line)?;
        if size == 0 {
            return Ok(None);
        }
        let parsed_line = parse_line(&line);
        if let Err(ref e) = parsed_line {
            error!("Failed to parse object line {line:?}: {e}");
        }
        Ok(Some(parsed_line?))
    }
}

/// Parse a single `{hash},{db_name}` line.
fn parse_line(line: &str) -> io::Result<Object> {
    if !line.ends_with('\n') {
        return Err(io_error(format!(
            "missing newline character after {line:?}"
        )));
    }
    let line = &line[..line.len() - 1];

    let mut splitter = line.split(',');
    let hash = splitter.next().expect("no first item when splitting");
    let hash =
        Hash::new(hash).map_err(|()| io_error(format!("failed to parse {hash:?} as hash")))?;
    let db_name = splitter
        .next()
        .ok_or_else(|| io_error(format!("DB name missing in {line:?}")))?
        .to_string();

    if let Some(value) = splitter.next() {
        return Err(io_error(format!(
            "line unexpectedly continued with {value:?}"
        )));
    }

    Ok(Object { hash, db_name })
}

fn io_error(message: String) -> io::Error {
    io::Error::new(io::ErrorKind::InvalidData, message)
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn line_parsing() {
        // Ok
        let obj = parse_line(
            "091c8a26a4b6f81b3e65e59ab4414804fb34892369fa111f647be2ec56b2494d,nice_test1\n",
        )
        .unwrap();
        assert_eq!(
            obj.hash.hex_digest(),
            "091c8a26a4b6f81b3e65e59ab4414804fb34892369fa111f647be2ec56b2494d"
        );
        assert_eq!(obj.db_name, "nice_test1");

        // No newline
        let err = parse_line(
            "091c8a26a4b6f81b3e65e59ab4414804fb34892369fa111f647be2ec56b2494d,nice_test1",
        )
        .unwrap_err();
        assert_eq!(err.kind(), io::ErrorKind::InvalidData);

        // No DB name
        let err = parse_line("091c8a26a4b6f81b3e65e59ab4414804fb34892369fa111f647be2ec56b2494d\n")
            .unwrap_err();
        assert_eq!(err.kind(), io::ErrorKind::InvalidData);

        // Third value
        let err = parse_line(
            "091c8a26a4b6f81b3e65e59ab4414804fb34892369fa111f647be2ec56b2494d,nice_test1,third\n",
        )
        .unwrap_err();
        assert_eq!(err.kind(), io::ErrorKind::InvalidData);

        // Invalid hash
        let err = parse_line(
            "X91c8a26a4b6f81b3e65e59ab4414804fb34892369fa111f647be2ec56b2494d,nice_test1\n",
        )
        .unwrap_err();
        assert_eq!(err.kind(), io::ErrorKind::InvalidData);
    }

    #[test]
    fn reader_empty_line() {
        let input =
            "091c8a26a4b6f81b3e65e59ab4414804fb34892369fa111f647be2ec56b2494d,nice_test1\n\n\
             eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee,nice_test1\n";
        let reader = input.as_bytes();
        let reader = ObjectReader::new(reader);
        assert!(reader.count().is_err());
    }

    #[test]
    fn read_valid_data() {
        let input = "a09c28449cd89bc699848eab1a04beafadcc772d6b56b258cdbde5777121f236,nice_test1\n\
                     380f1fbe17b17bed78e1bfa70804b4d921eafdd31a2982a14217e195e99fafd1,nice_test1\n\
                     cbfaac3c40a1c46d5c01ba1498ca2d9681eb24c18d6684d92ee9ab721b5d2043,nice_master\n";
        let reader = input.as_bytes();
        let mut reader = ObjectReader::new(reader);

        let obj = reader.next().unwrap().unwrap();
        assert_eq!(
            obj.hash.hex_digest(),
            "a09c28449cd89bc699848eab1a04beafadcc772d6b56b258cdbde5777121f236"
        );
        assert_eq!(obj.db_name, "nice_test1");

        let obj = reader.next().unwrap().unwrap();
        assert_eq!(
            obj.hash.hex_digest(),
            "380f1fbe17b17bed78e1bfa70804b4d921eafdd31a2982a14217e195e99fafd1"
        );
        assert_eq!(obj.db_name, "nice_test1");

        let obj = reader.next().unwrap().unwrap();
        assert_eq!(
            obj.hash.hex_digest(),
            "cbfaac3c40a1c46d5c01ba1498ca2d9681eb24c18d6684d92ee9ab721b5d2043"
        );
        assert_eq!(obj.db_name, "nice_master");

        assert!(reader.next().unwrap().is_none());
    }
}
